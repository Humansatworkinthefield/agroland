# Contributing

If you have questions, do not hesitate to start a discussion and open a [ticket/issue](https://gitlab.com/agroland/client/-/issues) to provide some detailed info about your problem/suggestion.

# Contact

Contact me anytime over [DISCORD](https://discord.gg/X2zPfF8whW)! Let's build a Fakemon GAME for browsers with the HIGHLY AMBITIONAL ULTIMATE GOAL of beeing able to play online PVP...

# Step by step guidelines

Note that many methods will certainly be changed and simplified to improve the maintainability time by time.

## **Use the config file to debug faster.**

The project has a configuration file located in: "**src/config.ts**".

This file contains an object with several key-value pairs. Those values are changed very often during debugging. To keep things at one place, I currently use this which might not be a good solution but it does the job for now.

It helps a lot if you implement new maps, new NPCs, new monsters... [Check it out.](https://gitlab.com/agroland/client/-/blob/main/src/config.ts). 

## **How to add my tileset?**

If you are interested in tileset creation or map creation with tilesets, have a look at the related repository: [Agroland Tileset collection](https://gitlab.com/agroland/tileset).

There you will find all the assets used in this game, which you can modify or expand by adding yours.

## **How to create a new map?**

- Make sure to not create a map that is too big. I recommend a maximum of 70 tiles on the X axis and 50-70 tiles on the Y axis or vice-versa for mobile perfomance reasons.
- Watch this [video sequence](https://youtu.be/yP5DKzriqXA?t=612) to learn more on how to create your map. (from 0:11:30 to 1:08:00)
- All the assets needed to create a map are in this repository: [Tileset collection](https://gitlab.com/agroland/tileset).

## **How to add my new map to the game?**

Once you have created your map, you should now export your creation as .PNG image.

Before exporting:

- Make sure to set the zoom-level to **400%** in TILED!
- Make sure that your map has a collision layer.
- The wild battlezones (highGrass) should also be covered with collisions.
- Create a new folder in the following directory: "**public/imgs/maps/**"
- Name it for example: "map24", where "24" indicates the map ID. 
- Map IDs are limited and have an ID range from currently 20 to 39.
- In this new folder, you will usually find 1 to 3 files with a naming convention: "**background**", "**foreground**" and "**highGrass**"
- You can now export your map (the background related layers) as .PNG into the new created folder and name it "background".
- If your map has foregrounds and highGrass, you should export the corresponding layers as .PNG as well and stick to the file-naming convention. [See example.](https://gitlab.com/agroland/client/-/tree/main/public/imgs/maps/map20)

The first steps are done!

What we need to do now is to add the ID of the map that you created, for example "24", in the source code into the existing map data to determine where the **ENTRY** of your map will be.

If you open the file: "**src/data/map/mapData.ts**" in your code editor, you will see the data corresponding to all the maps in form of a JavaScript object named "mapData":

Now, have a look at any map-collisions array, you will see a lot of different integers. A description of those integers can be found in form of a comment at the top of the file.

```javascript
// Example
const mapData = {
  // map-collisions with The map ID "23"
  '23': {
    ...
    // Collisions are represented with a 8bit integer array (from 0-255) to save memory space
    collisions: new Uint8Array([
      0,1,1,1,1,0,
      1,0,0,0,0,1,
      1,0,0,0,0,1,
      1,0,0,0,0,1,
      1,0,0,0,0,1,
      0,1,1,1,1,0,
    ])
  }
}
```

You can notice the integer "1" all around. It represents the borders of the map.

This is where you want to add an entry to another map. For example, if you want to access the "map24" from the "map23", the collisions should look like this:

```javascript
// Example
const mapData = {
  '23': {
    ...
    collisions: new Uint8Array([
      0,1,1,1,1,0,
      1,0,0,0,0,1,
      1,0,0,0,0,24,
      1,0,0,0,0,24,
      1,0,0,0,0,1,
      0,1,1,1,1,0,
    ])
  }
}
```
This way, the "map24" will be rendered as soon as the player collides with the integer "24".

- Choose a map from which you want to access your new "map24".
- For example, if you want to access you new "map24" from "map23", you should have a look at the collision array of map23.
- In that collision array, you simply need to change two or three "1" integers at the corners (depending on where and how big the entry should be) into your map ID "24".
- If the background image of "map23" is bordered with trees, where a map entry is not possible, you can modify the [map23 image with TILED](https://gitlab.com/agroland/tileset/-/tree/main/tilemap/maps) and remove 2-3 trees.
- Now that the collision array of "map23" has the reference of your new map-ID "24", it will render your map as soon as the player collides with that ID.

We are almost done! The next steps are needed to allow us to use the new map and move around there by adding the data needed:

In the TILED programm, you can export your project as .JSON format to get access to the collision data. The collision data is available if you have created a collision layer named "collisions" for your new map in TILED.

- Export the .JSON file anywhere on your machine, we will copy&paste some content.
- Once you have exported the .JSON file, you should now create the map data object of your new map.
- Go to "**src/data/map/mapData.ts**", add a new KEY "24" to the mapData object.
- This new KEY needs a VALUE, which is an object that contains all the map data.
- Enter the data that correspond to your map:
  - tilesCount
  - offset (For the initial offset, start with {x: 0, y: 0} for smartphone & desktop. We will determine this later once the map can be rendered.)
  - audio (I currently download mp3 files from [Pokémon DIAMOND/PEARL](https://downloads.khinsider.com/game-soundtracks/album/pokemon-diamond-and-pearl-super-music-collection))
  - houses (can be an empty array for now)
  - collisions
- Now we can look at the .JSON file that we previously exported from TILED, search for "collision" and copy the collision array from it.
- Paste the collision array into the property "collisions" of your map data as "new Uint8Array([your collision data])".
- We need to find and replace all the integers that are not "0" in that specific array with the integer "1".

This is how you map object should look like now:

```javascript
const mapData = {
  // Your new map24 data object example:
  '24': {
    tilesCount: { x: 5, y: 5 },
    offset: {
      smartphone: { x: 0, y: 0 },
      desktop: { x: 0, y: 0 }
    },
    audio: {
      src: './audio/map/myAwesomeMapAudio.mp3',
      html5: true,
      volume: .3,
      loop: true,
      preload: true
    },
    houses: [],
    collisions: new Uint8Array([
      0,1,1,1,1,0,
      1,0,0,0,0,1,
      1,0,0,0,0,1,
      1,0,0,0,0,1,
      1,0,0,0,0,1,
      0,1,1,1,1,0,
    ])
  }
}
```

Great! Your map can be visited now.

To speed things up while testing, you can check the config file in: "**src/config.ts**" and change the config values that help you, for example:

```javascript
export const config = {
  ...
  // skip the intro sequence
  withIntro: false,
  // The map ID where the game starts at
  playerMapID: 24,
  // The house ID in that specific map where the game starts at. Important: Set to 0 if you want to start in the map outside a house.
  playerHouseID: 0
  ...
}
```

You will notice 3 things:

- The map position is wrong (due to the offset that we previously set to {x: 0, y: 0})
- The wild battlezones are also collisions where you can not walk through
- We also need to be able to go back to the previous map.

To fix the offset issue, we simply need to play around with the "x" & "y" values of the offset:

- Start by testing with a smartphone viewport by opening the dev-tools in your browser, click on the viewport button and choose any smartphone size.
- Try an offset of {x: -100, y: -100}
- Refresh the site, you will see that the map has moved its position. Repeat the offset update until you reach the wished start-position.
- Now, we need to do the same for the desktop offset by testing with a desktop viewport!

To fix the wild battlezones, you simply need to find the corresponding "1" integers in the collisions array and replace the "1" with an integer between 245 and 255. 

To be able to go back to the previous map, we simply need to add the previous map ID in the collision array of our new map data, for example:

```javascript
'24': {
  ...
  // collisions array without previous map entry
  collisions: new Uint8Array([
    0,1,1,1,1,0,
    1,0,0,0,0,1,
    1,0,0,0,0,1,
    1,0,0,0,0,1,
    1,0,0,0,0,1,
    0,1,1,1,1,0
  ])
  // Your collision array should then look like this given the previous map ID is "23":
  collisions: new Uint8Array([
    0,1,1,1,1,0,
    1,0,0,0,0,1,
    23,0,0,0,0,1,
    23,0,0,0,0,1,
    1,0,0,0,0,1,
    0,1,1,1,1,0
  ])
}
```

Hopefully you found this helpful! Of course, your collisions array is much bigger, has much more collisions in it and you want to add more stuff into it. No problem, I will explain how to do it in the next steps.

## **How to place monsters as wild into the map?**

Your map is almost filled with "life". To add random wild monsters into it, you just need to open the file: "**src/data/monster/monsterLocation.ts**".

This file contains the relevant data about which monster should appear in which grass/map. You can modify it like this:

```javascript
// Your map ID
24: {
  // The high grass ID that matches the integers in the map data collisions array
  255: [
    MonsterNames.Mamus,
    ... // more monsters
  ],
  // The high grass ID that matches the integers in the map data collisions array
  254: [
    MonsterNames.Blipbug,
    ... // more monsters
  ],
},
```

Done! Now you need to determine a range for the monster-level in this map. Open the file "**src/classes/Movables.ts**". Search for the method "**getWildLvlRange()**" and add the level range for your map ID:

```javascript
#getWildLvlRange() {
  const ranges: { [key: string]: number[] } = {
    ...
    // your map ID 24
    24: [10, 15] // example level range from 10 to 15
  }
}
```

## **How to add the house entries into the map?**

Actually, this procedure is the same as adding a new map to the game like I explained above. In your new map data, you have a property called "houses" which currently is an empty array.

Depending on how many houses you have put into your map, you will need to add as much house data objects in that empty array.

- You will need to create a house interior map with the TILED programm.
- Export the background and foreground images as .PNG
- Export the .JSON file to get access to the collision array provided by TILED.
- While exporting the images, follow the directory naming conventions.
- Create the corresponding house data object in the array "houses" of your map.
  - id (Give your house an ID that can be in a range of 5-19)
  - tilesCount
  - offset (For the initial offset, start with {x: 0, y: 0} for smartphone & desktop. We will determine this later once the new map can be rendered.)
  - audio (I currently download mp3 files from [Pokémon DIAMOND/PEARL](https://downloads.khinsider.com/game-soundtracks/album/pokemon-diamond-and-pearl-super-music-collection))
  - collisions

Your house data object should look like this:

```javascript
const houseData = {
  // House ID
  id: 5,
  // The count of Tiles that you determine by creating a new HOUSE with the TILED programm.
  tilesCount: { x: 12, y: 12 },
  // The initial offset that determines the start position of the image.
  offset: {
    // Since the image is scaled down on small devices, I used 2 offset references, one for smartphones
    smartphone: { x: 0, y: 0 },
    // One for desktops that actually starts at the breakpoint of Tablets (768x1024)
    desktop: { x: 0, y: 0 }
  },
  // An audio object that is played when the player is in that house
  audio: {
    src: './audio/map/myAwesomeAudio.mp3',
    html5: true,
    volume: .3,
    loop: true,
    preload: true
  },
  // An integer array (from 0-255) that represents the collisions of the house interior
  collisions: new Uint8Array([])
},
```

Now we can look at the .JSON file that we previously exported from TILED, search for "collision" and copy the collision array from it.

- Paste the collision array into the property "collisions" of your house data.
- We need to find and replace all the integers that are not "0" in that specific array with the integer "1".
- We need to find the house entry in the map collisions array and change the integer "1" where the house door is, with the ID of the house so that once the player collides with that ID, it will render the house.
- update the offset on smartphone and desktops.
- set a house exit by replacing an integer "1" with a "5" anywhere in the collision array where the exit door should be.

That's it! The houses can now be visited!

## **How to add new NPCs?**

First of all, you will need to create some NPC sprites. Simply have a look at: "**public/imgs/npcs/**". You will see 2 folders, 1 for trainers and 1 for villagers.

There are 2 types of NPCs: trainers and non-trainers (villagers).

### Add non-trainer villager:

- Open the folder villagers in: "**public/imgs/npcs/**". You will see the some villager folders that have already been created.
- You can copy a folder and rename it with a villager name that you like.
- In that folder you will see 5 files named "idle", "up", "right",  "down", "left".
- It can also include 4 additional sprites named "headUp", "headRight", "headDown", "headLeft". This sprites are used if the NPC should walk over high grass.
- You need to keep the naming convention and every villager needs all the 5 sprites.
- To keep it simple and for orientation purposes about the sprite sizes, you can just modify the sprites that you just copied.
- Change the hair color, shirt color for example.

That's it! You just created a new villager sprite. Of course, your imagination has no limits and you can modify more or create your own as long as you keep the same sizes.

What we need to do now, is to add the NPC object into the source code. Go to: "**src/data/npcEntries.ts**". You will find an object called "nonTrainerNPCs" that contains all the current NPCs. A villager NPC object looks like this:

```javascript
// the NPC name as property which is used to get access to this reference
Bob: {
  // the name
  name: 'bob',
  // the image source where your new sprite is located. This source will be the one that is rendered  
  image: { src: './imgs/npcs/villagers/prof_antopoulos/up.png' },
  // if the npc should walk on high grass, it will also need 4 additional head sprites
  // they are rendered over the high grass while the body is rendered under the high grass 
  headImgs: {
    up: { src: './imgs/npcs/villagers/villager6/headUp.png' },
    right: { src: './imgs/npcs/villagers/villager6/headRight.png' },
    down: { src: './imgs/npcs/villagers/villager6/headDown.png' },
    left: { src: './imgs/npcs/villagers/villager6/headLeft.png' }
  },
  // the 4 directions sprite source
  sprites: {
    up: { src: './imgs/npcs/villagers/prof_antopoulos/up.png' },
    right: { src: './imgs/npcs/villagers/prof_antopoulos/right.png' },
    down: { src: './imgs/npcs/villagers/prof_antopoulos/down.png' },
    left: { src: './imgs/npcs/villagers/prof_antopoulos/left.png' }
  },
}
```

Your new villager has been added to the source code!

### Add trainer:

The only difference here is that the trainer NPC also needs a battle-sprite. Have a look at "**src/imgs/npcs/trainers/**" to see the implemented trainers.

Get an orientation about the sprite sizes and then add your own. Don't forget to stick to the file naming convention.

Just like the villager, the trainer also needs to be added into the source code. Go to: "**src/data/npcEntries.ts**". You will find an object called "trainerNPCs" that contains all the current trainers. A trainer NPC object looks like this:

```javascript
// the NPC name as property which is used to get access to this reference
Environmental_activist: {
  // the name
  name: 'environmental activist',
  // the image source where your new sprite is located. This source will be the one that is rendered
  image: { src: './imgs/npcs/trainers/Environmental_activist/right.png' },
  // an audio object that is used to play the specific trainer detection and battle audio
  audio: trainerAudio,
  // the 4 directions sprite source
  sprites: {
    up: { src: './imgs/npcs/trainers/Environmental_activist/up.png' },
    right: { src: './imgs/npcs/trainers/Environmental_activist/right.png' },
    down: { src: './imgs/npcs/trainers/Environmental_activist/down.png' },
    left: { src: './imgs/npcs/trainers/Environmental_activist/left.png' }
  },
  // the source of the battle sprite
  battleSpriteSrc: { src: './imgs/npcs/trainers/Environmental_activist/battle.png' },
  // an integer that represents the effort of beating this npc, which will be multiplied by its last monster level to get the amount of money won
  effort: 55,
  // a boolean that indicates if it is a trainer. WARNING: is set to false if it is a gym leader or the rival
  isTrainer: true,
  // a boolean that indicates if it is a gym leader
  isGym: false
},
```

Your new trainer has been added to the source code!

## **How to place NPCs into the map?**

You can place NPCs anywhere in a map or in a house just by adding an specific integer between 100-199 into a collisions array of your choice. For example, you can have a look at the current collisions array of any map and
you will be able to detect some of these integers.

If you are not sure which integers are already in use, since there can NOT be any duplications for NPCs, you can just look in: "**src/data/npcEntries.ts**".

There you will find an object called "allNPCs". This object contains the NPCs that are located in all maps. Look at the map ID where you want to add your NPC, you will find an array with NPC objects.

- Each NPC object has an ID.
- You can use the same IDs in different maps, but not in the same maps.
- You can use the same NPC sprite multiple times in the same map by giving it different IDs.
- The IDs that are given there can be found in the corresponding map collisions array.

In the collisions array where you want your NPC to be found in, you can add an ID that is not already in use by looking at the npc array in "**src/data/npcEntries.ts**".

Once you have placed your ID in the collisions array, You will need to add it as entry in the npc array in "**src/data/npcEntries.ts**".

Find the corresponding map where you added the ID into its collisions array and add a new object to the npc array:

```javascript
// Keep in mind that some properties are optional
// A non-trainer villager object entry into a map looks like this:
{
  // the reference to the initial npc object where the images and name can be found.
  ...structuredClone(nonTrainerNPCs['Villager4']),
  // a new image src if you want to render the npc with a specific direction first
  image: { src: '...' },
  // the id that you added into the collisions array
  id: 101,
  // a boolean that indicates if the npc is animated (false if npc should move around, true if it is an idle npc)
  animate: false,
  // a boolean that indicates if the npc moves around
  isMovingAround: true,
  // an integer that indicates the max. radius in pixel for the npc to move in
  maxRange: 150 * viewport.getScaleFactor(false),
  // if the npc moves around, the "moveFrames" object. If it is an idle npc, the "frames" object
  frames: moveFrames,
  // the dialog queue that is fired when the player interact with that npc
  firstDialog: (npc: NPC) => [
    () => dialog.show('My awesome dialog'),
    npc.finishDialog
  ],
  // a gift that can be handed to the player. The code for handing the item out needs to be written in the first dialog queue like the mother NPC in the first map
  itemGift: new Ball({ name: 'Ball', count: 5 }),
  // the second dialog queue that is fired once the item has been given out.
  secondDialog: (npc: NPC) => [
    () => dialog.show(`Have you cleaned your room and turned off your PC, ${player.name}?`),
    () => dialog.show('I miss you already!'),
    npc.finishDialog
  ]
}

// A trainer object entry looks like this: 
{
  // the reference to the initial npc object where the images and name can be found.
  ...structuredClone(trainerNPCs['Environmental_activist']),
  // a new image src if you want to render the npc with a specific direction first
  image: { src: '...' },
  // the id that you added into the collisions array
  id: 102,
  // a boolean that indicates if the npc is animated (false if npc should move around, true if it is an idle npc)
  animate: false,
  // if the npc moves around, the "moveFrames" object. If it is an idle npc, the "frames" object
  frames,
  // the dialog queue that is fired when the player interact with that npc or have been detected by him
  firstDialog: () => [() => dialog.show('HEY YOU! Are you a MINER?! BITCOIN MINER are destroying the planet!')],
  // The dialog that is shown in battle after beating this npc
  looseDialog: 'Well, I need to get stronger to beat all the MINER...',
  // The dialog that is shown in battle after loosing against this npc
  winDialog: 'HAHA I WON! This is how I will shut down all of the MINER!',
  // The dialog that is shown after the battle if player interacts with that npc again
  afterBattleDialog: () => [() => dialog.show('Leave me alone you CRIMINAL!')],
  // an array of monsters for this npc
  monsters: [
    new Monster({
      // the function that returns the data needed for the monster by name
      ...getMonster('Blipbug'),
      // a unique id that is not needed for trainer monsters
      uid: 0,
      // the audio of that monster by name
      audio: monsterAudio['Blipbug'],
      // the level
      level: 6,
      // a boolean that indicates that the monster is an enemy
      isEnemy: true
    }),
    ... // More monsters
  ]
  // a boolean that indicates if this npc has been defeated or not
  isBeaten: false
}
```

Awesome! We have more content for the game now! 

## **How to add new monsters?**

First of all, you should create the monster sprite. You can find a very cool collection of fakemons here:

[https://www.pokecommunity.com/showthread.php?t=267728](https://www.pokecommunity.com/showthread.php?t=267728)

[https://www.pokecommunity.com/showthread.php?t=314422](https://www.pokecommunity.com/showthread.php?t=314422)

[https://www.pokecommunity.com/showthread.php?t=368703](https://www.pokecommunity.com/showthread.php?t=368703)

- Choose one that is not already available, or create your own of course...
- Go to "**public/imgs/monsters**" and create a new folder with with the name of the monster.
- You will notice that we need 2 sprites for each monster: "face up" and "face down". Take care of the file naming convention.
- The **image sizes are important** since they will be rendered at the same sizes. So if you want a big monster, the image should be bigger. Get some orientation about sizes with the other available [sprites](https://gitlab.com/agroland/client/-/tree/dev/public/imgs/monsters).

Awesome! The only thing we need to do now, is to add some data for your new monster. Go to "**src/data/monster/allMonsters.ts**".

1. Add the name to the ENUM object:

```javascript
export enum MonsterNames {
  MyAwesomeMonster,
}
```

2. Add the name to the string array:

```javascript
const monNames = [
  'MyAwesomeMonster',
]
```

3. Add the data:

```javascript
const monsters = {
  ...
  // The name as property
  MyAwesomeMonster: {
    // the idx (number) that is incremental to the previous monster
    idx: 40,
    // the monster name
    name: 'MyAwesomeMonster',
    // the image source (face down)
    image: { src: './imgs/monsters/MyAwesomeMonster/MyAwesomeMonster0.png' },
    // the image source (face up)
    imageFaceUp: { src: './imgs/monsters/MyAwesomeMonster/MyAwesomeMonster.png' },
    // a description
    description: 'My awesome description',
    // the weight
    weight: 11, // kg
    // the height
    height: 59, // cm
    // the position which is always x: 0, y: 0 and is determined later
    position: { x: 0, y: 0 },
    // the type (accepts max 2 types in array)
    type: [MonsterType.water, Monstertype.flying],
    // the general stats
    stats: {
      // base stats from 1 to 255
      // The higher, the better
      base: {
        hp: 50,    // health points
        att: 35,   // attack
        def: 40,   // defense
        ini: 40,   // initiative
        spAtt: 39, // special attack
        spDef: 45  // special def
      },
      // internal values from 1 to 15
      // The higher, the better
      IV: {
        hp: 4,
        att: 3,
        def: 4,
        ini: 5,
        spAtt: 4,
        spDef: 6
      },
      // external value yield - 1 to 3 in total (gained by the monster that beats it)
      EVY: {
        hp: 0,
        att: 0,
        def: 1,
        ini: 0,
        spAtt: 0,
        spDef: 0
      },
      // the initial effort values that are incremented with the EVY of the fainting monster after each fight
      EV: initialStats, // Max 255 each stat and max 510 in total
      // The actual stats derived from level, base, IV & EV
      IS: initialStats,
    },
	  // the effort value that is used to determine the XP gain of the winning monster
    effort: 42,
	  // the growth category which determines how much XP are needed to level-up (1=slow, 2=medium-slow, 3=medium-fast, 4=fast)
    cat: 3,
    // The status that is changed during battle (frozen, poisoned...)
    status: initialStatus,
    // the catch rate from 1 to 255 where 255 is always catched
    catchRate: 100,
    // the spwan chance as wild monster where 1 is the highest chance
    spawnChance: .01,
    // the attacks that the monster has depending on its level. Choosen randomly for a wild monster.
    lvlAttacks: {
      1: allAttacks.Growl,
      3: allAttacks.Tackle,
      7: allAttacks.Waterball,
      10: allAttacks.Scratch,
    } as const,
    // the evolution level, undefined if no evolution
    transformLvl: 16,
    // the idx of the evolution monster. Is always the idx of the current monster +1
    // undefined if no evolution
    transTo: 41
  }
}
```

Congratulations! You have added a new Monster with all the data needed!

## **How to add new attacks?**

[Click here](https://gitlab.com/agroland/client/-/issues/40) to see the related ticket/issue. 

Creating a new attack object is the simple part... Go to "**src/classes/Attack.ts**".

You will find an object called "allAttacks". The attacks should have the following data:

```javascript
const allAttacks = {
  ...
  // the attack name as property
  Tackle: {
    // the attack name
    name: 'Tackle',
    // the attack power. Orientation: https://bulbapedia.bulbagarden.net/wiki/List_of_moves
    power: 30,
    // the attack type
    type: MonsterType.normal,
    // the maximum execution times
    maxExec: 35,
    // the remaining executions
    remExec: 35,
    // the accuracy from .01 to 1 (pseudo %)
    acc: 1,
    // the chance of a critical hit from .01 to 1 (pseudo %)
    criticalChance: .05,
    // special indicator
    isSpecial: false,
    // short attack description
    info: 'Hits the opponent with full body contact.'
  },
}
```

That's it!

The difficult part comes now, we need to add the attack sequence which includes the animation and the damage/status sequence.

In the same file, have a look at the class called "Sequences". In this class are all the current attack sequences listed. We need to consider the following points:

- The attack sequence method always has 4 parameters:
  - The attacking monster object
  - The target monster object
  - The ID of the target HP progressbar element
  - The damage result object

The sequence methods handles the attack animations with the GSAP library. See [docs](https://greensock.com/docs/) for usage information.

I currently use simple sprites for most of the attacks but of course you can animate an attack however you want, for example with canvas particles, which I find very cool but also very complicated.

The attack sequence will be picked automatically depending on which monster is able to execute it.

For this to work, we need to add the attack sequence method to the sequence property, which you can find on the very end of the class declared as a private method and looks like this:

```javascript
class Sequences {
  ...
  #sequences: TAttackSequences = {
    // It is important here that the KEY has the exact
    // same name as the attack property "name" declared in the object "allAttacks".
    'Tackle': this.tackle,
    ... // more sequences
  }
}
```

Once you have followed these steps and added all the relevant data and sequence method, you can add your new attack to a monster.

- Go to the file: "**src/data/monster/allMonsters.ts**"
- Find the monsters that you want your attack to be in

For example:

```javascript
const monsters = {
  ...
  Draggle: {
    ...
    lvlAttacks: {
      // key: the level where the monster should learn your attack 
      // value: the corresponding attack object
      10: allAttacks.YourAwesomeAttack
    } as const
  }
}
```

Done!

## **How to add new items?**

[Click here](https://gitlab.com/agroland/client/-/issues/40) to see the related ticket/issue. 

In progress... guide available soon.

## **How to place hidden/visible items into the map?**

In progress... guide available soon.