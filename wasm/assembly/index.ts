// The entry file of your WebAssembly module.

// export function add(a: i32, b: i32): i32 {
// 	return a + b
// }

export function calcScaledXPGain(isWild: boolean, effort: f64, level: f64, wLevel: f64): f64 {
	const a: f64 = isWild ? 1.0 : 1.5
	const t: f64 = 1.0
	const b: f64 = effort
	const e: f64 = 1.0
	const L: f64 = level
	const p: f64 = 1.0
	const s: f64 = 1.0 // getBattleMonsAliveCount()
	const Lp: f64 = wLevel
	const result: f64 = Math.floor(((((a * b * L) / (5.0 * s)) * Math.pow(((2.0 * L + 10.0) / (L + Lp + 10.0)), 2.5)))) * t * e * p
	return result > 100000.0 ? 100000.0 : result
}

export function isColliding(
	rect1x: i32,
	rect1y: i32,
	rect1Width: i32,
	rect1Height: i32,
	rect2x: i32,
	rect2y: i32,
	boundaryW: i32,
	boundaryH: i32,
	scaleFactor: boolean
): boolean {
	return rect1x + rect1Width >= rect2x + (scaleFactor ? 2 : 4) && // player right side against collision left side
		rect1x <= rect2x + boundaryW - (scaleFactor ? 2 : 4) && // player left side against collision left side
		rect1y + (scaleFactor ? 15 : 30) <= rect2y + boundaryH && // player top side against collision bottom side
		rect1y + rect1Height >= rect2y // player bottom side against collision top side
}

export function isNPCColliding(
	rect1x: i32,
	rect1y: i32,
	rect1Width: i32,
	rect1Height: i32,
	rect2x: i32,
	rect2y: i32,
	boundaryW: i32,
	boundaryH: i32
): boolean {
	return rect1x + rect1Width >= rect2x && // player right side against collision left side
		rect1x <= rect2x + boundaryW && // player left side against collision left side
		rect1y <= rect2y + boundaryH + 10 && // player top side against collision bottom side
		rect1y + rect1Height >= rect2y // player bottom side against collision top side
}

// export function isCollidingNPC(
// 	rect1x: i32,
// 	rect1y: i32,
// 	rect1Width: i32,
// 	rect1Height: i32,
// 	rect2x: i32,
// 	rect2y: i32,
// 	boundaryW: i32,
// 	boundaryH: i32
// ): boolean {
// 	return (
// 		rect1x + rect1Width >= rect2x &&
// 		rect1x <= rect2x + boundaryW &&
// 		rect1y <= rect2y + boundaryH &&
// 		rect1y + (rect1Height / 2) >= rect2y
// 	)
// }

export function calculateOverlappingArea(
	playerPosX: f64,
	playerWidth: f64,
	boundaryX: f64,
	boundaryWidth: f64,
	playerPosY: f64,
	playerHeight: f64,
	boundaryY: f64,
	boundaryHeight: f64
): f64 {
	return (Math.min(playerPosX + playerWidth, boundaryX + boundaryWidth) -
		Math.max(playerPosX, boundaryX)) *
		(Math.min(playerPosY + playerHeight, boundaryY + boundaryHeight) -
			Math.max(playerPosY, boundaryY))
}
