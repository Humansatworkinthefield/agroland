import gsap, { Bounce, Power0, Power1 } from 'gsap'
import { battle, player } from '..'
import { canvas } from '../classes/Canvas'
import { dialog } from '../classes/Dialog'
import { flags } from '../classes/Flags'
import { formula } from '../classes/Formula'
import { Monster } from '../classes/Monster'
import { reaction } from '../classes/ReactionQueue'
import { Sprite } from '../classes/Sprite'
import { viewport } from '../classes/Viewport'
import { getMonster } from '../data/monster/allMonsters'
import { IIsCatched } from '../models/functions'
import { focusFirstChildAfterTimeout, getElByQuery } from '../util/helper'
import { formatName } from '../util/fmt'
import { enemyMonInfoEl, monsterInfoEl, dexEntryEl } from '../util/html/elements'
import { endBattleAnimation, getCenteredSmokePos } from './battle'
import { playAudio, mapAudio } from '../data/audio'
import { statusSequence } from '../classes/Status'
import { getLocationStr } from '../data/map/location'
import { events } from '../classes/Events'

/**
 * This function animates the ball throw and the ball shake sequence
 * @param enemy - The monster object that might be catched
 * @param ballName - A string that represents the name of the used ball
 */
export const animateBallThrow = (enemy: Monster, ballName: string) => {
	const initialBallPos = {
		x: viewport.isSmartphone() ? canvas.element.width / 5 : canvas.element.width / 3,
		y: canvas.element.height
	}
	const ball = new Sprite({
		position: initialBallPos,
		image: { src: './imgs/ball/ball.png' },
		scaleFactor: viewport.getTrainerScaleFactor()
	})
	const initialSmokePos = {
		x: getCenteredSmokePos(enemy).x,
		y: getCenteredSmokePos(enemy).y
	}
	const smoke = new Sprite({
		position: initialSmokePos,
		image: { src: './imgs/ball/ballSmoke.png' },
		animate: true,
		frames: { max: 6, hold: 8 },
		scaleFactor: viewport.getTrainerScaleFactor()
	})
	// reset previous sprite changes
	ball.position = { ...initialBallPos }
	smoke.opacity = 1
	smoke.position = { ...initialSmokePos }
	smoke.frames.val = 0
	battle.pushToRenderedSprites(ball)
	playAudio('throw')
	// catch result
	const catched = formula.isCatched(ballName, enemy)
	if (catched.ballShakeCount < 0) {
		// Missed target
		missedTarget(enemy, ball)
		return
	}
	const timeline = gsap.timeline()
	if (catched.ballShakeCount >= 0) {
		// POOR throw animation without curved path
		// throw over viewport
		timeline.to(ball.position, {
			x: viewport.isSmartphone() ? canvas.element.width / 2.5 : canvas.element.width / 2,
			y: - 50,
			duration: viewport.isSmartphone() ? .8 : .6
			// over viewport move to right 
		}).to(ball.position, {
			x: enemy.sprite.position.x - 20,
			y: -50,
			duration: .5
			// move down
		}).to(ball.position, {
			x: enemy.sprite.position.x + enemy.sprite.enemyWidth / 2,
			y: enemy.sprite.position.y + enemy.sprite.enemyHeight / 2,
			duration: viewport.isSmartphone() ? 1 : .5,
			yoyo: true,
			ease: Power1.easeIn,
			// catch animation
			onComplete: () => {
				playAudio('ballImpact')
				playAudio('initBall')
				// high brightness
				enemy.sprite.brightness = 250
				// add smoke
				battle.pushToRenderedSprites(smoke)
				// enemy opacity
				gsap.to(enemy.sprite, {
					opacity: 0,
					duration: .7,
					yoyo: true,
					ease: Bounce.easeInOut,
					onComplete: () => {
						// Ball bounce audio
						playAudio('ballImpact')
					}
				})
				// ball bounce on monster
				gsap.to(ball.position, {
					y: enemy.sprite.position.y - enemy.sprite.enemyHeight / 6,
					duration: .4,
					yoyo: true,
					ease: Power1.easeOut,
					onComplete: () => {
						// Ball bounce audio
						const tb = setTimeout(() => {
							playAudio('ballImpact')
							clearTimeout(tb)
						}, 750)
						gsap.to(ball.position, {
							y: enemy.sprite.position.y + enemy.sprite.enemyHeight - 20,
							yoyo: true,
							ease: Bounce.easeOut,
							duration: 1,
							onComplete: () => {
								// Ball actually stands still with monster in it
								const t = setTimeout(() => {
									ballShake(enemy, catched, ball, smoke)
									clearTimeout(t)
								}, 500)
							}
						})
					}
				})
				// smoke opacity
				gsap.to(smoke, {
					opacity: 0,
					duration: 2,
				})
			}
		})
	}
}

/**
 * This function animates the ball shaking and calls the catch or breakfree sequence
 * @param enemy - The monster object that might be catched
 * @param catched - The catched object coming from the formula.isCatched() output
 * @param ball - The ball sprite object
 * @param smoke - The smoke sprite object
 */
const ballShake = (enemy: Monster, catched: IIsCatched, ball: Sprite, smoke: Sprite) => {
	// play ball hit 2x
	playBallHit()
	// First shake
	gsap.to(ball.position, {
		// x: 750 + 11,
		x: enemy.sprite.position.x + (enemy.sprite.enemyWidth / 2) + 11,
		yoyo: true,
		repeat: 5,
		duration: .1,
		onComplete: () => {
			// monster breaks free after first shake
			if (catched.brokeFree && catched.ballShakeCount === 1) {
				const t = setTimeout(() => {
					breakFree(enemy, smoke)
					clearTimeout(t)
				}, 900)
				return
			}
			// Second shake
			const t = setTimeout(() => {
				// play ball hit 2x
				playBallHit()
				gsap.to(ball.position, {
					x: enemy.sprite.position.x + (enemy.sprite.enemyWidth / 2) + 8,
					yoyo: true,
					repeat: 5,
					duration: .08,
					onComplete: () => {
						// monster breaks free after second shake
						if (catched.brokeFree && catched.ballShakeCount === 2) {
							const t = setTimeout(() => {
								breakFree(enemy, smoke)
								clearTimeout(t)
							}, 1100)
							return
						}
						// Third shake
						const bt = setTimeout(() => {
							// play ball hit 3x
							playBallHit(true)
							gsap.to(ball.position, {
								x: enemy.sprite.position.x + (enemy.sprite.enemyWidth / 2) + 8,
								yoyo: true,
								repeat: 5,
								duration: .15,
								onComplete: () => {
									if (catched.brokeFree && catched.ballShakeCount === 3) {
										const t = setTimeout(() => {
											breakFree(enemy, smoke)
											clearTimeout(t)
										}, 800)
										return
									}
									// monster catched
									const t = setTimeout(() => {
										monsterCatched(enemy)
										clearTimeout(t)
									}, 800)
								}
							})
							clearTimeout(bt)
						}, 1100)
					}
				})
				clearTimeout(t)
			}, 900)
		}
	})
}

/**
 * This function animates the monster breaking free from the ball
 * @param enemy - The monster object that might be catched
 * @param smoke - The smoke sprite object
 */
const breakFree = (enemy: Monster, smoke: Sprite) => {
	// console.log(battleMonsters[0])
	// remove ball
	battle.renderedSprites.splice(battle.renderedSprites.length - 2, 1)
	playAudio('initBall')
	// animation
	const timeline = gsap.timeline()
	timeline.to(smoke, {
		opacity: 1,
		duration: .1
	}).to(smoke, {
		opacity: 0,
		duration: 2
	})
	// enemy opacity
	gsap.to(enemy.sprite, {
		opacity: 1,
		duration: .5,
		onComplete: () => {
			// remove smoke
			const st = setTimeout(() => {
				battle.popRenderedSprite()
				clearTimeout(st)
			}, 1000)
			enemy.sprite.brightness = enemy.status.frozen ? 250 : 100
			dialog.show(`${enemy.name} broke out of the BALL!`)
			flags.setIsAnimating(false)
			// Enemy attacks after breaking out
			reaction.queue.push(() => {
				// check for attack preventing status
				const attPreventStatus = statusSequence.hasAttackPreventingStatus(enemy)
				// check if enemy is not frozen anymore
				if (statusSequence.isThawed(enemy)) {
					attPreventStatus.isAffected = false
					dialog.show(`${enemy.name} is not FROZEN anymore!`)
					// enemy still has a damaging status
					if (reaction.queue.length > 1) {
						reaction.execute()
						reaction.queue.push(
							() => {
								// enemy can not attack due to any attack-preventing status
								if (attPreventStatus.isAffected) {
									statusSequence.get(attPreventStatus.statusName)?.({ affectedMon: enemy })
									dialog.show(`${enemy.name} is ${attPreventStatus.statusName.toUpperCase()} and can not attack!`)
									return
								}
								// else: enemy can attack
								enemy.randomAttack(battle.monsterParticipated[0])
							},
							// other status damage
							...statusSequence.generateDamagingStatusQueue(enemy, battle.monsterParticipated[0], undefined, true)
						)
						return
					}
					// is not frozen anymore and has no other damaging status
					reaction.queue.push(() => enemy.randomAttack(battle.monsterParticipated[0]))
					return
				}
				// can not attack due to status
				if (attPreventStatus.isAffected) {
					statusSequence.get(attPreventStatus.statusName)?.({ affectedMon: enemy })
					dialog.show(`${enemy.name} is ${attPreventStatus.statusName.toUpperCase()} and can not attack!`)
					return
				}
				enemy.randomAttack(battle.monsterParticipated[0])
			})
		}
	})
}

/**
 * This function handles the monster catched sequence
 * @param enemy - The monster object that is catched
 */
const monsterCatched = (enemy: Monster) => {
	flags.setIsAnimating(false)
	// catched
	enemyMonInfoEl.style.display = 'none'
	dialog.show(`WUNDERFUL! ${enemy.name} has been caught!`)
	mapAudio.wild_battle.stop()
	playAudio('wild_victory')
	// add new entry again in case of player has catched monster before gaining his monster-dex
	player.monsterDex.addNewEntry(enemy)
	// add to monster-dex as catched
	player.monsterDex.setIsCatched(enemy.idx)
	const correctQueue = !player.monsterDex.isBroken ?
		[
			() => dialog.show(`The MONSTER-DEX shows some information about ${enemy.name}!`),
			() => {
				monsterInfoEl.style.display = 'none'
				dialog.hide()
				player.monsterDex.showEntry(enemy)
				focusFirstChildAfterTimeout(dexEntryEl)
				events.lastFocusEl = dexEntryEl.querySelector('button')
			},
			() => {
				dexEntryEl.removeAttribute('style')
				const list = getElByQuery('#dexList')
				list.removeAttribute('style')
				monsterInfoEl.style.display = 'block'
				const txt = player.monsters.length < 6 ?
					`${enemy.name} is now a part of your TEAM!` :
					`${enemy.name} has been pushed into your STORAGE!`
				dialog.show(txt)
				const enemyHP = enemy.stats.IS.hp
				const enemyStatus = enemy.status
				const newMon = new Monster({
					...getMonster(formatName(enemy.name)),
					uid: enemy.uid,
					level: enemy.level,
					audio: enemy.audio,
					attacks: enemy.attacks,
					catchLvl: enemy.level,
					catchLocation: getLocationStr(player.currentMapId)
				})
				newMon.stats.IS.hp = enemyHP
				newMon.setStatus(enemyStatus)
				if (player.monsters.length < 6) {
					player.addMonster(newMon)
					return
				}
				player.inventory.addMonster(newMon)
			}
		]
		:
		[
			() => {
				const txt = player.monsters.length < 6 ?
					`${enemy.name} is now a part of your TEAM!` :
					`${enemy.name} has been pushed into your STORAGE!`
				dialog.show(txt)
				const enemyHP = enemy.stats.IS.hp
				const enemyStatus = enemy.status
				const newMon = new Monster({
					...getMonster(formatName(enemy.name)),
					uid: enemy.uid,
					level: enemy.level,
					audio: enemy.audio,
					attacks: enemy.attacks,
					catchLvl: enemy.level,
					catchLocation: getLocationStr(player.currentMapId)
				})
				newMon.stats.IS.hp = enemyHP
				newMon.setStatus(enemyStatus)
				if (player.monsters.length < 6) {
					player.addMonster(newMon)
					return
				}
				player.inventory.addMonster(newMon)
			}
		]
	reaction.queue.push(...correctQueue, endBattleAnimation)
}

/**
 * This function animates the monster evading the ball throw
 * @param enemy - The monster object that will evade the ball throw
 * @param ball - The ball sprite object
 */
const missedTarget = (enemy: Monster, ball: Sprite) => {
	const timeline = gsap.timeline()
	timeline.to(ball.position, {
		x: viewport.isSmartphone() ? canvas.element.width / 2.5 : canvas.element.width / 2,
		y: - 50,
		duration: viewport.isSmartphone() ? .8 : .6
		// over viewport move to right 
	}).to(ball.position, {
		x: enemy.sprite.position.x - 20,
		y: -50,
		duration: .5,
		onComplete: () => {
			// move enemy aside
			gsap.to(enemy.sprite.position, {
				x: enemy.sprite.position.x + 120,
				ease: Power0.easeInOut,
				duration: .7
			})
		}
		// Move down
	}).to(ball.position, {
		x: enemy.sprite.position.x + enemy.sprite.enemyWidth / 2,
		y: enemy.sprite.position.y + enemy.sprite.enemyHeight,
		duration: viewport.isSmartphone() ? 1 : .5,
		yoyo: true,
		ease: Power1.easeIn,
		onComplete: () => {
			playAudio('ballImpact')
		}
	}).to(ball.position, {
		x: viewport.isSmartphone() ? canvas.element.width : canvas.element.width - 100,
		y: viewport.isSmartphone() ? enemy.sprite.position.y - enemy.sprite.enemyHeight : - 50,
		duration: .6,
		onComplete: () => {
			gsap.to(enemy.sprite.position, {
				x: enemy.sprite.position.x - 120,
				ease: Power0.easeInOut,
				duration: .4,
				onComplete: () => {
					dialog.show(`WOW! ${enemy.name} dodged!`)
					flags.setIsAnimating(false)
					// remove ball
					battle.popRenderedSprite()
					// enemy attacks
					reaction.queue.push(() => {
						// check for attack preventing status
						const attPreventStatus = statusSequence.hasAttackPreventingStatus(enemy)
						// check if enemy is not frozen anymore
						if (statusSequence.isThawed(enemy)) {
							attPreventStatus.isAffected = false
							dialog.show(`${enemy.name} is not FROZEN anymore!`)
							// enemy still has a damaging status
							if (reaction.queue.length > 1) {
								reaction.execute()
								reaction.queue.push(
									() => {
										// enemy can not attack due to any attack-preventing status
										if (attPreventStatus.isAffected) {
											statusSequence.get(attPreventStatus.statusName)?.({ affectedMon: enemy })
											dialog.show(`${enemy.name} is ${attPreventStatus.statusName.toUpperCase()} and can not attack!`)
											return
										}
										// else: enemy can attack
										enemy.randomAttack(battle.monsterParticipated[0])
									},
									// other status damage
									...statusSequence.generateDamagingStatusQueue(enemy, battle.monsterParticipated[0], undefined, true)
								)
								return
							}
							// is not frozen anymore and has no other damaging status
							reaction.queue.push(() => enemy.randomAttack(battle.monsterParticipated[0]))
							return
						}
						// can not attack due to status
						if (attPreventStatus.isAffected) {
							statusSequence.get(attPreventStatus.statusName)?.({ affectedMon: enemy })
							dialog.show(`${enemy.name} is ${attPreventStatus.statusName.toUpperCase()} and can not attack!`)
							return
						}
						enemy.randomAttack(battle.monsterParticipated[0])
					})
				}
			})
		}
	})
}

function playBallHit(third?: boolean) {
	// play ball hit 2x or 3x
	playAudio('hitBall')
	const t = setTimeout(() => {
		playAudio('hitBall')
		clearTimeout(t)
		if (third) {
			const t1 = setTimeout(() => {
				playAudio('hitBall')
				clearTimeout(t1)
			}, 400)
		}
	}, 400)
}