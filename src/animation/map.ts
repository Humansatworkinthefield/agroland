/*
	This file includes functions that animates the transition between map and battle or map and house entries 
 */

import gsap, { Power1 } from 'gsap'
import { battle, player } from '..'
import { dialog } from '../classes/Dialog'
import { flags } from '../classes/Flags'
import type { Monster } from '../classes/Monster'
import { getMap, setMap } from '../classes/Movables'
import { NPC } from '../classes/NPC'
import { elHasStyleVal, getElByQuery } from '../util/helper'
import { locationStrEl } from '../util/html/elements'

/**
 * This function creates an flashing transition from map to battle sequence
 * using the GSAP library.
 * Once the animation is complete, it fires a function passed as argument,
 * (usually battle.start())
 * 
 */
export const animateMapToBattleTransition = ({ wildEnemy, trainer }: { wildEnemy?: Monster, trainer?: NPC }) => {
	const bgEl = getElByQuery('#transitionLayer')
	gsap.to(bgEl, {
		opacity: 1,
		repeat: 5,
		duration: .4,
		onComplete: () => {
			gsap.to(bgEl, {
				opacity: 1,
				duration: .5,
				onComplete: () => {
					// fade out to see battle background
					gsap.to(bgEl, {
						opacity: 0,
						duration: .5,
						ease: Power1.easeOut,
						onComplete: () => {
							bgEl.removeAttribute('style')
						}
					})
					if (trainer) {
						trainer.battleSprite.brightness = 5
					}
					if (wildEnemy) {
						wildEnemy.sprite.brightness = 5
					}
					battle.background.brightness = 50
					player.battleSprite.brightness = 50
					dialog.changeBgColor('#888')
					// show empty dialog on battle start
					dialog.show('')
				}
			})
			// activate battleAnimation loop for wild battle
			if (wildEnemy) {
				battle.start({ wildEnemy })
				return
			}
			// else: activate battleAnimation loop for trainer fight
			battle.start({ trainer })
		}
	})
}

export const animateMapTransition = () => {
	const bgEl = getElByQuery('#transitionLayer')
	gsap.to(bgEl, {
		opacity: 1,
		duration: .7,
		onComplete: () => {
			gsap.to(bgEl, {
				opacity: 1,
				duration: .5,
				onComplete: () => {
					gsap.to(bgEl, {
						opacity: 0,
						duration: .7
					})
				}
			})
			// show location name if not in house
			if (player.currentHouseId === 0) {
				player.showLocationStr()
			} else {
				// check if location name is displayed and remove
				if (elHasStyleVal(locationStrEl)) {
					locationStrEl.removeAttribute('style')
				}
			}
			// start house animation
			getMap().animate()
			// play house sound
			getMap().playAudio()
			flags.setIsInteracting(false)
		}
	})
}

export const animateHouseToMapTransition = () => {
	const bgEl = getElByQuery('#transitionLayer')
	gsap.to(bgEl, {
		opacity: 1,
		duration: .7,
		onComplete: () => {
			gsap.to(bgEl, {
				opacity: 1,
				duration: .5,
				onComplete: () => {
					gsap.to(bgEl, {
						opacity: 0,
						duration: .7
					})
				}
			})
			// start map animation
			setMap({ mapID: player.currentMapId })
			// show location name
			player.showLocationStr()
			getMap().animate()
			getMap().playAudio()
			flags.setIsInteracting(false)
		}
	})
}