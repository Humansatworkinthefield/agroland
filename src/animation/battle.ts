import gsap from 'gsap'
import { battle, player } from '..'
import { canvas } from '../classes/Canvas'
import { dialog } from '../classes/Dialog'
import { flags } from '../classes/Flags'
import { Monster } from '../classes/Monster'
import { reaction } from '../classes/ReactionQueue'
import { Sprite } from '../classes/Sprite'
import { viewport } from '../classes/Viewport'
import { battleMenuEl, initPlayerMonHTML, initTrainerMonHTML, switchMonster } from '../util/html/battle'
import { elHasStyleVal, getElByQuery } from '../util/helper'
import { enemyMonInfoEl, playerInfoEl, UIWrap, monsterInfoEl, attacksSection, monsterStatsEl, dexEntryEl, enemyTrainerInfoEl } from '../util/html/elements'
import { mapAudio, playAudio } from '../data/audio'
import { NPC } from '../classes/NPC'
import { statusSequence } from '../classes/Status'
import { getMap } from '../classes/Movables'
import { events } from '../classes/Events'

/**
 * This function fires an animation which fades-in the first sprites of the battle
 * from the right (enemy) and left (player) using the GSAP library.
 * Once the animation is complete, it plays the spawned monster associated sound
 * and sets a flag to let player click dialog again
 */
export const fadeInSide = (sprite: Sprite | NPC | Monster) => {
	const duration = 3
	if (sprite instanceof Monster) {
		flags.setIsAnimating(true)
		gsap.to(sprite.sprite.position, {
			x: canvas.getEnemyPosXAfterFade(sprite.sprite),
			duration,
			onComplete: () => {
				// First Wild Battle queue
				if (sprite.isEnemy) {
					if (!player.monsterDex.isBroken) {
						player.monsterDex.addNewEntry(sprite)
					}
					sprite.playAudio()
					dialog.updateInnerHTML(`A wild ${sprite.name} just appeared!`)
					// Show first monster with HP > 0 of player
					const monIdx = player.getMonsterAliveIdx()
					if (monIdx >= 0) {
						reaction.queue.push(
							() => {
								fadePlayerAway(monIdx)
							},
							// Next reactions are in the fadePlayerAway function
						)
					}
					sprite.sprite.brightness = 100
					battle.background.brightness = 100
					player.battleSprite.brightness = 100
					dialog.changeBgColor('#fafafa')
				}
				flags.setIsAnimating(false)
				enemyMonInfoEl.style.display = 'block'
				playerInfoEl.style.display = 'block'
			}
		})
	}
	if (sprite instanceof NPC) {
		flags.setIsAnimating(true)
		gsap.to(sprite.battleSprite.position, {
			x: canvas.getEnemyPosXAfterFade(sprite.battleSprite, true),
			duration,
			onComplete: () => {
				sprite.battleSprite.brightness = 100
				battle.background.brightness = 100
				player.battleSprite.brightness = 100
				dialog.changeBgColor('#fafafa')
				dialog.updateInnerHTML(`${sprite.isGym ? 'GYM LEADER ' : ''}${sprite.name} wants to fight!`)
				// fade trainer away
				reaction.queue.push(() => fadeTrainerAway(sprite))
				flags.setIsAnimating(false)
				playAudio('trainerSpawn')
				if (document.body) {
					dialog.changeBgColor('#fafafa')
					// remove invert color style
					getElByQuery('#gameWrap').removeAttribute('style')
					// add back margin due to removed attribute
					canvas.alignGameWrap()
					document.body.removeAttribute('style')
				}
				enemyTrainerInfoEl.style.display = 'block'
				playerInfoEl.style.display = 'block'
			}
		})
	}
	if (sprite instanceof Sprite) {
		gsap.to(sprite.position, {
			x: canvas.getFriendlyPosX(),
			duration
		})
	}
}

/**
 * This function animates the end of the battle and starts the map animation again
 */
export const endBattleAnimation = (trainer?: NPC) => {
	flags.setIsEndOfBattle(true)
	// prevent player from moving until animation ends
	flags.setAreKeysBlocked(true)
	// reset focus
	events.lastFocusEl = null
	mapAudio.wild_victory.stop()
	battleMenuEl.replaceChildren()
	// clear battle monsters array
	battle.clearMonsParticipated()
	getMap().detectedTrainer.moving = true
	// reset through in-battle-modified stats to initial
	player.monsters.forEach(mon => mon.resetStats())
	gsap.to('#transitionLayer', {
		opacity: 1,
		onComplete: () => {
			// cancel battle animation loop
			if (battle.animationID > 0) { cancelAnimationFrame(battle.animationID) }
			// remove evolution section if available
			const evoSection = getElByQuery('#transformSection')
			if (elHasStyleVal(evoSection)) {
				evoSection.replaceChildren()
				evoSection.removeAttribute('style')
			}
			// remove dex entry if available
			if (elHasStyleVal(dexEntryEl)) {
				dexEntryEl.removeAttribute('style')
			}
			// hide battle components
			UIWrap.style.display = 'none'
			// hide stats window
			monsterStatsEl.removeAttribute('style')
			dialog.hide()
			reaction.clearQueue()
			flags.setIsDetected(false)
			battle.setIsBattleInitiated(false)
			console.log('battle initiated: ', battle.isInitiated)
			if (trainer?.isRival || trainer?.isGym) {
				flags.setIsInteracting(true)
				trainer.sprite.animate = false
			}
			// reactivate map animation loop
			getMap().animate()
			// fade in map
			gsap.to('#transitionLayer', {
				opacity: 0,
				duration: 1.5,
				onComplete: () => {
					flags.setAreKeysBlocked(false)
					if (!battle.isInitiated) {
						// play map sound
						getMap().playAudio()
						// handle rival trainer after battle
						if ((trainer?.isRival || trainer?.isGym) && trainer.afterBattleDialog) {
							reaction.queue.push(...trainer.afterBattleDialog(trainer))
							reaction.execute()
							if (trainer.isRival) {
								reaction.queue.push(
									() => {
										dialog.hide()
										getMap().audio.pause()
										trainer.playLookAudio()
										trainer.sprite.frames.hold = 10
										trainer.shouldWalkAway = true
									}
								)
							}
						}
					}
				}
			})
		}
	})
}

/**
 * This function animates the monster fading into battle and handles the rendered sprites logic 
 * @param monster - The monster object that should fade out of the ball
 */
export const monsterFadeFromBall = (monster: Monster, trainer?: NPC, switchBeforeFaint = false, switchAfterFaint = false) => {
	flags.setIsAnimating(true)
	// add monster to renderedSprites only if previous is not in array anymore (length = 1 due to enemy still rendered)
	if (battle.renderedSprites.length === 1) {
		monster.sprite.opacity = 0
		battle.pushToRenderedSprites(monster)
	}
	ballSpawnAnimation(monster)
	// add monster that participated in battle only if not already in array
	// if in array, then take the monster to the first pos again
	const idx = battle.monsterParticipated.findIndex(x => x === monster)
	if (idx < 0) {
		battle.monsterParticipated.splice(0, 0, monster)
	}
	if (idx >= 0) {
		battle.monsterParticipated.splice(idx, 1)
		battle.monsterParticipated.splice(0, 0, monster)
	}
	// high brightness
	monster.sprite.brightness = 500
	animateBrightness(monster)
	// monster opacity animation
	gsap.to(monster.sprite, {
		opacity: 1,
		duration: 1.5,
		onComplete: () => {
			battle.renderedSprites.forEach(sprite => {
				if (sprite instanceof Monster && sprite.isEnemy) {
					initPlayerMonHTML(monster, sprite, trainer)
					// enemy attacks new monster after switch
					if (battle.monsterParticipated.length > 1 && switchBeforeFaint && !switchAfterFaint) {
						reaction.queue.push(() => {
							// check for attack preventing status
							const attPreventStatus = statusSequence.hasAttackPreventingStatus(sprite)
							// check if enemy is not frozen anymore
							if (statusSequence.isThawed(sprite)) {
								attPreventStatus.isAffected = false
								dialog.show(`${sprite.name} is not FROZEN anymore!`)
								// enemy still has a damaging status
								if (reaction.queue.length > 1) {
									reaction.execute()
									reaction.queue.push(
										() => {
											// enemy can not attack due to any attack-preventing status
											if (attPreventStatus.isAffected) {
												statusSequence.get(attPreventStatus.statusName)?.({ affectedMon: sprite })
												dialog.show(`${sprite.name} is ${attPreventStatus.statusName.toUpperCase()} and can not attack!`)
												return
											}
											// else: enemy can attack
											sprite.randomAttack(monster, trainer)
										},
										// other status damage
										...statusSequence.generateDamagingStatusQueue(sprite, monster, trainer, true)
									)
									return
								}
								// is not frozen anymore and has no other damaging status
								reaction.queue.push(() => sprite.randomAttack(monster, trainer))
								return
							}
							// can not attack due to status
							if (attPreventStatus.isAffected) {
								statusSequence.get(attPreventStatus.statusName)?.({ affectedMon: sprite })
								dialog.show(`${sprite.name} is ${attPreventStatus.statusName.toUpperCase()} and can not attack!`)
								return
							}
							sprite.randomAttack(monster, trainer)
						})
					}
				}
			})
			// FRONTEND
			// Call Player Monster Audio
			monster.playAudio()
			// reset flag to allow player click dialog
			flags.setIsAnimating(false)
			// CSS
			// console.log('show attack section of player monster')
			monsterInfoEl.style.display = 'block'
			monsterInfoEl.style.opacity = '1'
			// battle.showUI()
			attacksSection.style.gridTemplateColumns = 'repeat(2, 1fr)'
			// battleDialog.removeAttribute('style')
		}
	})
}

export const trainerMonsterFade = (monster: Monster, trainer: NPC) => {
	flags.setIsAnimating(true)
	initTrainerMonHTML(monster)
	if (battle.renderedSprites.length === 1) {
		monster.sprite.opacity = 0
		battle.renderedSprites.splice(0, 0, monster)
	}
	ballSpawnAnimation(monster)
	if (!player.monsterDex.isBroken) {
		player.monsterDex.addNewEntry(monster)
	}
	// player switches monster after enemy trainer sends out next monster
	if (player.selectedMonToSwitch) {
		reaction.queue.push(
			() => {
				if (player.selectedMonToSwitch) {
					switchMonster(player.selectedMonToSwitch, trainer, false, true)
					player.setSelectedMonToSwitch(undefined)
				}
			}
		)
	}
	// high brightness
	monster.sprite.brightness = 500
	animateBrightness(monster)
	// set the current monster of trainer
	battle.setEnemyCurrentMon(monster)
	// monster opacity animation
	gsap.to(monster.sprite, {
		opacity: 1,
		duration: 1.5,
		onComplete: () => {
			// Call Trainer Monster Audio
			monster.playAudio()
			// reset flag to allow player click dialog
			flags.setIsAnimating(false)
			// CSS
			enemyMonInfoEl.style.display = 'block'
			enemyMonInfoEl.style.opacity = '1'
		}
	})
}

export const animateWildEscape = (monster: Monster) => {
	flags.setIsAnimating(true)
	playAudio('leaveBattle')
	gsap.to(monster.sprite.position, {
		x: monster.sprite.position.x + 500,
		duration: 1.5,
		onComplete: () => {
			battle.renderedSprites.shift()
			flags.setIsAnimating(false)
		}
	})
}

export function getCenteredSmokePos(monster: Monster) {
	const monWidth = monster.isEnemy ? monster.sprite.enemyWidth : monster.sprite.image.width * viewport.getScaleFactor(true)
	const monHeight = monster.isEnemy ? monster.sprite.enemyHeight : monster.sprite.image.height * viewport.getScaleFactor(true)
	const smokeWidth = 183 * viewport.getScaleFactor(false)
	const smokeHeight = 181 * viewport.getScaleFactor(false)
	const widthDiff = monWidth - smokeWidth
	const heightDiff = monHeight - smokeHeight
	return {
		x: monster.sprite.position.x + (widthDiff / 2),
		y: monster.sprite.position.y + (heightDiff / 2)
	}
}

function animateBrightness(monster: Monster) {
	const int = setInterval(() => {
		if (monster.sprite.brightness <= 100) {
			clearInterval(int)
			return
		}
		monster.sprite.brightness -= 10
	}, 20)
}

/**
 * This function animates the battle-player sprite away from the viewport to make space for its monster
 * @param monsterIdx - The index of the next available monster of the player
 */
const fadePlayerAway = (monsterIdx: number, trainer?: NPC) => {
	// first monster spawn
	const monster = player.monsters[monsterIdx]
	flags.setIsAnimating(true)
	playerInfoEl.style.display = 'none'
	monster.sprite.opacity = 0
	battle.renderedSprites.splice(1, 0, monster)
	dialog.show(`GO! ${monster.name}!`)
	gsap.to(player.battleSprite.position, {
		x: player.battleSprite.position.x - 500,
		duration: 1.5,
		onComplete: () => {
			// remove player sprite from renderedSprites
			battle.popRenderedSprite()
			// battle.resetBattlePos()
			monsterFadeFromBall(monster, trainer)
		}
	})
}

/**
 * This function animates the npc battle-trainer sprite away from the viewport to make space for its monster
 * @param trainer - The trainer NPC object
 */
const fadeTrainerAway = (trainer: NPC) => {
	flags.setIsAnimating(true)
	const trainerMonster = trainer.monsters?.[0]
	if (!trainerMonster) { return }
	enemyTrainerInfoEl.style.display = 'none'
	dialog.show(`${trainer.name} sends out ${trainerMonster.name}!`)
	gsap.to(trainer.battleSprite.position, {
		x: trainer.battleSprite.position.x + 500,
		duration: 1.5,
		onComplete: () => {
			// remove trainer sprite from renderedSprites
			battle.renderedSprites.shift()
			trainerMonsterFade(trainerMonster, trainer)
			// player monster
			const monIdx = player.getMonsterAliveIdx()
			if (monIdx >= 0) {
				reaction.queue.push(
					() => {
						fadePlayerAway(monIdx, trainer)
					},
					// Next reactions are in the fadePlayerAway function
				)
			}
		}
	})
}

/**
 * This function animates the monster fading into battle and handles 
 * @param monster - The monster object that should fade out of the ball
 */
const ballSpawnAnimation = (monster: Monster) => {
	// create Ball Smoke Sprite
	const smoke = new Sprite({
		position: {
			x: getCenteredSmokePos(monster).x,
			y: getCenteredSmokePos(monster).y
		},
		image: { src: './imgs/ball/ballSmoke.png' },
		frames: { max: 8, hold: 8 },
		animate: true,
		scaleFactor: viewport.getTrainerScaleFactor()
	})
	// push Sprites into renderedSprites to render it in the battle animation loop
	battle.pushToRenderedSprites(smoke)
	// Play Audio
	playAudio('initBall')
	// animate opacity
	gsap.to(smoke, {
		opacity: 0,
		duration: 2,
		onComplete: () => {
			// remove sprite
			battle.popRenderedSprite()
		}
	})
}