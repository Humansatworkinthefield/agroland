import type {
	IFrames,
	IHowlOptions,
	IImageSrc,
	IBadges,
	IMonsterDexArgs,
	IMonsterStats,
	IMonsterStatus,
	IPosition
} from './classes'
import type { IAttack } from './functions'

export interface ISavedPlayer {
	name: string
	monsters: ISavedMonster[]
	items: ISavedItem[]
	badges: IBadges
	monsterDex: IMonsterDexArgs
	inventory: ISavedInventory
	satoshis: number
	playtime: number
	moved: boolean
	location: number
	currentHouseId: number
	currentMapId: number
	lastHospitalData?: {
		mapID: number
		offset: IPosition
	}
}
export interface ISavedItem {
	count: number
	name: string
	recoverHP?: number
	catchProbability?: number
}
export interface ISavedMonster {
	idx: number
	uid: number
	name: string
	attacks: IAttack[]
	stats: IMonsterStats
	status: IMonsterStatus
	type: number[]
	level: number
	maxHP: number
	xp: number
	catchLvl: number,
	catchLocation: string
}
export interface ISavedCanvas {
	width: number
	height: number
}
export interface ISavedData {
	mapData: {
		[key: string]: ISavedMapData
	}
}
export interface ISavedBoundary {
	id: number
	position: IPosition
	width: number
	height: number
}
export interface ISavedNPC {
	id: number
	name: string
	image: IImageSrc
	animate: boolean
	boundary: {
		id: number
		position: IPosition
		width: number
		height: number
	},
	headImgs?: {
		up: IImageSrc
		right: IImageSrc
		down: IImageSrc
		left: IImageSrc
	}
	rivalFirstMonIdx: number
	position: IPosition
	initialStaticPos: IPosition
	isTrainer: boolean
	isRival?: boolean
	isBlocker?: boolean
	movesAround?: boolean
	isBeaten?: boolean
	itemGift?: ISavedItem
	monsters?: ISavedMonster[]
	items?: ISavedItem[]
	frames: IFrames
}
interface ISavedMapData {
	audio: IHowlOptions
	tilesCount: IPosition
	offset: {
		smartphone: IPosition
		desktop: IPosition
	}
	boundaries: ISavedBoundary[]
	npcs: ISavedNPC[]
	houses: {
		id: number
		tilesCount: IPosition
		offset: {
			smartphone: IPosition
			desktop: IPosition
		}
		boundaries: ISavedBoundary[]
		npcs: ISavedNPC[]
	}[]
}
interface ISavedInventory {
	items: ISavedItem[]
	monsters: ISavedMonster[]
}