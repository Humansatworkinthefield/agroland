import type { Howl } from 'howler'
import type { allAttacks } from '../classes/Attack'
import type { Item } from '../classes/Items/Item'
import type { Monster } from '../classes/Monster'
import type { NPC } from '../classes/NPC'
import { initialStatus } from '../data/monster/allMonsters'
import type { MonsterType } from '../data/monster/types'
import type { IFrames, IHowlOptions, IImageSrc, IPosition, ISpriteDirections } from './classes'

// eslint-disable-next-line @typescript-eslint/ban-types
export type TAttackSequences = { [k in keyof typeof allAttacks]?: Function } & { [k: string]: Function }
// eslint-disable-next-line @typescript-eslint/ban-types
export type TStatusSequence = { [k in keyof typeof initialStatus]?: Function } & { [k: string]: Function }
// TODO update from type [key: string] to [key: MonsterNames]
export interface IMonsterAudio {
	[key: string]: Howl
}
export interface IAttack {
	sequence?: (
		monster: Monster,
		recipient: Monster,
		healthBar: string,
		result: IDamageResult
	) => void
	name: string
	power: number
	type: MonsterType
	maxExec: number
	remExec: number
	acc: number
	criticalChance: number
	isSpecial: boolean
	info: string
}
export interface IAttackParams {
	attack: IAttack
	recipient: Monster
}
export interface IDamageResult {
	damage: number
	isCritical: boolean
	type1: number
	type2: number
}
export interface IIsCatched {
	brokeFree: boolean
	ballShakeCount: number
}
export interface IMonListParams {
	enemy?: Monster
	forItem?: boolean
	inMap?: boolean
	switchBeforeFaint?: boolean
	switchAfterFaint?: boolean
	trainer?: NPC
	nextMonster?: Monster
}
export interface ITiledMap {
	collisions: Uint8Array
	tilesCount: IPosition
}
export interface INPCMap {
	collisions: Uint8Array
	tilesCount: IPosition
	offset: IPosition
	mapID: number
	houseID?: number
}
export type TNonTrainerNPCs = {
	[key: string]: TNonTrainerNPC
}
export type TTrainerNPCs = {
	[key: string]: TTrainerNPC
}
export type TInitialNPC = {
	id: number
	name: string
	image: IImageSrc
	isMovingAround?: boolean
	audio?: {
		look?: IHowlOptions
		battle: IHowlOptions
		victory: IHowlOptions
	}
	animate: boolean
	frames: IFrames
	firstDialog: (npc: NPC) => (() => void)[]
	sprites?: ISpriteDirections
	battleSpriteSrc?: IImageSrc
	itemGift?: Item
	secondDialog?: (npc: NPC) => (() => void)[]
	winDialog?: string
	looseDialog?: string
	afterBattleDialog?: (npc: NPC) => (() => void)[]
	effort?: number
	isTrainer?: boolean
	isBlocker?: boolean
	isBeaten?: boolean
	isGym?: boolean
	items?: Item[]
	monsters?: Monster[]
	maxRange?: number
}
export type TAllNPCs = {
	id: number
	npcs: TInitialNPC[]
	houses: {
		[key: string]: {
			npcs: TInitialNPC[]
		}
	}
}[]
export interface IStatusSequenceParams {
	affectedMon: Monster,
	attacker: Monster,
	withDamage: boolean,
	trainer?: NPC
}
export interface IAttackPreventingSequence {
	affectedMon: Monster
	showDialog?: boolean
}
type TNonTrainerNPC = {
	name: string
	image: IImageSrc
	headImgs?: {
		up: IImageSrc
		right: IImageSrc
		down: IImageSrc
		left: IImageSrc
	}
	audio?: {
		look: IHowlOptions
		battle: IHowlOptions
		victory: IHowlOptions
	}
	sprites?: ISpriteDirections
}
type TTrainerNPC = {
	name: string
	image: IImageSrc
	audio: {
		look?: IHowlOptions
		battle: IHowlOptions
		victory: IHowlOptions
	}
	sprites: ISpriteDirections
	battleSpriteSrc: IImageSrc
	effort: number
	isTrainer: boolean
	isRival?: boolean
	isGym: boolean
}