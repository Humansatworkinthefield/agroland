export function cTo<T>(json?: string | null) {
	if (!json || json?.length < 2) { return null }
	try {
		// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
		const thing: T = JSON.parse(json)
		return thing
	} catch (e) { console.error(e) }
	return null
}
export function toJsonStr<T>(thing: T) { try { return JSON.stringify(thing) } catch (e) { return null } }

// export const isKeyof = <T>(o: T, k: PropertyKey): k is keyof T => k in o