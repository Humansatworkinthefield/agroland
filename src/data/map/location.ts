export function getLocationStr(id: number) { return location[id] }

const location: { [key: string]: string } = {
	20: 'Snab Town',
	21: 'Andro Town',
	22: 'Snab Forest',
	23: 'Canis Town'
}