import { IInteractions } from '../models/classes'

export const interactions: IInteractions = {
	// SNAB-TOWN
	20: {
		map: {
			200: {
				txt: 'Entry to ANDRO-TOWN...',
				direction: ['w', 'a']
			},
			201: {
				txt: 'SNAB-TOWN, 300 steps away',
				direction: ['w', 'a']
			},
			202: {
				txt: 'Entry to SNAB-FOREST',
				direction: ['w']
			},
			203: {
				txt: 'This door is LOCKED...!',
				direction: ['w']
			}
		},
		house: {
			// House IDs
			5: {
				200: {
					txt: 'A bunch of books about law, psychology and genetic engineering...',
					direction: ['w']
				},
				201: {
					txt: 'Looks comfortable but I have no time to rest now!',
					direction: ['w']
				},
				202: {
					txt: 'What an ugly frame...',
					direction: ['w']
				},
				204: {
					txt: 'My good old fakemon MANGAS!',
					direction: ['w', 'a']
				},
				205: {
					txt: 'My whiteboard will stay empty for a while now...',
					direction: ['w']
				},
			},
			6: {
				200: {
					txt: 'I don\'t know what this is...',
					direction: ['w', 'a']
				},
				201: {
					txt: 'This PC is secured with a PASSWORD!',
					direction: ['a']
				},
				202: {
					txt: 'Many books about the FAKEMON culture...',
					direction: ['w']
				}
			},
			7: {
				200: {
					txt: 'Magazines about medication and FAKEMON lifestyle.',
					direction: ['a']
				},
				201: { // placeholder for hospital healing interaction
					txt: 'Welcome to the MONSTER HOSPITAL!',
					direction: ['w']
				},
				202: {
					txt: 'Store and take MONSTER coming soon...',
					direction: ['w']
				},
				203: {
					txt: 'A printer? Let\'s focus on the important things!',
					direction: ['w']
				},
			},
			8: {
				200: {
					txt: 'Cool and fresh water for customers. It\'s delicious!',
					direction: ['s']
				},
				201: {
					txt: 'Welcome to our SHOP! Currently under developement. You will be able to buy stuff here soon...',
					direction: ['a']
				},
				202: {
					txt: 'A lot of ITEMs to help a TRAINER and their MONSTER!',
					direction: ['w']
				},
				203: {
					txt: 'Everything is sorted precisely!',
					direction: ['w']
				},
				204: {
					txt: 'A collection of unhealthy SODA. I will stick to WATER!',
					direction: ['w']
				},
				205: {
					txt: 'A collection of unhealthy SODA. I will stick to WATER!',
					direction: ['w']
				},
				206: {
					txt: 'So many differents BALLs to catch MONSTERs, one cooler than the other!',
					direction: ['w']
				},
			},
			9: {},
			// forest house
			10: {
				200: {
					txt: 'A lot of foreign DIRTY FIAT PAPER MONEY! Disgusting! I better not take it with me...',
					direction: ['w']
				},
				201: {
					txt: 'Looks like somebody is going to travel...',
					direction: ['s', 'a']
				},
			},
			11: {}
		}
	},
	// ANDRO-TOWN
	21: {
		map: {
			200: {
				txt: 'ANDRO TOWN GYM - LEADER: UNKNOWN',
				direction: ['w']
			},
		},
		house: {}
	},
	// SNAB-FOREST
	22: {
		map: {
			200: {
				txt: 'SNAB-FOREST',
				direction: ['w']
			},
			201: {
				txt: 'SNAB-FOREST EXIT IN 100 STEPS',
				direction: ['w']
			},
			202: {
				txt: 'EXIT SNAB-FOREST',
				direction: ['w']
			},
		},
		house: {}
	},
	// CANIS-TOWN
	23: {
		map: {
			200: {
				txt: 'ROUTE 21 UNDER CONSTRUCTION',
				direction: ['d']
			},
			201: {
				txt: 'ROUTE 21 UNDER CONSTRUCTION',
				direction: ['w']
			},
			202: {
				txt: 'CANIS TOWN GYM - LEADER: PETER SNIFF',
				direction: ['w']
			},
		},
		house: {
			7: {
				200: {
					txt: 'Magazines about medication and FAKEMON lifestyle.',
					direction: ['a']
				},
				201: { // placeholder for hospital healing interaction
					txt: 'Welcome to the MONSTER HOSPITAL!',
					direction: ['w']
				},
				202: {
					txt: 'Store and take MONSTER coming soon...',
					direction: ['w']
				},
				203: {
					txt: 'A printer? Let\'s focus on the important things!',
					direction: ['w']
				},
			},
			8: {
				201: {
					txt: 'Welcome to our SHOP! Currently under developement. You will be able to buy stuff here soon...',
					direction: ['a']
				},
				204: {
					txt: 'A collection of unhealthy SODA. I will stick to WATER!',
					direction: ['w']
				},
				205: {
					txt: 'A collection of unhealthy SODA. I will stick to WATER!',
					direction: ['w']
				},
			},
		}
	}
}
