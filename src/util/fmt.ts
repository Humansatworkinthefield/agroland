import { TMonsterName } from '../data/monster/allMonsters'

export const formatName = (name?: string) => {
	if (!name || !name.length) { throw new Error(`Invalid name: ${name ? name : 'NAME MISSING'}`) }
	return ((name[0].toUpperCase() + name.substring(1).toLowerCase()).replaceAll(' ', '_')) as TMonsterName
}
export const formatMonIdx = (idx: number, forEntry = true) =>
	`${forEntry ? '- ' : ''}${(idx + 1).toString().padStart(3, '0')}${forEntry ? ' -' : ''}`