import { events } from '../classes/Events'
import { Ball } from '../classes/Items/Ball'
import { Item } from '../classes/Items/Item'
import { Potion } from '../classes/Items/Potion'
import { RareCandy } from '../classes/Items/RareCandy'
import { Monster } from '../classes/Monster'
import { TMonsterName } from '../data/monster/allMonsters'
import { getMonsterTypeNameById } from '../data/monster/types'
import { IAttack } from '../models/functions'

export function assertNonNullish<T>(v: T, msg: string): asserts v is NonNullable<T> {
	if (v === null || v === undefined) { throw Error(msg) }
}

export function getGrowthStr(cat: number) {
	if (cat === 1) {
		return 'Slow'
	}
	if (cat === 2) {
		return 'Medium-Slow'
	}
	if (cat === 3) {
		return 'Medium-Fast'
	}
	if (cat === 4) {
		return 'Fast'
	}
	return undefined
}

export function getRandomInt(min: number, max: number) {
	min = Math.ceil(min)
	max = Math.floor(max)
	return Math.floor(Math.random() * (max - min + 1)) + min
}

export function clearArr<T extends Array<U>, U>(array: T) {
	array.length = 0
}

export function getElByQuery<T extends HTMLElement>(query: string) {
	const el = document.querySelector<T>(query)
	assertNonNullish(el, `Element "${query}" not found!`)
	if (!(el instanceof HTMLElement)) { throw new Error(`HTML ELement '${query}' has the wrong type`) }
	return el
}

// export const playAfterTimeOut = (audio: Howl, ms: number) => {
// 	const t = setTimeout(() => {
// 		audio.play()
// 		clearTimeout(t)
// 	}, ms)
// }

export function getUniqueID() {
	return Math.floor(Math.random() * Date.now())
}

export function newItemInstance(item: Item, count?: number) {
	if (item.name === 'BALL') {
		return new Ball({ name: item.name, count: count || item.count })
	}
	if (item.name === 'RARE CANDY') {
		return new RareCandy({ name: item.name, count: count || item.count })
	}
	return new Potion({ name: item.name, count: count || item.count })
}

const spriteDirections = ['up', 'right', 'down', 'left'] as const
export function getRandomSpriteDirection() {
	return spriteDirections[getRandomInt(0, spriteDirections.length - 1)] as typeof spriteDirections[0]
}

export function getMonsterNameEffectiveType(playerFirstMonName: TMonsterName) {
	if (playerFirstMonName === 'Draggle') { return 'Sheely' }
	if (playerFirstMonName === 'Sheely') { return 'Emby' }
	return 'Draggle'
}

export function focusFirstChildAfterTimeout(element: HTMLElement) {
	const t = setTimeout(() => {
		element.querySelector('button')?.focus()
		events.lastFocusEl = element.querySelector('button')
		clearTimeout(t)
	}, 50)
}

export function isValidName(name: string | null) {
	if (!name) { return false }
	const regex = /[`!@#$%^&*()_+\-=[\]{};':"\\|,.<>/?~]/
	if (regex.test(name)) { return false }
	if (name.length < 2 || name.length > 8) { return false }
	return true
}

export function removeArrEntry(arr: unknown[], idx: number) {
	if (idx < 0) { return }
	arr[idx] = arr[arr.length - 1]
	arr.pop()
}

export function elHasClassName(el: Element | HTMLElement | null, className: string) {
	if (!el) { return false }
	return el.classList.contains(className)
}

export function elHasStyleVal(el: HTMLElement | null) {
	const values = [
		'display: grid;',
		'display: block;',
		'display: flex;',
	]
	for (let i = 0; i < values.length; i++) {
		const val = values[i]
		if (el?.getAttribute('style')?.includes(val)) { return true }
	}
	return false
}

export function getTypesStr({ monster, attack }: { monster?: Monster, attack?: IAttack }) {
	let types = ''
	if (attack) {
		return getMonsterTypeNameById(attack.type)
	}
	if (monster) {
		for (let i = 0; i < monster.type.length; i++) {
			const type = getMonsterTypeNameById(monster.type[i])
			types += `${type.toUpperCase()}${i < monster.type.length - 1 ? ', ' : ''}`
		}
	}
	return types
}