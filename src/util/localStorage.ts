import { getMonster, getMonsterNameByIdx } from '../data/monster/allMonsters'
import { Ball } from '../classes/Items/Ball'
import { Boundary } from '../classes/Boundary'
import { Inventory } from '../classes/Inventory'
import { Monster } from '../classes/Monster'
import { MonsterDex } from '../classes/MonsterDex'
import { NPC } from '../classes/NPC'
import { Potion } from '../classes/Items/Potion'
import { monsterAudio } from '../data/audio'
import { allNPCs } from '../data/npcEntries'
import type { IAllData, IPosition } from '../models/classes'
import type { ISavedBoundary, ISavedCanvas, ISavedData, ISavedItem, ISavedMonster, ISavedNPC, ISavedPlayer } from '../models/dataSaved'
import { cTo } from '../models/typeGuards'
import { formatName } from './fmt'
import { mapData } from '../data/map/mapData'
import { TInitialNPC, INPCMap, ITiledMap } from '../models/functions'
import { canvas } from '../classes/Canvas'
import { viewport } from '../classes/Viewport'
import { config } from '../config'

// local storage helper
export function getFromLocalStorage<T>(key: string, prefix?: string) {
	if (!key || !localStorage) { return null }
	try {
		const resp = localStorage.getItem(prefix ? `${prefix}||${key}` : key)
		// console.log(resp)
		if (!resp) { return null }
		// console.log('local storage response: ', resp)
		const jsonResp: T | null = cTo<T>(resp)
		// console.log({ jsonResp })
		return jsonResp ? jsonResp : resp
	} catch (e) { console.log(e) }
	return null
}
export function setLocalStorage(key: string, val: string, prefix?: string) {
	// console.log({ val })
	if (!key || !localStorage) { return null }
	try { return localStorage.setItem(prefix ? `${prefix}||${key}` : key, val) } catch (e) { console.log(e) }
	return null
}
export function removeFromLocalStorage(key: string, prefix?: string) {
	if (!key || !localStorage) { return null }
	try { return localStorage.removeItem(prefix ? `${prefix}||${key}` : key) } catch (e) { console.log(e) }
	return null
}
export function getAllKeysByPrefixFromLocalStorage(prefix: string) {
	return getAllKeysFromLocalStorage()?.filter(x => x?.trim() && x?.startsWith(prefix))
}
export function getAllKeysFromLocalStorage() {
	if (!localStorage) { return null }
	const keys = []
	for (let i = 0; i < localStorage.length; i++) { keys.push(localStorage.key(i)) }
	return keys.filter(x => x?.trim())
}

// saved progress
export function hasProgress() {
	const savedPlayer = getFromLocalStorage<ISavedPlayer>('player')
	if (!savedPlayer || typeof savedPlayer === 'string') { return false }
	return true
}
export function getSavedPlayer() {
	const savedPlayer = getFromLocalStorage<ISavedPlayer>('player')
	if (!savedPlayer || typeof savedPlayer === 'string') {
		// return initial player
		return getInitialPlayerObj()
	}
	// else: return saved player
	return {
		...savedPlayer,
		monsters: convertSavedMons(savedPlayer.monsters),
		items: convertSavedItems(savedPlayer.items),
		monsterDex: MonsterDex.newMonsterDex(savedPlayer.monsterDex),
		inventory: Inventory.newInventory({
			items: convertSavedItems(savedPlayer.inventory.items),
			monsters: convertSavedMons(savedPlayer.inventory.monsters)
		}),
	}
}
export function getSavedCanvasSizes() {
	const savedCanvas = getFromLocalStorage<ISavedCanvas>('canvas')
	if (!savedCanvas || typeof savedCanvas === 'string') {
		return
	}
	return savedCanvas
}
export function getSavedData() {
	const data = getFromLocalStorage<ISavedData>('mapdata')
	// console.log('data in getSavedData: ', data)
	if (!data || typeof data === 'string') {
		// return initial boundary objects and npc objects created with initial mapData
		return createAllBoundaries()
	}
	// return saved boundary and npc objects
	return convertAllMapData(data)
}
export function getInitialPlayerObj() {
	return {
		name: 'PLAYER',
		badges: {
			'Ground': { inPossession: false, img: { src: './imgs/badges/ground.png' } },
			'Plant': { inPossession: false, img: { src: './imgs/badges/grass.png' } },
			'Water': { inPossession: false, img: { src: './imgs/badges/water.png' } },
			'Fire': { inPossession: false, img: { src: './imgs/badges/fire.png' } },
			'Lightning': { inPossession: false, img: { src: './imgs/badges/thunder.png' } },
			'Psy': { inPossession: false, img: { src: './imgs/badges/psy.png' } },
			'Ghost': { inPossession: false, img: { src: './imgs/badges/ghost.png' } },
			'Master': { inPossession: false, img: { src: './imgs/badges/master.png' } },
		},
		monsters: !config.playerWithMonster ? [] : config.playerMonster.map(mon => new Monster({
			...getMonster(getMonsterNameByIdx(mon.name)),
			uid: 0,
			audio: monsterAudio[getMonsterNameByIdx(mon.name)],
			level: mon.lv,
		})),
		items: [],
		monsterDex: MonsterDex.newMonsterDex({
			available: config.dex.available,
			isBroken: config.dex.isBroken,
			entries: config.dex.entries.map(entry => ({
				idx: entry.idx,
				name: getMonsterNameByIdx(entry.idx),
				catched: true
			}))
		}),
		inventory: Inventory.newInventory({
			items: [
				new Potion({ name: 'Potion', count: 1 }),
				// new Ball({ name: 'Ball', count: 50 })
			],
			monsters: []
		}),
		location: 20, // used to show current location string in map 
		currentHouseId: config.playerHouseID,
		currentMapId: config.playerMapID
	}
}

// boundary helpers
export function createBoundaryArray({ collisions, tilesCount, offset, mapID, houseID }: INPCMap) {
	// get npc needed from map or from houses in map
	let initialNPCs: TInitialNPC[]
	const map = allNPCs.find(map => map.id === mapID)
	if (map) {
		if (houseID) {
			initialNPCs = map.houses[houseID].npcs
		} else {
			initialNPCs = map.npcs
		}
	}
	// const initialNPCs = houseID ? allNPCs[mapID].houses[houseID].npcs : allNPCs[mapID].npcs
	// boundary size
	const size = (houseID ? 64 : 48) * viewport.getScaleFactor(false)
	// create 2d array for rendering collision boundaries
	const twoDArray = create2DMap({ collisions, tilesCount })
	// boundaries array from collisionsMap
	const boundaryArray: Boundary[] = []
	// npc array from collisionsMap
	const npcArray: NPC[] = []
	twoDArray.forEach((row, i) => {
		row.forEach((id, j) => {
			// npc position
			const position = {
				x: j * size + offset.x,
				y: i * size + offset.y - (16 * viewport.getScaleFactor(false))
			}
			// create all boundary objects
			const boundary = new Boundary({
				id,
				position: {
					x: id === 4 ? (j * size + offset.x + size / 2) : (j * size + offset.x),
					y: i * size + offset.y
				},
				width: getCorrectBoundarySize({ id, width: true, standardSize: size }),
				height: getCorrectBoundarySize({ id, standardSize: size })
			})
			if ((id > 0 && id < 100) || id > 199) {
				boundaryArray.push(boundary)
			}
			// create all npc objects
			if (id > 99 && id < 200) {
				const npc = initialNPCs.find(npc => npc.id === id)
				if (!npc) { return }
				npcArray.push(
					new NPC({
						...npc,
						id,
						position,
						boundary
					})
				)
			}
		})
	})
	return { boundaryArray, npcArray }
}
export function calcOffset(offset: IPosition) {
	// if (window.innerWidth >= 1024) { return offset }
	const hardWidth = 360
	const hardHeight = 292
	const hardOffsetX = offset.x
	const hardOffsetY = offset.y
	const offsetXDiff = (canvas.element.width - hardWidth) / 2
	const offsetYDiff = (canvas.element.height - hardHeight) / 2
	const newOffsetX = hardOffsetX + offsetXDiff
	const newOffsetY = hardOffsetY + offsetYDiff
	return { x: newOffsetX, y: newOffsetY }
}

// convert local storage to classes
function convertSavedItems(savedItems: ISavedItem[]) {
	const itemInstances = savedItems.map(item => convertSavedSingleItem(item))
	return itemInstances
}
function convertSavedSingleItem(item: ISavedItem) {
	if (Object.prototype.hasOwnProperty.call(item, 'recoverHP')) {
		return new Potion({ name: item.name, count: item.count })
	}
	return new Ball({ name: item.name, count: item.count })
}
function convertAllMapData(allData: ISavedData) {
	const mapData = allData.mapData
	for (let i = 0; i < Object.entries(mapData).length; i++) {
		const mapID = Object.entries(mapData)[i][0]
		const map = Object.entries(mapData)[i][1]
		map.boundaries = convertSavedBoundaries(map.boundaries)
		map.npcs = convertSavedNPCs(map.npcs, +mapID)
		for (let j = 0; j < map.houses.length; j++) {
			const house = map.houses[j]
			house.boundaries = convertSavedBoundaries(house.boundaries)
			house.npcs = convertSavedNPCs(house.npcs, +mapID, house.id)
		}
	}
	return mapData as unknown as IAllData
}
function convertSavedBoundaries(boundaries: ISavedBoundary[]) {
	return boundaries.map(boundary => new Boundary({ ...boundary }))
}
function convertSavedNPCs(npcs: ISavedNPC[], mapID: number, houseID?: number) {
	// get npc needed from map or from houses in map
	let initialNPCs: TInitialNPC[]
	const map = allNPCs.find(map => map.id === mapID)
	if (map) {
		if (houseID) {
			initialNPCs = map.houses[houseID].npcs
		} else {
			initialNPCs = map.npcs
		}
	}
	const npcInstances = npcs.map(npc => {
		const initialNPCIdx = initialNPCs.findIndex(initNPC => initNPC.id === npc.id)
		return new NPC({
			...initialNPCs[initialNPCIdx],
			name: npc.name,
			audio: initialNPCs[initialNPCIdx].audio,
			image: { src: npc.isBeaten ? `./imgs/npcs/trainers/${npc.isRival ? 'Rival' : formatName(npc.name)}/idle.png` : npc.image.src },
			headImgs: npc.headImgs,
			animate: npc.animate,
			frames: npc.frames,
			position: npc.position,
			initialStaticPos: npc.initialStaticPos,
			isMovingAround: npc.movesAround,
			boundary: new Boundary({ ...npc.boundary }),
			isBeaten: npc.isBeaten,
			rivalFirstMonIdx: npc.rivalFirstMonIdx,
			isBlocker: npc.isBlocker,
			itemGift: npc.itemGift && convertSavedSingleItem(npc.itemGift),
			monsters: npc.monsters && convertSavedMons(npc.monsters, true),
			items: npc.items && convertSavedItems(npc.items),
		})
	})
	return npcInstances as unknown as ISavedNPC[]
}
function convertSavedMons(savedMons: ISavedMonster[], isEnemy = false) {
	const monsterInstances = savedMons.map(monster => new Monster({
		...getMonster(formatName(monster.name)),
		audio: monsterAudio[formatName(monster.name)],
		...monster,
		savedStats: monster.stats,
		isEnemy
	}))
	return monsterInstances
}

// boundary helpers
function createAllBoundaries() {
	const maps: IAllData = {}
	for (let i = 0; i < Object.entries(mapData).length; i++) {
		const mapID = Object.entries(mapData)[i][0]
		const map = Object.entries(mapData)[i][1]
		const mapBoundaries = createBoundaryArray({
			collisions: map.collisions,
			tilesCount: map.tilesCount,
			offset: calcOffset(viewport.isSmallDevice() ? map.offset.smartphone : map.offset.desktop),
			mapID: +mapID
		})
		maps[mapID] = {
			tilesCount: map.tilesCount,
			offset: {
				smartphone: calcOffset(map.offset.smartphone),
				desktop: calcOffset(map.offset.desktop)
			},
			audio: map.audio,
			boundaries: mapBoundaries.boundaryArray,
			npcs: mapBoundaries.npcArray,
			houses: []
		}
		for (let j = 0; j < map.houses.length; j++) {
			const house = map.houses[j]
			const houseBoundaries = createBoundaryArray({
				collisions: house.collisions,
				tilesCount: house.tilesCount,
				offset: calcOffset(viewport.isSmallDevice() ? house.offset.smartphone : house.offset.desktop),
				mapID: +mapID,
				houseID: house.id
			})
			maps[mapID].houses.push({
				id: house.id,
				tilesCount: house.tilesCount,
				offset: {
					smartphone: calcOffset(house.offset.smartphone),
					desktop: calcOffset(house.offset.desktop)
				},
				audio: house.audio,
				boundaries: houseBoundaries.boundaryArray,
				npcs: houseBoundaries.npcArray
			})
		}
	}
	return maps
}
function getCorrectBoundarySize({ id, width, standardSize }: { id: number, width?: boolean, standardSize: number }) {
	// for NPCs in houses, always 48*48
	if (id > 99 && id < 200) { return 48 * viewport.getScaleFactor(false) }
	// for an outside-boundary with ID 2 or 4 (right or left), return a half-width 24
	if (width && (id === 2 || id === 4)) { return standardSize / 2 }
	// for an outside-boundary with ID 3 (top), return a half-height 24
	if (!width && id === 3) { return standardSize / 2 }
	// else: standard size
	return standardSize
}
function create2DMap({ collisions, tilesCount }: ITiledMap) {
	const arr: Uint8Array[] = []
	for (let i = 0; i < collisions.length; i += tilesCount.x) {
		arr.push(collisions.slice(i, i + tilesCount.x))
	}
	return arr
}
