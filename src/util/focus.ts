import { dialog } from '../classes/Dialog'
import { events } from '../classes/Events'
import { playAudio } from '../data/audio'
import { elHasClassName, elHasStyleVal, getElByQuery } from './helper'
import { updateAttackInfo } from './html/battle'

export function changeFocus({ direction }: { direction: string }) {
	const activeEl = document.activeElement as HTMLElement | null
	if (!activeEl || activeEl.id === 'appBody') { return }
	console.log({ activeEl })
	// get parent of focused element
	const focusParent = activeEl.parentElement
	if (!focusParent) { return }
	console.log({ focusParent })
	if (direction === 'w') {
		playAudio('pressab')
		// handle credits page link focus
		handleCreditsFocus({ direction: 'w', activeEl })
		// handle count increase
		if (countBtnHasFocus()) {
			updateInput({ add: true })
			return
		}
		// monster list
		if (isMonListFocus(focusParent) || activeEl.id === 'cancelMonSwitch') {
			handleMonListFocus(activeEl, focusParent, 'w')
			return
		}
		// battle UI
		if (isBattleUIFocus(focusParent)) {
			handleBattleUIFocus(activeEl, focusParent, 'w')
			return
		}
		// handle top-down element sibling focus
		if (hasOnlyTopDownChildren(focusParent)) {
			focusPreviousSibling(activeEl, focusParent)
			if (elHasStyleVal(getElByQuery('#forgetAttackUI'))) {
				updateAttackInfo(document.activeElement as HTMLButtonElement)
			}
		}
		return
	}
	if (direction === 'a') {
		// early return
		if (hasOnlyTopDownChildren(focusParent)) {
			return
		}
		playAudio('pressab')
		// battle UI
		if (isBattleUIFocus(focusParent)) {
			handleBattleUIFocus(activeEl, focusParent, 'a')
			return
		}
		// handle left-right element sibling focus
		if (hasOnlyLeftRightChildren(focusParent) || isMonListFocus(focusParent)) {
			focusPreviousSibling(activeEl, focusParent)
		}
		return
	}
	if (direction === 's') {
		playAudio('pressab')
		// handle credits page link focus
		handleCreditsFocus({ direction: 's', activeEl })
		// handle count decrease
		if (countBtnHasFocus()) {
			updateInput({ add: false })
			return
		}
		// monster list
		if (isMonListFocus(focusParent) || activeEl.id === 'cancelMonSwitch') {
			handleMonListFocus(activeEl, focusParent, 's')
			return
		}
		// battle UI
		if (isBattleUIFocus(focusParent)) {
			handleBattleUIFocus(activeEl, focusParent, 's')
			return
		}
		// handle top-down element sibling focus
		if (hasOnlyTopDownChildren(focusParent)) {
			focusNextSibling(activeEl, focusParent)
			if (elHasStyleVal(getElByQuery('#forgetAttackUI'))) {
				updateAttackInfo(document.activeElement as HTMLButtonElement)
			}
		}
		return
	}
	if (direction === 'd') {
		// early return
		if (hasOnlyTopDownChildren(focusParent)) {
			return
		}
		playAudio('pressab')
		// battle UI
		if (isBattleUIFocus(focusParent)) {
			handleBattleUIFocus(activeEl, focusParent, 'd')
			return
		}
		// handle left-right element sibling focus
		if (hasOnlyLeftRightChildren(focusParent) || isMonListFocus(focusParent)) {
			focusNextSibling(activeEl, focusParent)
		}
	}
}

function hasOnlyTopDownChildren(focusParent: HTMLElement) {
	return focusParent.id === 'pauseMenu' ||
		focusParent.id === 'QAWrap' ||
		focusParent.id === 'monsterList' ||
		focusParent.id === 'itemList' ||
		focusParent.id === 'useDropItemWrap' ||
		focusParent.id === 'inventoryPC' ||
		focusParent.id === 'inventoryItems' ||
		focusParent.id === 'inventoryPlayerBag' ||
		focusParent.id === 'dexListWrap' ||
		focusParent.id === 'gameMenuWrap' ||
		focusParent.id === 'forgetAttackBtns'
}

function hasOnlyLeftRightChildren(focusParent: HTMLElement) {
	return focusParent.id === 'dropCountBtns' ||
		focusParent.id === 'statusTabBtnsWrap' ||
		focusParent.id === 'dexTabBtnsWrap' ||
		focusParent.id === 'confirmNewGameBtns'
}

function handleCreditsFocus({ direction, activeEl }: { direction: string, activeEl: HTMLElement }) {
	if (!elHasClassName(activeEl, 'linkFocus')) { return }
	const links = document.getElementsByClassName('linkFocus') as HTMLCollectionOf<HTMLElement>
	if (!links) { return }
	for (let i = 0; i < links.length; i++) {
		const link = links[i]
		// jump to last link if first link is active and direction = w 
		if (activeEl === links[0] && direction === 'w') {
			const nextFocus = links[links.length - 1]
			nextFocus.focus()
			events.lastFocusEl = nextFocus
			// scroll to bottom
			nextFocus.parentElement?.parentElement?.scrollTo(0, nextFocus.parentElement?.parentElement?.scrollHeight)
			break
		}
		// jump to first link if last link is active and direction = s
		if (activeEl === links[links.length - 1] && direction === 's') {
			const nextFocus = links[0]
			nextFocus.focus()
			events.lastFocusEl = nextFocus
			// scroll to top
			nextFocus.parentElement?.parentElement?.scrollTo(0, 0)
			break
		}
		// focus down
		if (activeEl === link && direction === 's') {
			const nextFocus = links[i + 1]
			nextFocus.focus()
			events.lastFocusEl = nextFocus
			if (i + 1 === links.length - 1) {
				// last el, scroll to bottom
				nextFocus.parentElement?.parentElement?.scrollTo(0, nextFocus.parentElement?.parentElement?.scrollHeight)
			}
			break
		}
		// focus up
		if (activeEl === link && direction === 'w') {
			const nextFocus = links[i - 1]
			nextFocus.focus()
			events.lastFocusEl = nextFocus
			if (i - 1 === 0) {
				// first el, scroll to top
				nextFocus.parentElement?.parentElement?.scrollTo(0, 0)
			}
			break
		}
	}
}

function isMonListFocus(focusParent: HTMLElement) {
	return elHasClassName(focusParent, 'monListBtnWrap')
}

function isBattleUIFocus(focusParent: HTMLElement) {
	return focusParent.id === 'attacksMenu' ||
		focusParent.id === 'battleMenu'
}

function focusNextSibling(activeEl: HTMLElement, focusParent: HTMLElement) {
	const nextFocus = activeEl.nextElementSibling as HTMLElement | null
	if (!nextFocus) {
		// focus the first element in list if there is no next sibling element
		const firstEl = focusParent.firstElementChild as HTMLElement | null
		firstEl?.focus()
		events.lastFocusEl = firstEl
		return
	}
	nextFocus.focus()
	events.lastFocusEl = nextFocus
}

function focusPreviousSibling(activeEl: HTMLElement, focusParent: HTMLElement) {
	const previousFocus = activeEl.previousElementSibling as HTMLElement | null
	if (!previousFocus) {
		const lastEl = focusParent.lastElementChild as HTMLElement | null
		// focus the last element in list if there is no previous sibling element
		lastEl?.focus()
		events.lastFocusEl = lastEl
		return
	}
	previousFocus.focus()
	events.lastFocusEl = previousFocus
}

function handleMonListFocus(focusEl: HTMLElement, focusParent: HTMLElement, direction: string) {
	const monList = getElByQuery('#monsterList')
	const firstMonWrap = monList.firstElementChild as HTMLElement | null
	const lastMonWrap = monList.lastElementChild as HTMLElement | null
	const firstBtnHasFocus = focusEl.getAttribute('name')?.includes('1')
	const cancelBtn = document.getElementById('cancelMonSwitch')
	// focus up
	if (direction === 'w') {
		// if cancel monster switch btn has focus
		if (focusEl === cancelBtn) {
			// focus last monster
			const nextFocus = lastMonWrap?.lastElementChild?.firstElementChild as HTMLElement | null
			nextFocus?.focus()
			events.lastFocusEl = nextFocus
			return
		}
		// is first monster, focus last monster
		if (!focusParent.parentElement?.previousElementSibling) {
			// if monster list is there to switch monster in fight
			if (dialog.el.innerText.includes('Which MONSTER should fight?')) {
				// focus the cancel btn
				cancelBtn?.focus()
				events.lastFocusEl = cancelBtn
				return
			}
			if (firstBtnHasFocus) {
				const nextFocus = lastMonWrap?.lastElementChild?.firstElementChild as HTMLElement | null
				nextFocus?.focus()
				events.lastFocusEl = nextFocus
				return
			}
			// last btn has focus
			const nextFocus = lastMonWrap?.lastElementChild?.lastElementChild as HTMLElement | null
			nextFocus?.focus()
			events.lastFocusEl = nextFocus
			return
		}
		if (firstBtnHasFocus) {
			const nextFocus = focusParent.parentElement?.previousElementSibling?.lastElementChild?.firstElementChild as HTMLElement | null
			nextFocus?.focus()
			events.lastFocusEl = nextFocus
			return
		}
		const nextFocus = focusParent.parentElement?.previousElementSibling?.lastElementChild?.lastElementChild as HTMLElement | null
		nextFocus?.focus()
		events.lastFocusEl = nextFocus
	}
	// focus down
	if (direction === 's') {
		// if cancel monster switch btn has focus
		if (focusEl === cancelBtn) {
			// focus last monster
			const nextFocus = firstMonWrap?.lastElementChild?.firstElementChild as HTMLElement | null
			nextFocus?.focus()
			events.lastFocusEl = nextFocus
			return
		}
		// is last monster, focus first monster
		if (!focusParent.parentElement?.nextElementSibling) {
			// if monster list is there to switch monster in fight
			if (dialog.el.innerText.includes('Which MONSTER should fight?')) {
				// focus the cancel btn
				cancelBtn?.focus()
				events.lastFocusEl = cancelBtn
				return
			}
			if (firstBtnHasFocus) {
				const nextFocus = firstMonWrap?.lastElementChild?.firstElementChild as HTMLElement | null
				nextFocus?.focus()
				events.lastFocusEl = nextFocus
				return
			}
			// last btn has focus
			const nextFocus = firstMonWrap?.lastElementChild?.lastElementChild as HTMLElement | null
			nextFocus?.focus()
			events.lastFocusEl = nextFocus
			return
		}
		if (firstBtnHasFocus) {
			const nextFocus = focusParent.parentElement?.nextElementSibling?.lastElementChild?.firstElementChild as HTMLElement | null
			console.log('nextFocus: ', nextFocus)
			nextFocus?.focus()
			events.lastFocusEl = nextFocus
			return
		}
		const nextFocus = focusParent.parentElement?.nextElementSibling?.lastElementChild?.lastElementChild as HTMLElement | null
		nextFocus?.focus()
		events.lastFocusEl = nextFocus
	}
}

function handleBattleUIFocus(focusEl: HTMLElement, focusParent: HTMLElement, direction: string) {
	const attackBtnWrap = getElByQuery('#attacksMenu')
	const menuBtnWrap = getElByQuery('#battleMenu')
	if (focusParent.id === 'attacksMenu') {
		if (direction === 'a') {
			if (focusEl.id === 'attack1' || focusEl.id === 'attack3') {
				const nextFocus = menuBtnWrap.lastElementChild as HTMLElement | null
				nextFocus?.focus()
				events.lastFocusEl = nextFocus
				return
			}
			const nextFocusName = `attack${(+focusEl.id[focusEl.id.length - 1] - 1)}`
			const nextFocus = document.getElementById(nextFocusName)
			if (!nextFocus) { return }
			nextFocus.focus()
			events.lastFocusEl = nextFocus
		}
		if (direction === 'd') {
			if (focusEl.id === 'attack2' || focusEl === attackBtnWrap.lastElementChild) {
				// switch from attack top-right to battleMenu top-left element
				const nextFocus = menuBtnWrap.firstElementChild as HTMLElement | null
				nextFocus?.focus()
				events.lastFocusEl = nextFocus
				return
			}
			const nextFocusName = `attack${(+focusEl.id[focusEl.id.length - 1] + 1)}`
			const nextFocus = document.getElementById(nextFocusName)
			if (!nextFocus) { return }
			nextFocus.focus()
			events.lastFocusEl = nextFocus
		}
		if (direction === 'w') {
			const nextFocusName = `attack${(+focusEl.id[focusEl.id.length - 1] - 2)}`
			const nextFocus = document.getElementById(nextFocusName)
			if (!nextFocus) { return }
			nextFocus.focus()
			events.lastFocusEl = nextFocus
		}
		if (direction === 's') {
			const nextFocusName = `attack${(+focusEl.id[focusEl.id.length - 1] + 2)}`
			const nextFocus = document.getElementById(nextFocusName)
			if (!nextFocus) { return }
			nextFocus.focus()
			events.lastFocusEl = nextFocus
		}
		return
	}
	if (focusParent.id === 'battleMenu') {
		if (direction === 'a') {
			// if first element has focus
			if (focusEl === menuBtnWrap.firstElementChild) {
				if (attackBtnWrap.children.length === 1) {
					const nextFocus = document.getElementById('attack1')
					nextFocus?.focus()
					events.lastFocusEl = nextFocus
					return
				}
				const nextFocus = document.getElementById('attack2')
				nextFocus?.focus()
				events.lastFocusEl = nextFocus
				return
			}
			// if third element has focus
			if (focusEl === menuBtnWrap.firstElementChild?.nextElementSibling?.nextElementSibling) {
				const nextFocus = document.getElementById(`attack${attackBtnWrap.children.length}`)
				nextFocus?.focus()
				events.lastFocusEl = nextFocus
				return
			}
			const nextFocusName = `menu${(+focusEl.id[focusEl.id.length - 1] - 1)}`
			const nextFocus = document.getElementById(nextFocusName)
			if (!nextFocus) { return }
			nextFocus.focus()
			events.lastFocusEl = nextFocus
		}
		if (direction === 'd') {
			// if second or last element has focus
			if (focusEl === menuBtnWrap.firstElementChild?.nextElementSibling || focusEl === menuBtnWrap.lastElementChild) {
				const nextFocus = attackBtnWrap.firstElementChild as HTMLElement | null
				nextFocus?.focus()
				events.lastFocusEl = nextFocus
				return
			}
			const nextFocusName = `menu${(+focusEl.id[focusEl.id.length - 1] + 1)}`
			const nextFocus = document.getElementById(nextFocusName)
			if (!nextFocus) { return }
			nextFocus.focus()
			events.lastFocusEl = nextFocus
		}
		if (direction === 'w') {
			if (!focusEl.id) { return }
			const nextFocusName = `menu${(+focusEl.id[focusEl.id.length - 1] - 2)}`
			const nextFocus = document.getElementById(nextFocusName)
			if (!nextFocus) { return }
			nextFocus.focus()
			events.lastFocusEl = nextFocus
		}
		if (direction === 's') {
			if (!focusEl.id) { return }
			const nextFocusName = `menu${(+focusEl.id[focusEl.id.length - 1] + 2)}`
			const nextFocus = document.getElementById(nextFocusName)
			if (!nextFocus) { return }
			nextFocus.focus()
			events.lastFocusEl = nextFocus
		}
	}
}

function countBtnHasFocus() {
	return document.activeElement?.id === 'dropX'
}

function updateInput({ add }: { add: boolean }) {
	const input = getElByQuery('#itemCountToDrop') as HTMLInputElement
	if (input.value === (add ? input.max : input.min)) { return }
	// update input
	input.value = (add ? +input.value + 1 : +input.value - 1).toString()
	// update btn txt
	updateBtnCount(add)
}

function updateBtnCount(add = true) {
	const btn = getElByQuery('#dropX')
	const btnCount = +btn.innerText.substring(btn.innerText.lastIndexOf('x') + 1, btn.innerText.length)
	const beginTxt = btn.innerText.substring(0, btn.innerText.lastIndexOf('x') + 1)
	btn.innerText = `${beginTxt}${add ? btnCount + 1 : btnCount - 1}`
}