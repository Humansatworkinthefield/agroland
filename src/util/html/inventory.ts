import { player } from '../..'
import { dialog } from '../../classes/Dialog'
import { events } from '../../classes/Events'
import { flags } from '../../classes/Flags'
import { Item } from '../../classes/Items/Item'
import { playAudio } from '../../data/audio'
import { elHasClassName, focusFirstChildAfterTimeout } from '../helper'
import { closeInventoryBtn, dropAllBtn, dropBackBtn, dropInventoryItemBtn, dropXBtn, inputCount, inputItemToDrop, inventoryItemBtn, inventoryItemList, inventoryPlayerBag, inventorySection, playerItemBagBtn } from './elements'

export function openInventoryMenu() {
	flags.setIsInteracting(true)
	player.sprite.frames.val = 0
	player.sprite.animate = false
	playAudio('pcOn')
	dialog.show(`${player.name} turns on the PC...`)
	const t = setTimeout(() => {
		dialog.updateInnerHTML('Choose a MENU')
		inventorySection.style.display = 'grid'
		focusFirstChildAfterTimeout(inventorySection)
		clearTimeout(t)
	}, 1250)
}

export function cancelInventoryModal(playerBag: boolean, forDrop = false) {
	dialog.updateInnerHTML(forDrop ? 'Which inventory ITEM do you want to DROP?' : 'Which inventory ITEM do you want to TAKE?')
	inputCount.setAttribute('value', '1')
	inputItemToDrop.removeAttribute('style')
	if (!playerBag) {
		focusFirstChildAfterTimeout(inventoryItemList)
		return
	}
	focusFirstChildAfterTimeout(inventoryPlayerBag)
}

function openInventoryItemList({ forDrop }: { forDrop: boolean }) {
	inventoryItemList.replaceChildren()
	hideInventory()
	dialog.updateInnerHTML(forDrop ? 'Which inventory ITEM do you want to DROP?' : 'Which inventory ITEM do you want to TAKE?')
	renderInventoryItems(forDrop)
}

function openPlayerBag() {
	inventoryPlayerBag.replaceChildren()
	hideInventory()
	dialog.updateInnerHTML('Which ITEM do you want to STORE?')
	renderBagItems()
}

function renderInventoryItems(forDrop = false) {
	inventoryItemList.replaceChildren()
	for (let i = 0; i < player.inventory.items.length; i++) {
		const item = player.inventory.items[i]
		const button = document.createElement('button')
		if (i === 0) { button.id = 'firstItemEl' }
		button.classList.add('itemWrap')
		// take inventory item click event
		button.onclick = () => {
			if (!forDrop) {
				handleTakeItem(item)
				return
			}
			// else: handle drop item
			handleDropItem(item, forDrop)
		}
		const iName = document.createElement('p')
		iName.innerHTML = item.name
		button.appendChild(iName)
		const iCount = document.createElement('p')
		iCount.innerHTML = `x ${item.count}`
		button.appendChild(iCount)
		inventoryItemList.appendChild(button)
	}
	const backBtn = document.createElement('button')
	backBtn.classList.add('itemWrap')
	backBtn.innerHTML = '<p>BACK</p>'
	backBtn.onclick = () => events.handleBackBtn()
	inventoryItemList.appendChild(backBtn)
	if (!elHasClassName(inventoryItemList, 'display')) {
		inventoryItemList.classList.add('display')
	}
	focusFirstChildAfterTimeout(inventoryItemList)
}

function renderBagItems() {
	inventoryPlayerBag.replaceChildren()
	for (let i = 0; i < player.items.length; i++) {
		const item = player.items[i]
		const button = document.createElement('button')
		if (i === 0) { button.id = 'firstItemEl' }
		button.classList.add('itemWrap')
		// take inventory item click event
		button.onclick = () => handleStoreItem(item)
		const iName = document.createElement('p')
		iName.innerHTML = item.name
		button.appendChild(iName)
		const iCount = document.createElement('p')
		iCount.innerHTML = `x ${item.count}`
		button.appendChild(iCount)
		inventoryPlayerBag.appendChild(button)
	}
	const backBtn = document.createElement('button')
	backBtn.classList.add('itemWrap')
	backBtn.innerHTML = '<p>BACK</p>'
	backBtn.onclick = () => events.handleBackBtn()
	inventoryPlayerBag.appendChild(backBtn)
	if (!elHasClassName(inventoryPlayerBag, 'display')) {
		inventoryPlayerBag.classList.add('display')
	}
	focusFirstChildAfterTimeout(inventoryPlayerBag)
}

// take from inventory into player bag
function handleTakeItem(item: Item) {
	// player bag is full
	if (player.hasMaxItems()) {
		dialog.updateInnerHTML(`${player.name}s BAG is full!`)
		return
	}
	// take single item entirely without opening count modal
	if (item.count === 1) {
		player.addItem(item, 1)
		player.inventory.dropItem(item, 1)
		// rerender
		renderInventoryItems()
		focusFirstChildAfterTimeout(inventoryItemList)
		dialog.updateInnerHTML(`x1 ${item.name} taken!`)
		const t = setTimeout(() => {
			if (!elHasClassName(inventoryItemList, 'display')) { return }
			dialog.updateInnerHTML('Which inventory ITEM do you want to TAKE?')
			clearTimeout(t)
		}, 1000)
		return
	}
	// choose item count to take
	dialog.updateInnerHTML(`How many ${item.name}s do you want to TAKE?`)
	dropAllBtn.innerText = 'TAKE ALL'
	inputItemToDrop.style.display = 'flex'
	inputCount.max = `${item.count}`
	inputCount.value = '1'
	dropXBtn.innerText = `TAKE x${inputCount.value}`
	dropXBtn.focus()
	// inputCount.focus()
	events.lastFocusEl = dropXBtn
	// input change
	inputCount.onchange = () => {
		dropXBtn.innerText = `TAKE x${inputCount.value}`
	}
	inputCount.oninput = () => {
		dropXBtn.innerText = `TAKE x${inputCount.value}`
	}
	// go back
	dropBackBtn.onclick = () => cancelInventoryModal(false)
	// take all
	dropAllBtn.onclick = () => {
		dialog.updateInnerHTML(`x${item.count} ${item.name} taken!`)
		const t = setTimeout(() => {
			if (!elHasClassName(inventoryItemList, 'display')) { return }
			dialog.updateInnerHTML('Which inventory ITEM do you want to TAKE?')
			clearTimeout(t)
		}, 1000)
		player.inventory.takeItem(item, item.count)
		// close modal
		inputItemToDrop.removeAttribute('style')
		inputCount.setAttribute('value', '1')
		// rerender item list
		renderInventoryItems()
		focusFirstChildAfterTimeout(inventoryItemList)
	}
	// take amount x
	dropXBtn.onclick = () => {
		const inputVal = inputCount.value
		if (!inputVal) { return }
		player.addItem(item, +inputVal)
		player.inventory.dropItem(item, +inputVal)
		dialog.updateInnerHTML(`x${inputVal} ${item.name} taken!`)
		const t = setTimeout(() => {
			if (!elHasClassName(inventoryItemList, 'display')) { return }
			dialog.updateInnerHTML('Which inventory ITEM do you want to TAKE?')
			clearTimeout(t)
		}, 1000)
		// close modal
		inputItemToDrop.removeAttribute('style')
		inputCount.setAttribute('value', '1')
		// rerender item list
		renderInventoryItems()
		focusFirstChildAfterTimeout(inventoryItemList)
	}
}

// drop from inventory
function handleDropItem(item: Item, forDrop = false) {
	// drop single item entirely without opening count modal
	if (item.count === 1) {
		player.inventory.dropItem(item, 1)
		// rerender
		renderInventoryItems(forDrop)
		// inventoryItemList.querySelector('button')?.focus()
		focusFirstChildAfterTimeout(inventoryItemList)
		dialog.updateInnerHTML(`x1 ${item.name} dropped!`)
		const t = setTimeout(() => {
			if (!elHasClassName(inventoryItemList, 'display')) { return }
			dialog.updateInnerHTML('Which inventory ITEM do you want to DROP?')
			clearTimeout(t)
		}, 1000)
		return
	}
	// choose item count to drop
	dialog.updateInnerHTML(`How many ${item.name}s do you want to DROP?`)
	dropAllBtn.innerText = 'DROP ALL'
	inputItemToDrop.style.display = 'flex'
	inputCount.max = `${item.count}`
	inputCount.value = '1'
	dropXBtn.innerText = `DROP x${inputCount.value}`
	dropXBtn.focus()
	events.lastFocusEl = dropXBtn
	// input change
	inputCount.onchange = () => {
		dropXBtn.innerText = `DROP x${inputCount.value}`
	}
	inputCount.oninput = () => {
		dropXBtn.innerText = `DROP x${inputCount.value}`
	}
	// go back
	dropBackBtn.onclick = () => cancelInventoryModal(false)
	// drop all
	dropAllBtn.onclick = () => {
		dialog.updateInnerHTML(`x${item.count} ${item.name} dropped!`)
		const t = setTimeout(() => {
			if (!elHasClassName(inventoryItemList, 'display')) { return }
			dialog.updateInnerHTML('Which inventory ITEM do you want to DROP?')
			clearTimeout(t)
		}, 1000)
		player.inventory.dropItem(item, item.count)
		// close modal
		inputItemToDrop.removeAttribute('style')
		inputCount.setAttribute('value', '1')
		// rerender item list
		renderInventoryItems(forDrop)
		focusFirstChildAfterTimeout(inventoryItemList)
	}
	// drop amount x
	dropXBtn.onclick = () => {
		const inputVal = inputCount.value
		if (!inputVal) { return }
		player.inventory.dropItem(item, +inputVal)
		dialog.updateInnerHTML(`x${inputVal} ${item.name} dropped!`)
		const t = setTimeout(() => {
			if (!elHasClassName(inventoryItemList, 'display')) { return }
			dialog.updateInnerHTML('Which inventory ITEM do you want to DROP?')
			clearTimeout(t)
		}, 1000)
		// close modal
		inputItemToDrop.removeAttribute('style')
		inputCount.setAttribute('value', '1')
		// rerender item list
		renderInventoryItems(forDrop)
		focusFirstChildAfterTimeout(inventoryItemList)
	}
}

// put from player bag into inventory
function handleStoreItem(item: Item) {
	// store single item entirely
	if (item.count === 1) {
		player.inventory.addItem(item, 1)
		// rerender
		renderBagItems()
		focusFirstChildAfterTimeout(inventoryPlayerBag)
		// inventoryPlayerBag.querySelector('button')?.focus()
		dialog.updateInnerHTML(`x1 ${item.name} stored!`)
		const t = setTimeout(() => {
			dialog.updateInnerHTML('Which ITEM do you want to STORE?')
			clearTimeout(t)
		}, 1000)
		return
	}
	// choose item count to store
	dialog.updateInnerHTML(`How many ${item.name}s do you want to STORE?`)
	dropAllBtn.innerText = 'STORE ALL'
	inputItemToDrop.style.display = 'flex'
	inputCount.max = `${item.count}`
	inputCount.value = '1'
	dropXBtn.innerText = `STORE x${inputCount.value}`
	dropXBtn.focus()
	events.lastFocusEl = dropXBtn
	// input change
	inputCount.onchange = () => {
		dropXBtn.innerText = `STORE x${inputCount.value}`
	}
	inputCount.oninput = () => {
		dropXBtn.innerText = `STORE x${inputCount.value}`
	}
	// go back
	dropBackBtn.onclick = () => cancelInventoryModal(true)
	// store all
	dropAllBtn.onclick = () => {
		dialog.updateInnerHTML(`x${item.count} ${item.name} stored!`)
		const t = setTimeout(() => {
			dialog.updateInnerHTML('Which ITEM do you want to STORE?')
			clearTimeout(t)
		}, 1000)
		player.inventory.addItem(item, item.count)
		// close modal
		inputItemToDrop.removeAttribute('style')
		inputCount.setAttribute('value', '1')
		// rerender item list
		renderBagItems()
		focusFirstChildAfterTimeout(inventoryPlayerBag)
	}
	// take amount x
	dropXBtn.onclick = () => {
		const inputVal = inputCount.value
		if (!inputVal) { return }
		player.inventory.addItem(item, +inputVal)
		dialog.updateInnerHTML(`x${inputVal} ${item.name} stored!`)
		const t = setTimeout(() => {
			if (!elHasClassName(inventoryItemList, 'display')) { return }
			dialog.updateInnerHTML('Which inventory ITEM do you want to STORE?')
			clearTimeout(t)
		}, 1000)
		// close modal
		inputItemToDrop.removeAttribute('style')
		inputCount.setAttribute('value', '1')
		// rerender item list
		renderBagItems()
		focusFirstChildAfterTimeout(inventoryPlayerBag)
	}
}

function hideInventory() {
	inventorySection.removeAttribute('style')
	dialog.hide()
}

// show inventory items
inventoryItemBtn.onclick = () => openInventoryItemList({ forDrop: false })

// show player bag items to store
playerItemBagBtn.onclick = () => openPlayerBag()

// show player bag items to drop
dropInventoryItemBtn.onclick = () => openInventoryItemList({ forDrop: true })

// close inventory PC
closeInventoryBtn.onclick = () => {
	events.handleBackBtn()
}