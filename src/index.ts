import { Player } from './classes/Player'
import { events } from './classes/Events'
import { Battle } from './classes/Battle'
// import { Howl } from 'howler'
// import { mapAudio, monsterAudio } from './data/audio'

export const player = Player.newPlayer()

export const battle = Battle.newBattle({ mapID: player.currentMapId })

events.attachStartIntroEvent()

// Activate the following loops if you want to hear all audio files and volume
// it will append sound and volume buttons into the document

// test main sounds
/* for (let i = 0; i < Object.entries(mapAudio).length; i++) {
	const entry = Object.entries(mapAudio)[i]
	const mainWrap = document.createElement('div')
	mainWrap.style.display = 'flex'
	const btn = document.createElement('button')
	btn.innerText = `${entry[0]} - vol: ${entry[1].volume().toFixed(2)}`
	btn.style.margin = '10px 0 10px 10px'
	btn.style.padding = '10px'
	let isPlaying = false
	const volWrap = initVolBtns(entry, isPlaying, btn)
	btn.onclick = () => {
		if (!isPlaying) {
			isPlaying = true
			entry[1].play()
			btn.innerText = `PLAYING - ${entry[0]} - vol: ${entry[1].volume().toFixed(2)}`
			return
		}
		entry[1].stop()
		isPlaying = false
		btn.innerText = `${entry[0]} - vol: ${entry[1].volume().toFixed(2)}`
	}
	mainWrap.appendChild(btn)
	mainWrap.appendChild(volWrap)
	document.body.appendChild(mainWrap)
} */

// test monster sounds
/* for (let i = 0; i < Object.entries(monsterAudio).length; i++) {
	const entry = Object.entries(monsterAudio)[i]
	const mainWrap = document.createElement('div')
	mainWrap.style.display = 'flex'
	let isPlaying = false
	for (let i = 0; i < Object.entries(entry[1]).length; i++) {
		const mainWrap = document.createElement('div')
		mainWrap.style.display = 'flex'
		const el = Object.entries(entry[1])[i]
		const btn = document.createElement('button')
		btn.innerText = `${el[0]} ${entry[0]} - vol: ${el[1].volume().toFixed(2)}`
		btn.style.margin = '10px 0 10px 10px'
		btn.style.padding = '10px'
		const volWrap = initVolBtns(el, isPlaying, btn, entry)
		btn.onclick = () => {
			if (!isPlaying) {
				isPlaying = true
				el[1].play()
				btn.innerText = `PLAYING - ${el[0]} ${entry[0]} - vol: ${el[1].volume().toFixed(2)}`
				return
			}
			el[1].stop()
			btn.innerText = `${el[0]} ${entry[0]} - vol: ${el[1].volume().toFixed(2)}`
			isPlaying = false
		}
		mainWrap.appendChild(btn)
		mainWrap.appendChild(volWrap)
		document.body.appendChild(mainWrap)
	}
} */

// volume helper
/* function initVolBtns(
	entry: [string, Howl],
	isPlaying: boolean,
	btn: HTMLButtonElement,
	monEntry?: [string, {
		init: Howl
		faint: Howl
	}]
) {
	const volWrap = document.createElement('div')
	volWrap.style.display = 'grid'
	volWrap.style.gridTemplateColumns = 'repeat(1, 1fr)'
	volWrap.style.width = 'fit-content'
	volWrap.style.margin = '10px 10px 10px 0'
	volWrap.style.borderLeft = '2px solid #000'
	const volUpBtn = document.createElement('button')
	volUpBtn.innerText = 'up'
	volUpBtn.onclick = () => {
		entry[1].volume(entry[1].volume() + .1)
		btn.innerText = `${isPlaying ? 'PLAYING -' : ''} ${entry[0]} ${monEntry ? monEntry[0] : ''} - vol: ${entry[1].volume().toFixed(2)}`
	}
	const volDownBtn = document.createElement('button')
	volDownBtn.innerText = 'down'
	volDownBtn.onclick = () => {
		entry[1].volume(entry[1].volume() - .1)
		btn.innerText = `${isPlaying ? 'PLAYING -' : ''} ${entry[0]} ${monEntry ? monEntry[0] : ''} - vol: ${entry[1].volume().toFixed(2)}`
	}
	volWrap.appendChild(volUpBtn)
	volWrap.appendChild(volDownBtn)
	return volWrap
} */
