import type { ISpriteArgs } from '../models/classes'
import { getRandomInt } from '../util/helper'
import { canvas } from './Canvas'
import { Img } from './Img'

export class Sprite {
	position // save (done)
	image
	imageFaceUp
	#width = 0
	#height = 0
	frames
	sprites
	animate
	rotation
	opacity
	isEnemy
	brightness
	isBattleBg
	scaleFactor

	/** @returns the enemy sprite width */
	get enemyWidth() { return this.#width }
	/** @returns the enemy sprite height */
	get enemyHeight() { return this.#height }
	/** @returns the player monster sprite height */
	get playerMonWidth() { return this.image.width * this.scaleFactor }
	/** @returns the player monster sprite height */
	get playerMonHeight() { return this.image.height * this.scaleFactor }

	/** This class contains the main methods for the sprite object */
	public constructor({
		position,
		image,
		imageFaceUp,
		frames = { max: 1, hold: 10 },
		sprites,
		animate = false,
		rotation = 0,
		isEnemy,
		scaleFactor,
		isBattleBg
	}: ISpriteArgs) {
		this.scaleFactor = scaleFactor || 1
		this.image = new Img(image?.src)
		this.image.onload = () => {
			const imgWidth = this.image.width
			const imgHeight = this.image.height
			this.#width = (imgWidth / this.frames.max) * this.scaleFactor
			this.#height = imgHeight * this.scaleFactor
		}
		this.imageFaceUp = new Img(imageFaceUp?.src)
		this.imageFaceUp.onload = () => {
			this.#width = this.imageFaceUp.width * this.scaleFactor
			this.#height = this.imageFaceUp.height * this.scaleFactor
		}
		this.position = position
		this.frames = { ...frames, val: 0, elapsed: 0 }
		this.sprites = {
			up: new Img(sprites?.up.src),
			right: new Img(sprites?.right.src),
			down: new Img(sprites?.down.src),
			left: new Img(sprites?.left.src)
		}
		this.animate = animate
		this.rotation = rotation
		this.opacity = 1
		this.isEnemy = isEnemy
		this.brightness = 100
		this.isBattleBg = isBattleBg
	}

	/**
	 * This method uses canvas context to draw the sprites based on the constructor parameters
	 */
	public draw() {
		if (!canvas.ctx || !this.#height) { return }
		canvas.ctx.save()
		// 
		canvas.ctx.translate(this.position.x + this.#width / 2, this.position.y + this.#height / 2)
		// for attack-sprites to face the correct direction (accepts range from 0 - 6.2)
		canvas.ctx.rotate(this.rotation)
		//
		canvas.ctx.translate(-this.position.x - this.#width / 2, -this.position.y - this.#height / 2)
		// set opacity in canvas context
		canvas.ctx.globalAlpha = this.opacity
		// choose right image depending on enemy or not
		const spriteImg = this.isEnemy ? this.imageFaceUp : this.image
		if (this.brightness !== 100) {
			canvas.ctx.filter = `brightness(${this.brightness}%)`
		}
		// https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/drawImage
		canvas.ctx.drawImage(
			spriteImg,
			this.isBattleBg ? 0 : this.frames.val * (this.#width / this.scaleFactor),
			0,
			spriteImg.width / this.frames.max,
			spriteImg.height,
			this.position.x,
			this.position.y,
			this.isBattleBg ? canvas.element.width : (spriteImg.width * this.scaleFactor) / this.frames.max,
			this.isBattleBg ? canvas.element.height : spriteImg.height * this.scaleFactor
		)
		// associated to ctx.save() and ctx.globalAlpha
		canvas.ctx.restore()
		if (!this.animate) { return }
		// set frames elapsed for sprites cropped in 4 pieces, to create move animation 
		if (this.frames.max > 1) {
			this.frames.elapsed++
		}
		// changes the cropped piece with the next one to create move animation
		if (this.frames.elapsed % this.frames.hold === 0) {
			if (this.frames.val < this.frames.max - 1) {
				// npcs with random direction change
				if (this.image.src.includes('/npcs/') && this.frames.hold == 210) {
					this.frames.val = getRandomInt(0, 3)
					return
				}
				// rest of sprites with incremental direction change
				this.frames.val++
			}
			else { this.frames.val = 0 }
		}
	}

	/**
	 * This method sets the sprite position to a new position
	 * @param newPos - the new position object
	 */
	public setPos(newPos: { x: number, y: number }) {
		this.position = newPos
	}

	/**
	 * This method allows to serialize the Sprite object for saving purposes
	 * @returns a JSON string for saving progress purposes
	 */
	public toJSON() {
		return { position: this.position }
	}

}
