import { battle } from '..'
import { elHasStyleVal, focusFirstChildAfterTimeout, getElByQuery } from '../util/helper'
import { attacksSection } from '../util/html/elements'
import { flags } from './Flags'
import { reaction } from './ReactionQueue'

class Dialog {

	static #instance: Dialog
	#el
	#isPrinting = false
	#isPrintCanceled = false

	/** @returns the dialog html element */
	get el() { return this.#el }

	public static newDialog() {
		if (!this.#instance) { return new Dialog() }
		return this.#instance
	}

	/** This class contains the main methods for the dialog window */
	private constructor() {
		this.#el = getElByQuery('#dialogWindow')
		// add listener
		this.#el.addEventListener('click', this.handleEvent)
	}

	/**
	 * This method displays the dialog window
	 * @param txt - A string that should be displayed in the dialog window
	 */
	public show(txt: string) {
		this.#isPrintCanceled = false
		// clear previous dialog
		this.#clearTxt()
		// show new dialog
		this.#el.style.display = 'block'
		// print letter by letter
		this.#printLByL(txt, 20)
		// hide battle UI to prevent its events from firing
		if (battle.isInitiated) {
			battle.hideUI()
		}
	}

	/**
	 * This method hides the dialog window
	 */
	public hide() {
		this.#el.removeAttribute('style')
	}

	/**
	 * This method shows the hidden dialog with the previous message if one was applied 
	 */
	public display() {
		this.el.style.display = 'block'
	}

	/**
	 * This method changes the content of the dialog without printing letter by letter
	 * @param innerHTML - A string that should be displayed in the dialog window
	 */
	public updateInnerHTML(innerHTML: string) {
		this.#isPrintCanceled = true
		this.#el.style.display = 'block'
		this.#el.innerHTML = innerHTML
	}

	/**
	 * This method handles the dialog window click-event
	 */
	public handleEvent = () => {
		// cancel letter by letter printing and show whole dialog txt
		if (this.#isPrinting) {
			this.#isPrintCanceled = true
			return
		}
		// avoid queue execution if animation is running or if the dialog has a specific textContent
		if (flags.getIsAnimating() || this.cantSkip()) { return }
		// execute queue
		if (reaction.queue.length > 0) {
			reaction.execute()
			return
		}
		if (battle.isInitiated) {
			battle.showUI()
			// set focus on first attack
			focusFirstChildAfterTimeout(attacksSection)
		}
		// hide dialog
		this.hide()
	}

	/**
	 * @returns a boolean that indicates if the dialog can be skipped depending on the current dialog text content 
	 */
	public cantSkip() {
		const txtArr = [
			'Choose your FIRST MONSTER!',
			'Are you sure that you want',
			'Are you sure that you want',
			'What will ',
			'Choose a MENU',
			'Which MONSTER ',
			'switch the MONSTER?',
			'Do you want to SAVE your progress?',
			'forget an attack and learn',
			'We offer the best possible HEALING methods!'
		]
		for (let i = 0; i < txtArr.length; i++) {
			if (this.showsStr(txtArr[i])) {
				return true
			}
		}
		return false
	}

	/**
	 * This method changes the dialog background color
	 * @param color - A new color HEX string
	 */
	public changeBgColor(color: string) {
		this.#el.style.backgroundColor = color
	}

	/**
	 * This method checks if the dialog window is displayed
	 */
	public isShowing() {
		return elHasStyleVal(this.#el)
	}

	/**
	 * @return a boolean which indicates if the dialog includes a specific string
	 */
	public showsStr(str: string) {
		return this.#el.innerText.includes(str)
	}

	/**
	 * This method prints the dialog txt letter by letter
	 * @param txt - The string that needs to be printed
	 * @param ms - A number representing the interval between each print in milliseconds
	 */
	#printLByL(txt: string, ms: number) {
		this.#isPrinting = true
		let i = 0
		const interval = setInterval(() => {
			if (this.#isPrintCanceled) {
				this.#el.textContent = txt
				i = 0
				this.#isPrinting = false
				this.#isPrintCanceled = false
				clearInterval(interval)
				return
			}
			this.#el.textContent += txt.charAt(i)
			i++
			if (i > txt.length) {
				clearInterval(interval)
				i = 0
				this.#isPrinting = false
			}
		}, ms)
	}

	/**
	 * This method clears the textContent of the dialog element with an empty string
	 */
	#clearTxt() {
		this.#el.textContent = ''
	}

}

export const dialog = Dialog.newDialog()