import { monsterAudio } from '../data/audio'
import { getAllMonsters } from '../data/monster/allMonsters'
import type { IMonsterDexArgs } from '../models/classes'
import { focusFirstChildAfterTimeout, getElByQuery, getGrowthStr, getTypesStr } from '../util/helper'
import { formatMonIdx, formatName } from '../util/fmt'
import { pauseMenuEl, dexListEl, dexEntryEl } from '../util/html/elements'
import { Img } from './Img'
import { Monster } from './Monster'

export class MonsterDex {

	static #instance: MonsterDex
	#available
	#isBroken
	#entries

	/*** Getter ***/
	/** @returns a boolean which indicates if the monster-dex is available or not */
	get available() { return this.#available }
	/** @returns a boolean which indicates if the monster-dex is broken or not */
	get isBroken() { return this.#isBroken }
	/** @returns an array of monster-dex entries */
	get entries() { return this.#entries }

	public static newMonsterDex({ available, isBroken, entries }: IMonsterDexArgs) {
		if (!this.#instance) {
			return new MonsterDex({
				available,
				isBroken,
				entries
			})
		}
		return this.#instance
	}

	/** This class contains the main methods for the monster-dex-object */
	private constructor({
		available,
		isBroken,
		entries,

	}: IMonsterDexArgs) {
		this.#available = available
		this.#isBroken = isBroken
		this.#entries = entries
	}

	/** This method sets the monster-dex-object property "available" to true */
	public unlockMonsterDex() {
		this.#available = true
	}

	/** This method sets the monster-dex-object property "isBroken" to false */
	public repairMonsterDex() {
		this.#isBroken = false
	}

	public getEntriesCount() {
		return this.#entries.length
	}

	/**
	 * This method returns the number of monster catched
	 * @returns a number of monster catched
	 */
	public getCatchedCount() {
		return this.#entries.filter(x => x.catched).length
	}

	/**
	 * This method adds the given monster to the monster-dex after checking if it is already in the monster-dex
	 * @param entry - A monster object to add as a new monster-dex-entry
	 */
	public addNewEntry(entry?: Monster) {
		if (!entry) {
			console.warn('no entry available for monster-dex')
			return
		}
		const name = formatName(entry.name)
		if (!this.#isInList(entry.idx)) {
			this.#entries.push({
				idx: entry.idx,
				name,
				catched: false
			})
		}
	}

	/**
	 * This method updates the monster-dex-entry property "catched" to true if it is not already catched
	 * @param idx - A number that indicates the index of the monster
	 * @returns 
	 */
	public setIsCatched(idx: number) {
		const monster = this.#getEntryByMonIdx(idx)
		if (!monster || monster.catched) { return }
		monster.catched = true
	}

	/***** Methods to handle HTML Elements *****/

	/** This method handles the HTML of the monster-dex to render the list */
	public open() {
		const allMons = getAllMonsters()
		// hide pause menu
		pauseMenuEl.removeAttribute('style')
		dexListEl.replaceChildren()
		// overview
		const overview = document.createElement('div')
		overview.classList.add('listOverview')
		const seenP = document.createElement('p')
		seenP.innerHTML = `Seen: ${this.isBroken ? 'ERROR-934' : this.#entries.length}`
		const own = document.createElement('p')
		own.innerHTML = `Owner: ${this.isBroken ? 'ERROR-934' : this.getCatchedCount()}`
		overview.appendChild(seenP)
		overview.appendChild(own)
		dexListEl.appendChild(overview)
		const dexListWrap = document.createElement('div')
		dexListWrap.id = 'dexListWrap'
		dexListWrap.classList.add('dexListWrap')
		if (!this.isBroken) {
			// List
			for (let i = 0; i < Object.values(allMons).length; i++) {
				const monster = Object.values(allMons)[i]
				const button = document.createElement('button')
				button.classList.add('dexListEntryBtn')
				button.onclick = () => {
					this.showEntry(new Monster({
						...monster,
						uid: 0,
						level: 1,
						audio: monsterAudio[monster.name]
					}))
				}
				const wrap = document.createElement('div')
				wrap.classList.add('dexListItemWrap')
				const idxPara = document.createElement('p')
				idxPara.style.marginRight = '6px'
				idxPara.innerHTML = formatMonIdx(monster.idx, false)
				const catched = new Img('./imgs/ball/ball.png')
				catched.style.marginRight = '6px'
				const name = document.createElement('p')
				name.innerHTML = '---------'
				if (this.#isInList(monster.idx)) {
					name.innerHTML = ` - ${monster.name.toUpperCase()}`
				}
				wrap.appendChild(idxPara)
				if (this.#getEntryByMonIdx(monster.idx)?.catched) {
					wrap.appendChild(catched)
				}
				wrap.appendChild(name)
				button.appendChild(wrap)
				dexListWrap.appendChild(button)
				// dexListEl.appendChild(button)
			}
		}
		dexListEl.appendChild(dexListWrap)
		dexListEl.style.display = 'grid'
		focusFirstChildAfterTimeout(dexListEl)
	}

	/** This method handles the HTML of the monster-dex and hides the list */
	public closeList() {
		// hide dex list
		dexListEl.removeAttribute('style')
		// show pause menu
		pauseMenuEl.style.display = 'grid'
		focusFirstChildAfterTimeout(pauseMenuEl)
	}

	/** 
	 * This method handles the HTML of the monster-dex and renders an entry from list
	 * @param name - The name of the monster that should be displayed as monster-dex-entry
	 */
	public showEntry(monster: Monster) {
		// play monster audio only if it is catched
		if (this.#isInList(monster.idx)) { monster.playAudio() }
		// Hide dex list
		const list = getElByQuery('#dexList')
		list.removeAttribute('style')
		const entryWrapEl = getElByQuery('#monsterDexEntry')
		const infoBtn = getElByQuery('#DexInfoTabBtn')
		const audioBtn = getElByQuery('#DexAudioTabBtn')
		const locationBtn = getElByQuery('#DexLocationTabBtn')
		infoBtn.onclick = () => openInfoTab(infoBtn.getAttribute('name'), infoBtn.id)
		audioBtn.onclick = () => {
			if (!this.#isInList(monster.idx)) {
				audioBtn.innerText = '???'
				const t = setTimeout(() => {
					audioBtn.innerText = 'AUDIO'
					clearTimeout(t)
				}, 2000)
				return
			}
			monster.playAudio()
		}
		locationBtn.onclick = () => openInfoTab(locationBtn.getAttribute('name'), locationBtn.id)
		// open first tab
		openInfoTab(infoBtn.getAttribute('name'), infoBtn.id)
		focusFirstChildAfterTimeout(getElByQuery('#dexTabBtnsWrap'))
		// monster idx
		const monIdx = getElByQuery('#dexMonIdx')
		monIdx.innerHTML = formatMonIdx(monster.idx)
		// monster name
		const monName = getElByQuery('#dexMonName')
		monName.innerText = `NAME: ${this.#isInList(monster.idx) ? monster.name : '???'}`
		// monster type
		const monType = getElByQuery('#dexMonType')
		const types = getTypesStr({ monster })
		monType.innerText = `TYPE: ${this.#isInList(monster.idx) ? types : '???'}`
		// monster weight
		const monWeigth = getElByQuery('#dexMonWeight')
		monWeigth.innerText = `WEIGHT: ${this.#isInList(monster.idx) ? monster.weight : '???'} kg.`
		// monster height
		const monHeight = getElByQuery('#dexMonHeight')
		monHeight.innerText = `HEIGHT: ${this.#isInList(monster.idx) ? monster.height : '???'} cm`
		// monster growth type
		const monGrowth = getElByQuery('#dexMonGrowth')
		const growthStr = getGrowthStr(monster.cat)
		monGrowth.innerText = `GROWTH: ${this.#isInList(monster.idx) ? growthStr || 'Not found!' : '???'}`
		// monster description
		const monDes = getElByQuery('#dexMonDescription')
		monDes.innerText = `${this.#isInList(monster.idx) ? monster.description : `There is no information about NUMBER ${formatMonIdx(monster.idx, false)} yet...`}`
		// monster image
		const monImg = getElByQuery('#dexMonImage')
		monImg.replaceChildren()
		if (this.#isInList(monster.idx)) {
			const imgEl = new Img()
			imgEl.classList.add('monDexImage')
			imgEl.src = monster.sprite.imageFaceUp.src
			monImg.appendChild(imgEl)
		} else {
			monImg.innerHTML = '???'
		}
		// center the location content
		const locationTab = getElByQuery('#DexLocationTab')
		locationTab.classList.add('transitoryClass')
		// display dex entry
		entryWrapEl.style.display = 'block'
	}

	/** This method handles the HTML of the monster-dex and hides the entry */
	public closeEntry() {
		// Show list again
		const list = getElByQuery('#dexList')
		list.style.display = 'grid'
		// hide entry
		dexEntryEl.removeAttribute('style')
		focusFirstChildAfterTimeout(list)
	}

	/**
	 * This method checks if the monster-dex-entry is marked as catched or not
	 * @param idx - The index of the monster object
	 * @returns a boolean which indicates if the monster-dex-entry catch property is true or false 
	 */
	public isMonsterCatched(idx: number) {
		return this.#getEntryByMonIdx(idx)?.catched
	}

	/**
	 * This method allows to serialize the Monster-Dex object for saving purposes
	 * @returns a JSON string for saving progress purposes
	 */
	public toJSON() {
		return {
			available: this.#available,
			isBroken: this.#isBroken,
			entries: this.#entries
		}
	}

	/* Private Methods */

	/**
	 * This method checks if the monster is in the monster-dex object as entry
	 * @param idx - The index of the monster object
	 * @returns a boolean which indicates if the monster objet has been added to the monster-dex-list
	 */
	#isInList(idx: number) {
		return this.#entries.findIndex(x => x.idx === idx) >= 0 ? true : false
	}

	/**
	 * This method returns a monster-dex-entry object by a given monster idx
	 * @param idx - The index of the monster object
	 * @returns a monster-dex-entry by the given monster index
	 */
	#getEntryByMonIdx(idx: number) {
		return this.#entries.find(x => x.idx === idx)
	}

}

// status info tabs
function openInfoTab(tabID: string | null, btnID: string | null) {
	if (!tabID) { return }
	const els = document.getElementsByClassName('dexTabSelection') as HTMLCollectionOf<HTMLDivElement>
	for (let i = 0; i < els.length; i++) {
		els[i].style.display = 'none'
	}
	getElByQuery(`#${tabID}`).style.display = 'grid'
	const tab = getElByQuery('#monsterDexEntry')
	if (tab) {
		if (btnID === 'DexInfoTabBtn') {
			tab.style.backgroundColor = '#fff1d8'
		}
		if (btnID === 'DexLocationTabBtn') {
			tab.style.backgroundColor = '#d8daff'
		}
	}
	showActiveTabBtn(btnID)
}
function showActiveTabBtn(btnID: string | null) {
	if (!btnID) { return }
	const els = document.getElementsByClassName('dexTabBtn') as HTMLCollectionOf<HTMLButtonElement>
	for (let i = 0; i < els.length; i++) {
		els[i].style.border = 'none'
	}
	getElByQuery(`#${btnID}`).style.border = '2px solid orange'
}