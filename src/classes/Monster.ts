import type { IAttack, IAttackParams } from '../models/functions'
import { Sprite } from './Sprite'
import type { IMonsterArgs, IMonsterStatus } from '../models/classes'
import { xpTable } from '../data/monster/xpTable'
import { initialStatus } from '../data/monster/allMonsters'
import { attackSequences } from './Attack'
import { reaction } from './ReactionQueue'
import { formula } from './Formula'
import { flags } from './Flags'
import { canvas } from './Canvas'
import { dialog } from './Dialog'
import { getRandomInt } from '../util/helper'
import { NPC } from './NPC'
import { statusSequence } from './Status'
import { animateWildEscape, endBattleAnimation } from '../animation/battle'
import { mapAudio, monsterAudio } from '../data/audio'
import { battle } from '..'
import { viewport } from './Viewport'
import { events } from './Events'
import { formatName } from '../util/fmt'

export class Monster {

	#idx
	#uid
	#name
	#audio
	#isEnemy
	#isWild
	#attacks
	#lvlAttacks
	#stats
	#status
	#cat
	#type
	#level
	#maxHP
	#sprite
	#xp
	#effort
	#catchRate
	#description
	#weight
	#height
	#transformLvl
	#transTo
	#canEscape
	#catchLvl
	#catchLocation

	/***** Getter *****/

	/** @returns the index of the monster */
	get idx() { return this.#idx }
	/** @returns the unique id of the monster */
	get uid() { return this.#uid }
	/** @returns the name of the monster */
	get name() { return this.#name }
	/** @returns the audio of the monster */
	get audio() { return this.#audio }
	/** @returns a boolean that indicates if the monster is an enemy or not */
	get isEnemy() { return this.#isEnemy }
	/** @returns a boolean that indicates if the monster is wild or not */
	get isWild() { return this.#isWild }
	/** @returns the maximal HP of the monster */
	get maxHP() { return this.#maxHP }
	/** @returns the attack object array of the monster */
	get attacks() { return this.#attacks }
	/** @returns the attack object array of the monster */
	get lvlAttacks() { return this.#lvlAttacks }
	/** @returns the type array of the monster */
	get type() { return this.#type }
	/** @returns the stats object of the monster */
	get stats() { return this.#stats }
	/** @returns the status object of the monster */
	get status() { return this.#status }
	/** @returns the category of the monster */
	get cat() { return this.#cat }
	/** @returns the level of the monster */
	get level() { return this.#level }
	/** @returns the sprite object of the monster */
	get sprite() { return this.#sprite }
	/** @returns the current XP (number) of the monster */
	get xp() { return this.#xp }
	/** @returns the effort value (number) of the monster */
	get effort() { return this.#effort }
	/** @returns the catch rate of the monster */
	get catchRate() { return this.#catchRate }
	/** @returns the description string of the monster */
	get description() { return this.#description }
	/** @returns the weight in kg (number) of the monster */
	get weight() { return this.#weight }
	/** @returns the height in cm (number) of the monster */
	get height() { return this.#height }
	/** @returns the level needed to reach transformation */
	get transformLvl() { return this.#transformLvl }
	/** @returns the monster name of next transformation */
	get transTo() { return this.#transTo }
	/** @returns a boolean that indicates if the monster can escape from a wild battle */
	get canEscape() { return this.#canEscape }
	get catchLvl() { return this.#catchLvl }
	get catchLocation() { return this.#catchLocation }

	/**
	 * This class contains the main properties and methods of the monster object
	 * - The position property is always {x: 0, y: 0} and will be determined in the
	 * constructor once the image sprite is loaded to be able to get the image sizes  
	 */
	public constructor({
		idx,
		uid,
		name,
		isEnemy,
		isWild,
		position,
		image,
		imageFaceUp,
		frames = { max: 1, hold: 10 },
		sprites,
		animate = false,
		rotation = 0,
		opacity,
		audio,
		attacks,
		lvlAttacks,
		stats,
		savedStats,
		status,
		cat,
		type,
		level,
		effort,
		catchRate,
		description,
		weight,
		height,
		transformLvl,
		transTo,
		xp,
		canEscape,
		catchLvl,
		catchLocation
	}: IMonsterArgs) {
		this.#status = status
		this.#sprite = new Sprite({
			position,
			image,
			imageFaceUp,
			frames,
			sprites,
			animate,
			rotation,
			opacity,
			isEnemy,
			scaleFactor: viewport.getScaleFactor(true)
		})
		if (status.frozen) { this.#sprite.brightness = 250 }
		// determine the monster sprite position once the image is loaded to be able to get the image sizes
		this.#sprite.image.onload = () => {
			this.#sprite.position = this.getMonsterSpawnPos()
		}
		this.#idx = idx
		this.#uid = uid
		this.#name = name.toUpperCase()
		this.#audio = audio
		this.#level = level
		this.#lvlAttacks = lvlAttacks
		this.#attacks = attacks || this.#initAttacks()
		this.#type = type ? type : []
		this.#cat = cat
		this.#isEnemy = isEnemy
		this.#isWild = isWild
		this.#xp = xp || xpTable[cat - 1][level - 1]
		this.#effort = effort
		const calcStats = formula.calcStatsIS(stats, this.#level)
		this.#stats = savedStats || {
			base: stats.base,
			IV: stats.IV,
			EV: stats.EV,
			EVY: stats.EVY,
			IS: calcStats
		}
		this.#maxHP = calcStats.hp
		this.#catchRate = catchRate
		this.#description = description
		this.#weight = weight
		this.#height = height
		this.#transformLvl = transformLvl
		this.#transTo = transTo
		this.#canEscape = canEscape
		this.#catchLvl = catchLvl
		this.#catchLocation = catchLocation
	}

	/**
	 * This method sets the enemy property of the monster to the given boolean value
	 * @param val - A boolean that indicates if the monster is an enemy or not
	 */
	public setIsEnemy(val: boolean) {
		this.#sprite.isEnemy = val
		this.#isEnemy = val
	}

	/**
	 * This method handles the procedure of a monster attack by updating the corresponding monster properties and by starting the attack animation 
	 * @param {attack, recipient} - an object containing the following properties:
	 * - attack: The attack that is used
	 * - recipient: The monster object that gets attacked
	 */
	public attack({ attack, recipient }: IAttackParams) {
		// reset focus
		events.lastFocusEl = null
		// show dialog box
		dialog.show(`${this.#name} used ${attack.name.toUpperCase()}!`)
		// substract from attack execution time
		// TODO add logic for enemy remaining attack executions (random enemy-attack should sort out the 0 remaining attacks)
		if (!this.#isEnemy) { attack.remExec -= 1 }
		// Missed attack chance
		if (!battle.isHit(attack.acc)) {
			reaction.queue.push(
				() => dialog.show(`${this.#name} missed the target!`)
			)
			return
		}
		// flag to avoid dialogBox spam click
		flags.setIsAnimating(true)
		// calculate damage
		const result = formula.calcDamage(this, attack, recipient)
		// substract from health
		recipient.#stats.IS.hp -= result.damage
		// determine enemy or not for healthbar params
		const healthBar = this.#isEnemy ? '#playerHealthBar' : '#enemyHealthBar'
		// attack animation and frontend handling
		attackSequences?.getSequence(attack.name)?.(this, recipient, healthBar, result)
	}

	/**
	 * This method generates a random integer which is used to choose the index of the monster-attack object
	 * @param recipient - The target monster object
	 */
	public randomAttack(recipient: Monster, trainer?: NPC) {
		// check if wild monster escapes wild battle
		if (this.#willEscape()) {
			dialog.show('WHAT?!!')
			// monster escape sequence
			reaction.queue.push(
				() => animateWildEscape(this),
				() => dialog.show(`Wild ${this.name} just escaped!`),
				() => {
					mapAudio.wild_battle.stop()
					endBattleAnimation()
				}
			)
			return
		}
		const attack = this.attacks[Math.floor(Math.random() * this.attacks.length)]
		// trigger opponent attack
		this.attack({ attack, recipient })
		// player monster fainted
		if (recipient.stats.IS.hp <= 0) {
			reaction.queue.push(() => recipient.faint(this, trainer))
		}
		// if opponent status is affected: the queue contains the sequence, else: the queue will be empty  
		reaction.queue.push(
			...statusSequence.generateDamagingStatusQueue(this, recipient, trainer)
		)
		reaction.queue.push(
			...statusSequence.generateDamagingStatusQueue(this, recipient, trainer, true)
		)
	}

	public attackSequence(attack: IAttack, enemy: Monster, attackButton: HTMLButtonElement, trainer?: NPC) {
		// Player Monster only responses if alive
		if (this.stats.IS.hp > 0) {
			reaction.queue.push(() => {
				// trigger the selected attack of player monster
				this.attack({
					attack,
					recipient: enemy
				})
				// update attack remaining executions
				attackButton.innerHTML = `${attack.name.toUpperCase()}<p class="attackExec">${attack.remExec}/${attack.maxExec}</p>`
				// enemy defeated
				if (enemy.stats.IS.hp <= 0) {
					reaction.queue.push(() => enemy.faint(this, trainer))
					return
				}
				// the attack has affected the opponents status
				if (attackSequences.statusChangedFirst) {
					attackSequences.setStatusChangedFirst(false)
					reaction.queue.push(
						...statusSequence.generateDamagingStatusQueue(enemy, this, trainer),
						...statusSequence.generateNonDamagingStatusQueue(enemy)
					)
				}
			})
		}
	}

	/**
	 * This method handles the procedure of a monster fainting by updating the corresponding monster properties and by starting the faint animation 
	 * @param winner - The monster object that has won the battle
	 */
	public faint(winner: Monster, trainer?: NPC) {
		// flag
		flags.setIsAnimating(true)
		// Monster info section
		const infoSection = this.#isEnemy ? '#enemyInfoSection' : '#playerMonsterInfoSection'
		// audio
		this.playAudio()
		// this.playFaintAudio()
		// calculate the amount of XP gained
		const xp = formula.calcScaledXPGain(winner, this)
		// show dialog box
		dialog.show(`${this.#name} fainted...`)
		// animate enemy defeated & add xp to battle participants
		attackSequences?.faint(this, infoSection, xp, trainer)
		// calculate and append EV gain to winner
		formula.calcEVGain(this)
	}

	/**
	 * This method adds the given XP to the monster object or
	 * sets the XP to its maximum if the monster has already reached max-level of 100
	 * @param xp - A number which indicates the amount of XP that a monster will gain
	 */
	public increaseXP(xp: number) {
		if (this.#xp >= xpTable[this.#cat - 1][xpTable[this.#cat - 1].length - 1]) {
			this.#xp = xpTable[this.#cat - 1][xpTable[this.#cat - 1].length - 1]
			return
		}
		this.#xp += xp
	}

	/**
	 * This method adds the given HP value to the monster HP or
	 * sets to its maximum if the addition results in a higher value than the max-HP of that monster
	 * @param hp - A number which indicates the amount of HP to increase
	 */
	public increaseHP(hp: number) {
		this.#stats.IS.hp += hp
		if (this.#stats.IS.hp > this.#maxHP) {
			this.#stats.IS.hp = this.#maxHP
		}
	}

	/**
	 * This method checks if the monster has reached the XP needed to level up
	 * but always returns false if the monster has already reached its max-level of 100
	 * @returns a boolean which indicates if the monster has reached the XP needed to level up
	 */
	public isLevelUp() {
		if (this.#level === 100) { return false }
		return this.#xp >= xpTable[this.#cat - 1][this.#level]
	}

	/**
	 * This method sets the monster status
	 * @param status - The new status object
	 */
	public setStatus(status: IMonsterStatus) {
		this.#status = status
	}

	/**
	 * This method increases the level of a monster by a given value
	 * @param lvlCount - A number which indicates how many levels should be added
	 */
	public increaseLvl(lvlCount: number) {
		this.#level += lvlCount
	}

	/**
	 * This method sets the XP to the minimum of the given level
	 */
	public setInitialXPOfLvl() {
		this.#xp = xpTable[this.#cat - 1][this.#level - 1]
	}

	/**
	 * This method increases the IS-stats of a monster after a level up
	 */
	public increaseStats() {
		const calcStats = formula.calcStatsIS(this.#stats, this.#level)
		const hpDiff = calcStats.hp - this.#maxHP
		this.#maxHP = calcStats.hp
		this.#stats = {
			base: { ...this.#stats.base },
			IV: { ...this.#stats.IV },
			EV: { ...this.#stats.EV },
			EVY: { ...this.#stats.EVY },
			IS: { ...calcStats, hp: this.#stats.IS.hp + hpDiff }
		}
	}

	/**
	 * This method recalculates the monster stats and is used basically after a battle-end to reset
	 * the modified stats that have been changed due to attack-effects
	 */
	public resetStats() {
		const hp = this.#stats.IS.hp
		this.#stats.IS = {
			...formula.calcStatsIS(this.#stats, this.#level),
			hp
		}
	}

	/**
	 * This method sets the HP, the attack-remaining-executionsa of the monster to its maximum.
	 * It also sets the properties of the status object to its initial values
	 */
	public regenerate() {
		this.#stats.IS = {
			...formula.calcStatsIS(this.#stats, this.#level),
			hp: this.#maxHP
		}
		this.#attacks.forEach(attack => attack.remExec = attack.maxExec)
		this.#status = { ...initialStatus, XPPower: this.#status.XPPower, affection: this.#status.affection }
	}

	/**
	 * This method plays the initial audio of the monster
	 */
	public playAudio() { this.#audio?.play() }

	/** 
	 * This method handles the responsive start position of the battle sprites
	 * - The start position is outside the viewport so we can fade the sprites in from the side
	*/
	public getMonsterSpawnPos() {
		return {
			x: this.isEnemy ? canvas.getEnemyPosXBeforeFade() : canvas.getFriendlyPosX(),
			y: this.isEnemy ? canvas.getEnemyPosY(this.#sprite) : canvas.getFriendlyPosY(this.#sprite)
		}
	}

	/**
	 * This method handles the position of the battle sprites after they are faded into the viewport
	 */
	public getMonsterPos() {
		return {
			x: this.isEnemy ? canvas.getEnemyPosXAfterFade(this.#sprite) : canvas.getFriendlyPosX(),
			y: this.isEnemy ? canvas.getEnemyPosY(this.#sprite) : canvas.getFriendlyPosY(this.#sprite)
		}
	}

	/**
	 * This method loads the monster audio
	 */
	public loadAudio() {
		const name = formatName(this.name)
		if (monsterAudio[name].state() === 'unloaded') {
			monsterAudio[name].load()
		}
	}

	/**
	 * This method allows to serialize the Monster object for saving purposes
	 * @returns a JSON string for saving progress purposes
	 */
	public toJSON() {
		return {
			idx: this.#idx,
			uid: this.#uid,
			name: this.#name,
			attacks: this.#attacks,
			stats: this.#stats,
			status: this.#status,
			type: this.#type,
			level: this.#level,
			maxHP: this.#maxHP,
			xp: this.#xp,
			catchLvl: this.#catchLvl,
			catchLocation: this.#catchLocation
		}
	}

	#willEscape() {
		return this.#isWild && this.#canEscape && this.#canEscape.chance > Math.random()
	}

	/**
	 * This method initializes the attacks of a monster based on the monster-level
	 */
	#initAttacks() {
		const attacks = Object.entries(this.lvlAttacks)
			.filter(([lvl]) => +lvl <= this.#level).map(x => x[1] as IAttack)
		if (attacks.length <= 4) { return attacks }
		do { attacks.splice(getRandomInt(0, 4), 1) } while (attacks.length > 4)
		return attacks
	}
}
