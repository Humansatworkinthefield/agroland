import { player } from '..'
import { IIventoryArgs } from '../models/classes'
import { newItemInstance, removeArrEntry } from '../util/helper'
import type { Item } from './Items/Item'
import type { Monster } from './Monster'

export class Inventory {

	static #instance: Inventory
	#items: Item[] // save
	#monsters: Monster[] // save

	get items() { return this.#items }
	get monsters() { return this.#monsters }

	public static newInventory({ items, monsters }: IIventoryArgs) {
		if (!this.#instance) { return new Inventory({ items, monsters }) }
		return this.#instance
	}

	/**
	 * This class contains the main methods to handle the player PC inventory
	 */
	private constructor({ items, monsters }: IIventoryArgs) {
		this.#items = items
		this.#monsters = monsters
	}

	public addItem(newItem: Item, count: number) {
		const idx = this.#items.findIndex(x => x.name === newItem.name)
		const playerBagItemIdx = player.items.findIndex(x => x.name === newItem.name)
		player.items[playerBagItemIdx].decreaseCount(count)
		if (idx === -1) {
			this.#items.push(newItemInstance(newItem, count))
			return
		}
		this.#items[idx].increaseCount(count)
	}

	public takeItem(item: Item, count: number) {
		const storageItemIdx = this.#items.findIndex(it => it.name === item.name)
		if (storageItemIdx >= 0) {
			player.addItem(this.#items[storageItemIdx], count)
			this.#items[storageItemIdx].decreaseCount(count)
			if (this.#items[storageItemIdx].count === 0) {
				removeArrEntry(this.#items, storageItemIdx)
			}
		}
	}

	public dropItem(item: Item, count: number) {
		const itemIdx = this.#items.findIndex(x => x.name === item.name)
		this.#items[itemIdx].decreaseCount(count)
		if (this.#items[itemIdx].count === 0) {
			removeArrEntry(this.#items, itemIdx)
		}
	}

	public addMonster(newMon: Monster) {
		this.#monsters.push(newMon)
	}

	public removeMonster(monster: Monster) {
		const monIdx = this.#monsters.findIndex(mon => mon.uid === monster.uid)
		removeArrEntry(this.#monsters, monIdx)
	}

	public swapMonster(playerMon: Monster, storageMonUID: number) {
		const storageMonIdx = this.#monsters.findIndex(mon => mon.uid === storageMonUID)
		const playerMonIdx = player.monsters.findIndex(mon => mon.uid === playerMon.uid)
		if (storageMonIdx >= 0 && playerMonIdx >= 0) {
			player.monsters[playerMonIdx] = this.#monsters[storageMonIdx]
			this.#monsters[storageMonIdx] = playerMon
		}
	}

	/**
	 * This method allows to serialize the Inventory object for saving purposes
	 * @returns a JSON string for saving progress purposes
	 */
	public toJSON() {
		return {
			items: this.#items,
			monsters: this.#monsters
		}
	}

}