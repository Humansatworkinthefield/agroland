import gsap from 'gsap'
import { battle, player } from '..'
import { endBattleAnimation } from '../animation/battle'
import { mapAudio, monsterAudio, playAudio } from '../data/audio'
import type { TAttackSequences, IDamageResult } from '../models/functions'
import { displayMonList, initTrainerHTML, initYesNoAfterTrainerMonFaints } from '../util/html/battle'
import { getElByQuery } from '../util/helper'
import { attacksSection, enemyMonInfoEl, enemyTrainerInfoEl, monsterStatsEl } from '../util/html/elements'
import { dialog } from './Dialog'
import { flags } from './Flags'
import { Monster } from './Monster'
import { reaction } from './ReactionQueue'
import { Sprite } from './Sprite'
import { MonsterType } from '../data/monster/types'
import { getMonsterByIdx } from '../data/monster/allMonsters'
import { formula } from './Formula'
import { formatName } from '../util/fmt'
import { base64AttackImgs } from '../data/base64Imgs'
import { NPC } from './NPC'
import { viewport } from './Viewport'

// Attacks reference:  https://pokemondb.net/move/generation/1
export type TAttackNames = keyof typeof allAttacks
export const allAttacks = {
	Tackle: {
		name: 'Tackle',
		power: 30, // Looks approximately like the max damage (-10) on level 100 
		type: MonsterType.normal,
		maxExec: 35, // maximum execution times
		remExec: 35, // remaining executions
		acc: 1, // accuracy : % hit chance.
		criticalChance: .05,
		isSpecial: false,
		info: 'Hits the opponent with full body contact.'
	},
	Growl: {
		name: 'Growl',
		power: 0,
		type: MonsterType.normal,
		maxExec: 40,
		remExec: 40,
		acc: 1,
		criticalChance: 0,
		isSpecial: false,
		info: 'A loud scream that intimidates the opponent and reduces its attack by 1.'
	},
	Fireball: {
		name: 'Fireball',
		power: 45,
		type: MonsterType.fire,
		maxExec: 25,
		remExec: 25,
		acc: .98,
		criticalChance: .03,
		isSpecial: true,
		info: 'A hot flame that can set the enemy on fire.'
	},
	Waterball: {
		name: 'Waterball',
		power: 45,
		type: MonsterType.water,
		maxExec: 25,
		remExec: 25,
		acc: .98,
		criticalChance: .03,
		isSpecial: true,
		info: 'A small waterbomb that hits the enemy hard and reduces its defense.'
	},
	Snowball: {
		name: 'Snowball',
		power: 45,
		type: MonsterType.ice,
		maxExec: 25,
		remExec: 25,
		acc: .98,
		criticalChance: .03,
		isSpecial: true,
		info: 'A very cold attack that can freeze the enemy.'
	},
	LeafBullet: {
		name: 'Leaf Bullet',
		power: 45,
		type: MonsterType.grass,
		maxExec: 25,
		remExec: 25,
		acc: .98,
		criticalChance: .03,
		isSpecial: true,
		info: 'Sharp leaves that targets the enemy with high speed and can cause bleeding.'
	},
	Dirtball: {
		name: 'Dirtball',
		power: 45,
		type: MonsterType.ground,
		maxExec: 25,
		remExec: 25,
		acc: .98,
		criticalChance: .03,
		isSpecial: true,
		info: 'A buildup of smelly dirt that can reduce the enemys accuracy.'
	},
	Scratch: {
		name: 'Scratch',
		power: 35,
		type: MonsterType.normal,
		maxExec: 35,
		remExec: 35,
		acc: .99,
		criticalChance: .09,
		isSpecial: false,
		info: 'A low-powered attack learned by MONSTERs with claws/talons at a low level.'
	},
	Bite: {
		name: 'Bite',
		power: 40,
		type: MonsterType.normal,
		maxExec: 30,
		remExec: 30,
		acc: .9,
		criticalChance: .09,
		isSpecial: false,
		info: 'A low-powered attack learned by MONSTERs with sharp teeth at a low level.'
	},
	Cut: {
		name: 'Cut',
		power: 50,
		type: MonsterType.normal,
		maxExec: 30,
		remExec: 30,
		acc: .95,
		criticalChance: .09,
		isSpecial: false,
		info: 'A dangerous attack that can highly cause bleeding and can also be used to cut up bushes.'
	},
	WingAttack: {
		name: 'Wing Attack',
		power: 50,
		type: MonsterType.flying,
		maxExec: 30,
		remExec: 30,
		acc: .95,
		criticalChance: .04,
		isSpecial: false,
		info: 'This attack kicks up objects laying around and can reduce the accuracy of the target.'
	},
	FUD: {
		name: 'FUD',
		power: 0,
		type: MonsterType.dark,
		maxExec: 20,
		remExec: 20,
		acc: 1,
		criticalChance: 0,
		isSpecial: false,
		info: 'Confuses the opponent by distracting it and leaves it in fear, uncertainty and doubt.'
	},
	ChainSplit: {
		name: 'Chain Split',
		power: 50,
		type: MonsterType.steel,
		maxExec: 25,
		remExec: 25,
		acc: 1,
		criticalChance: .08,
		isSpecial: true,
		info: 'Hits the opponent with hard chains and can confuse it.'
	},
	Attack51: {
		name: '51% Attack',
		power: 100,
		type: MonsterType.ghost,
		maxExec: 6,
		remExec: 6,
		acc: 1,
		criticalChance: .1,
		isSpecial: true,
		info: 'A powerfull attack that can paralyze the opponent. High critical chance.'
	},
	Hashrain: {
		name: 'Hashrain',
		power: 60,
		type: MonsterType.electric,
		maxExec: 20,
		remExec: 20,
		acc: .9,
		criticalChance: .05,
		isSpecial: true,
		info: 'A stormy rain combined with lightning thunderstorm that can paralyze the opponent.'
	},
	Lasereyes: {
		name: 'Laser Eyes',
		power: 60,
		type: MonsterType.electric,
		maxExec: 20,
		remExec: 20,
		acc: 1,
		criticalChance: .1,
		isSpecial: true,
		info: 'A laser beam coming from focused eyes that can set the enemy on fire.'
	},
	DiffAdjust: {
		name: 'Difficulty Adjustment',
		power: 75,
		type: MonsterType.normal,
		maxExec: 15,
		remExec: 15,
		acc: 1,
		criticalChance: .1,
		isSpecial: true,
		info: 'It balances the battlefield by reducing the opponent attack by a random number between 1-3'
	}
} as const

export function getAttackByName(name: TAttackNames) {
	return allAttacks[name]
}

const cutSprite = new Sprite({
	position: {
		x: 0,
		y: 0
	},
	image: { src: base64AttackImgs.cut },
	frames: { max: 4, hold: 10 },
	animate: true,
	// rotation: monster.isEnemy ? 3 : 0,
	scaleFactor: viewport.getScaleFactor(true)
})

const scratchSprite = new Sprite({
	position: {
		x: 0,
		y: 0
	},
	image: { src: base64AttackImgs.scratch },
	frames: { max: 4, hold: 7 },
	animate: true,
	// rotation: monster.isEnemy ? 3 : 0,
	scaleFactor: viewport.getScaleFactor(true)
})

class Sequences {

	#statusChangedFirst = false
	static #instance: Sequences

	public static newSequences() {
		if (!this.#instance) { return new Sequences() }
		return this.#instance
	}

	get statusChangedFirst() { return this.#statusChangedFirst }

	/** This class contains the attack related animation and methods */
	// eslint-disable-next-line @typescript-eslint/no-empty-function
	private constructor() {}

	public setStatusChangedFirst(val: boolean) {
		this.#statusChangedFirst = val
	}

	/******************************************/
	/********** Sequences/Animation  **********/
	/******************************************/

	/**
	 * This method animates the attack "Tackle"
	 * @param monster - The attacking monster object
	 * @param recipient - The target monster object
	 * @param healthBar - The target hp progress bar ID
	 * @param result - The damage object
	 */
	public tackle = (
		monster: Monster,
		recipient: Monster,
		healthBar: string,
		result: IDamageResult
	) => {
		const moveDistance = monster.isEnemy ? -20 : 20
		const timeline = gsap.timeline()
		timeline.to(monster.sprite.position, {
			x: monster.sprite.position.x - moveDistance
		}).to(monster.sprite.position, {
			x: monster.sprite.position.x + moveDistance * 2,
			duration: .1,
			onComplete: () => {
				// enemy actually gets hitted
				playAudio('tackleHit')
				// animate healthbar
				this.hpProgress(recipient, healthBar)
				// animate enemy damage
				this.#damage(recipient, result)
			}
		}).to(monster.sprite.position, {
			x: monster.sprite.position.x
		})
	}

	/**
	 * DOES AFFECT OPPONENT STATUS
	 * This method animates the attack "Fireball" and determines with a 9% chance if the opponent "burning" status will change to "true".
	 * @param monster - The attacking monster object
	 * @param recipient - The target monster object
	 * @param healthBar - The target hp progress bar
	 * @param result - The damage object
	 */
	public fireball = (
		monster: Monster,
		recipient: Monster,
		healthBar: string,
		result: IDamageResult
	) => {
		// status change
		if (
			!recipient.status.burning && !recipient.status.frozen &&// if is not burning & not frozen
			!recipient.type.includes(MonsterType.fire) && // if is not immune type fire
			battle.isHit(allAttacks.Fireball.criticalChance * 3) // battle.isHit(allAttacks.Fireball.criticalChance * 3) // 9% burning chance // DEBUG: Math.random() > .0001
		) {
			this.#statusChangedFirst = true
			recipient.status.burning = 1
			recipient.stats.IS.att /= 2
		}
		// create fireball sprite
		const fireball = new Sprite({
			position: this.#getThrowAttackSpawnPos(monster),
			image: { src: base64AttackImgs.fireball },
			frames: { max: 4, hold: 5 },
			animate: true,
			rotation: monster.isEnemy ? -2.4 : 1,
			scaleFactor: viewport.getScaleFactor(true)
		})
		// push fireball Sprite into renderedSprites to render it in the battle animation loop
		battle.renderedSprites.splice(1, 0, fireball)
		playAudio('initFireball')
		// animate fireball throw
		gsap.to(fireball.position, {
			x: this.#getThrowAttackPosX(recipient),
			y: this.#getThrowAttackPosY(recipient),
			duration: .7,
			onComplete: () => {
				// enemy actually gets hitted
				playAudio('fireballHit')
				// animate healthbar
				this.hpProgress(recipient, healthBar)
				// animate enemy damage
				this.#damage(recipient, result)
				// remove fireball sprite from renderedSprites
				battle.renderedSprites.splice(1, 1)
			}
		})
	}

	/**
	 * This method animates the attack "Waterball"
	 * @param monster - The attacking monster object
	 * @param recipient - The target monster object
	 * @param healthBar - The target hp progress bar
	 * @param result - The damage object
	 */
	public waterball = (
		monster: Monster,
		recipient: Monster,
		healthBar: string,
		result: IDamageResult
	) => {
		// create waterball sprite
		const waterball = new Sprite({
			position: this.#getThrowAttackSpawnPos(monster),
			image: { src: base64AttackImgs.waterball },
			frames: { max: 4, hold: 5 },
			animate: true,
			rotation: monster.isEnemy ? -2.4 : 1,
			scaleFactor: viewport.getScaleFactor(true)
		})
		// push waterball Sprite into renderedSprites to render it in the battle animation loop
		battle.renderedSprites.splice(1, 0, waterball)
		playAudio('initWaterball')
		// animate waterball throw
		gsap.to(waterball.position, {
			x: this.#getThrowAttackPosX(recipient),
			y: this.#getThrowAttackPosY(recipient),
			duration: .7,
			onComplete: () => {
				// enemy actually gets hitted
				playAudio('waterballHit')
				// animate healthbar
				this.hpProgress(recipient, healthBar)
				// animate enemy damage
				this.#damage(recipient, result)
				// remove waterball sprite from renderedSprites
				battle.renderedSprites.splice(1, 1)
			}
		})
	}

	/**
	 * DOES AFFECT OPPONENT STATUS
	 * This method animates the attack "Snowball" and determines with a 9% chance if the opponent "frozen" status will change to "true".
	 * @param monster - The attacking monster object
	 * @param recipient - The target monster object
	 * @param healthBar - The target hp progress bar
	 * @param result - The damage object
	 */
	public snowball = (
		monster: Monster,
		recipient: Monster,
		healthBar: string,
		result: IDamageResult
	) => {
		// status change
		if (
			!recipient.status.frozen && // if is not frozen
			!recipient.type.includes(MonsterType.ice) && // if is not immune type
			battle.isHit(allAttacks.Snowball.criticalChance * 3) // battle.isHit(allAttacks.Snowball.criticalChance * 3) // 9% freeze chance // DEBUG: Math.random() > .0001
		) {
			this.#statusChangedFirst = true
			recipient.status.frozen = 1
			recipient.stats.IS.att /= 2
		}
		// create snowball sprite
		const snowball = new Sprite({
			position: this.#getThrowAttackSpawnPos(monster),
			image: { src: base64AttackImgs.snowball },
			frames: { max: 4, hold: 5 },
			animate: true,
			rotation: monster.isEnemy ? -2.4 : 1,
			scaleFactor: viewport.getScaleFactor(true)
		})
		// push snowball Sprite into renderedSprites to render it in the battle animation loop
		battle.renderedSprites.splice(1, 0, snowball)
		playAudio('initSnowball')
		// animate snowball throw
		gsap.to(snowball.position, {
			x: this.#getThrowAttackPosX(recipient),
			y: this.#getThrowAttackPosY(recipient),
			duration: .7,
			onComplete: () => {
				// enemy actually gets hitted
				playAudio('snowballHit')
				// animate healthbar
				this.hpProgress(recipient, healthBar)
				// animate enemy damage
				this.#damage(recipient, result)
				// remove snowball sprite from renderedSprites
				battle.renderedSprites.splice(1, 1)
			}
		})
	}

	/**
	 * DOES AFFECT OPPONENT STATS VALUES
	 * This method animates the attack "Dirtball" //TODO handle stats change (reduce the accuracy of attack execution)
	 * @param monster - The attacking monster object
	 * @param recipient - The target monster object
	 * @param healthBar - The target hp progress bar
	 * @param result - The damage object
	 */
	public dirtball = (
		monster: Monster,
		recipient: Monster,
		healthBar: string,
		result: IDamageResult
	) => {
		// create dirtball sprite
		const dirtball = new Sprite({
			position: this.#getThrowAttackSpawnPos(monster),
			image: { src: base64AttackImgs.dirtball },
			frames: { max: 4, hold: 5 },
			animate: true,
			rotation: monster.isEnemy ? -2.4 : 1,
			scaleFactor: viewport.getScaleFactor(true)
		})
		// push dirtball Sprite into renderedSprites to render it in the battle animation loop
		battle.renderedSprites.splice(1, 0, dirtball)
		playAudio('initDirtball')
		// animate dirtball throw
		gsap.to(dirtball.position, {
			x: this.#getThrowAttackPosX(recipient),
			y: this.#getThrowAttackPosY(recipient),
			duration: .7,
			onComplete: () => {
				// enemy actually gets hitted
				playAudio('dirtballHit')
				// animate healthbar
				this.hpProgress(recipient, healthBar)
				// animate enemy damage
				this.#damage(recipient, result)
				// remove dirtball sprite from renderedSprites
				battle.renderedSprites.splice(1, 1)
			}
		})
	}

	/**
	 * DOES AFFECT OPPONENT STATUS
	 * This method animates the attack "leafBullet" // TODO handle status change
	 * @param monster - The attacking monster object
	 * @param recipient - The target monster object
	 * @param healthBar - The target hp progress bar
	 * @param result - The damage object
	 */
	public leafBullet = (
		monster: Monster,
		recipient: Monster,
		healthBar: string,
		result: IDamageResult
	) => {
		// create leafBullet sprite
		const leafBullet = new Sprite({
			position: this.#getThrowAttackSpawnPos(monster),
			image: { src: base64AttackImgs.leafBullet },
			frames: { max: 6, hold: 5 },
			animate: true,
			rotation: monster.isEnemy ? -2.4 : 1,
			scaleFactor: viewport.getScaleFactor(true)
		})
		// push leafBullet Sprite into renderedSprites to render it in the battle animation loop
		battle.renderedSprites.splice(1, 0, leafBullet)
		playAudio('initLeafBullet')
		// animate leafBullet throw
		gsap.to(leafBullet.position, {
			x: this.#getThrowAttackPosX(recipient),
			y: this.#getThrowAttackPosY(recipient),
			duration: .7,
			onComplete: () => {
				// enemy actually gets hitted
				playAudio('leafBulletHit')
				// animate healthbar
				this.hpProgress(recipient, healthBar)
				// animate enemy damage
				this.#damage(recipient, result)
				// remove waterball sprite from renderedSprites
				battle.renderedSprites.splice(1, 1)
			}
		})
	}

	/**
	 * DOES AFFECT OPPONENT STATS VALUES
	 * This method animates the attack "Growl" which also decreases the opponents attack power
	 * reference: https://bulbapedia.bulbagarden.net/wiki/Growl_(move)
	 * @param monster - The attacking monster object
	 * @param recipient - The target monster object
	 */
	public growl = (
		monster: Monster,
		recipient: Monster,
	) => {
		// create growl sprite
		const growl = new Sprite({
			position: {
				x: monster.isEnemy ? monster.sprite.position.x - monster.sprite.enemyWidth : monster.sprite.position.x + (monster.sprite.image.width * viewport.getScaleFactor(true)),
				y: monster.isEnemy ? monster.sprite.position.y - monster.sprite.enemyHeight / 4 : monster.sprite.position.y - (monster.sprite.image.height * viewport.getScaleFactor(true)) / 4
			},
			image: { src: base64AttackImgs.growl },
			frames: { max: 3, hold: 10 },
			animate: true,
			rotation: monster.isEnemy ? 3 : 0,
			scaleFactor: viewport.getScaleFactor(true)
		})
		playAudio('growl')
		// push growl Sprite into renderedSprites to render it in the battle animation loop
		battle.renderedSprites.splice(1, 0, growl)
		// animate monster vibration
		gsap.to(monster.sprite.position, {
			x: monster.sprite.position.x + 5,
			yoyo: true,
			repeat: 15,
			duration: .06,
			onComplete: () => {
				// shake screen side to side
				this.#shakeScreen(monster, recipient)
				// remove growl sprite from renderedSprites
				battle.renderedSprites.splice(1, 1)
				// decrease recipient stats att
				if (recipient.stats.IS.att > 1) {
					recipient.stats.IS.att -= 1
				}
				// reset sprite pos
				monster.sprite.position.x -= 5
				// prompt user after shakeScreen animation ends
				const t = setTimeout(() => {
					dialog.show(recipient.stats.IS.att > 1 ? `${recipient.name}s ATTACK is decreasing!` : 'Nothing happens!')
					flags.setIsAnimating(false)
					clearTimeout(t)
				}, 800)
			}
		})
	}

	/**
	 * This method animates the attack "Scratch"
	 * @param monster - The attacking monster object
	 * @param recipient - The target monster object
	 * @param healthBar - The target hp progress bar
	 * @param result - The damage object
	 */
	public scratch = (
		monster: Monster,
		recipient: Monster,
		healthBar: string,
		result: IDamageResult
	) => {
		// update attack sprite pos
		scratchSprite.position = this.#getCenteredAttackPos(recipient, scratchSprite)
		scratchSprite.rotation = monster.isEnemy ? 3 : 0
		scratchSprite.frames.val = 0
		// push scratch Sprite into renderedSprites to render it in the battle animation loop
		battle.renderedSprites.splice(2, 0, scratchSprite)
		playAudio('scratch')
		const t = setTimeout(() => {
			// remove scratch sprite from renderedSprites
			battle.renderedSprites.splice(2, 1)
			// play a hit audio
			playAudio('tackleHit')
			// shake screen up & down
			gsap.to(battle.background.position, {
				y: battle.background.position.y + 20,
				yoyo: true,
				repeat: 8,
				duration: .05,
				onComplete: () => {
					// reset bg pos
					battle.background.position.y -= 20
					if (result.isCritical) { reaction.queue.splice(0, 0, () => dialog.show('Critical hit!')) }
					// display type effectivness
					const typeEffectivness = battle.generateTypeEffectiveTxt(result.type1, result.type2)
					if (typeEffectivness) { reaction.queue.splice(0, 0, () => dialog.show(typeEffectivness)) }
					// animate healthbar
					this.hpProgress(recipient, healthBar)
				}
			})
			clearTimeout(t)
		}, 380)
	}

	/**
	 * DOES AFFECT OPPONENT STATUS
	 * This method animates the attack "Bite" // TODO handle status change
	 * @param monster - The attacking monster object
	 * @param recipient - The target monster object
	 * @param healthBar - The target hp progress bar
	 * @param result - The damage object
	 */
	public bite = (
		monster: Monster,
		recipient: Monster,
		healthBar: string,
		result: IDamageResult
	) => {
		// TODO add animation and sound
		console.log(monster, recipient, healthBar, result)
		flags.setIsAnimating(false)
	}

	/**
	 * DOES AFFECT OPPONENT STATUS
	 * This method animates the attack "Cut" and determines with a 18% chance if the opponent "bleeding" status will change to "true".
	 * @param monster - The attacking monster object
	 * @param recipient - The target monster object
	 * @param healthBar - The target hp progress bar
	 * @param result - The damage object
	 */
	public cut = (
		monster: Monster,
		recipient: Monster,
		healthBar: string,
		result: IDamageResult
	) => {
		// status change
		if (
			!recipient.status.bleeding && // if is not bleeding
			!recipient.type.includes(MonsterType.steel) &&
			!recipient.type.includes(MonsterType.ghost) &&
			!recipient.type.includes(MonsterType.rock) && // if is not immune type
			battle.isHit(allAttacks.Cut.criticalChance * 2) // 18% bleeding chance // DEBUG: Math.random() > .0001
		) {
			this.#statusChangedFirst = true
			recipient.status.bleeding = 1
			recipient.stats.IS.def /= 1.5
		}
		// update attack sprite pos
		cutSprite.position = this.#getCenteredAttackPos(recipient, cutSprite)
		cutSprite.rotation = monster.isEnemy ? 3 : 0
		cutSprite.frames.val = 0
		// push cut Sprite into renderedSprites to render it in the battle animation loop
		battle.renderedSprites.splice(2, 0, cutSprite)
		playAudio('cut')
		const t = setTimeout(() => {
			// remove scratch sprite from renderedSprites
			battle.renderedSprites.splice(2, 1)
			// play a hit audio
			playAudio('tackleHit')
			// animate enemy blink
			gsap.to(recipient.sprite, {
				opacity: 0,
				repeat: 5,
				yoyo: true,
				duration: .08,
				onComplete: () => {
					// display type effectivness
					const typeEffectivness = battle.generateTypeEffectiveTxt(result.type1, result.type2)
					if (typeEffectivness) { reaction.queue.splice(0, 0, () => dialog.show(typeEffectivness)) }
					if (result.isCritical) {
						reaction.queue.splice(
							0,
							0,
							() => dialog.show('Critical hit!')
						)
					}
					// animate healthbar
					this.hpProgress(recipient, healthBar)
				}
			})
			clearTimeout(t)
		}, 600)
	}

	/**
	 * DOES AFFECT OPPONENT STATS VALUES
	 * This method animates the attack "Wing attack" //TODO handle stats change (reduce the accuracy of attack execution)
	 * @param monster - The attacking monster object
	 * @param recipient - The target monster object
	 * @param healthBar - The target hp progress bar
	 * @param result - The damage object
	 */
	public wingAttack = (
		monster: Monster,
		recipient: Monster,
		healthBar: string,
		result: IDamageResult
	) => {
		// TODO add animation and sound
		console.log(monster, recipient, healthBar, result)
		flags.setIsAnimating(false)
	}

	/**
	 * DOES AFFECT OPPONENT STATS VALUES
	 * This method animates the attack "FUD" //TODO handle stats change (reduce the attack power by 1 and initiative by 2)
	 * @param monster - The attacking monster object
	 * @param recipient - The target monster object
	 * @param healthBar - The target hp progress bar
	 * @param result - The damage object
	 */
	public fud = (
		monster: Monster,
		recipient: Monster,
		healthBar: string,
		result: IDamageResult
	) => {
		// TODO add animation and sound
		console.log(monster, recipient, healthBar, result)
		flags.setIsAnimating(false)
	}

	/**
	 * DOES AFFECT OPPONENT STATUS
	 * This method animates the attack "Chain split" //TODO handle status change to "confused"
	 * @param monster - The attacking monster object
	 * @param recipient - The target monster object
	 * @param healthBar - The target hp progress bar
	 * @param result - The damage object
	 */
	public chainSplit = (
		monster: Monster,
		recipient: Monster,
		healthBar: string,
		result: IDamageResult
	) => {
		// TODO add animation and sound
		console.log(monster, recipient, healthBar, result)
		flags.setIsAnimating(false)
	}

	/**
	 * DOES AFFECT OPPONENT STATUS
	 * This method animates the attack "51% Attack" //TODO handle status change to "paralyzed"
	 * @param monster - The attacking monster object
	 * @param recipient - The target monster object
	 * @param healthBar - The target hp progress bar
	 * @param result - The damage object
	 */
	public attack51 = (
		monster: Monster,
		recipient: Monster,
		healthBar: string,
		result: IDamageResult
	) => {
		// TODO add animation and sound
		console.log(monster, recipient, healthBar, result)
		flags.setIsAnimating(false)
	}

	/**
	 * DOES AFFECT OPPONENT STATUS
	 * This method animates the attack "Hashrain" //TODO handle status change to "paralyzed"
	 * @param monster - The attacking monster object
	 * @param recipient - The target monster object
	 * @param healthBar - The target hp progress bar
	 * @param result - The damage object
	 */
	public hashrain = (
		monster: Monster,
		recipient: Monster,
		healthBar: string,
		result: IDamageResult
	) => {
		// TODO add animation and sound
		console.log(monster, recipient, healthBar, result)
		flags.setIsAnimating(false)
	}

	/**
	 * DOES AFFECT OPPONENT STATUS
	 * This method animates the attack "Laser Eyes" //TODO handle status change to "burning"
	 * @param monster - The attacking monster object
	 * @param recipient - The target monster object
	 * @param healthBar - The target hp progress bar
	 * @param result - The damage object
	 */
	public lasereyes = (
		monster: Monster,
		recipient: Monster,
		healthBar: string,
		result: IDamageResult
	) => {
		// TODO add animation and sound
		console.log(monster, recipient, healthBar, result)
		flags.setIsAnimating(false)
	}

	/**
	 * DOES AFFECT OPPONENT STATS VALUES
	 * This method animates the attack "Difficulty adjustment" //TODO can paralyze and handle stats change (reduce attack power by 1)
	 * @param monster - The attacking monster object
	 * @param recipient - The target monster object
	 * @param healthBar - The target hp progress bar
	 * @param result - The damage object
	 */
	public diffAdjust = (
		monster: Monster,
		recipient: Monster,
		healthBar: string,
		result: IDamageResult
	) => {
		// TODO add animation and sound
		console.log(monster, recipient, healthBar, result)
		flags.setIsAnimating(false)
	}

	/************************************/
	/********** Public Methods **********/
	/************************************/

	/**
	 * This method handles and animates the faint sequence of a monster
	 * @param monster - The target monster object
	 * @param infoSection - A string that represents the info element-ID of the target monster
	 * @param xp - A number that represents the amount of XP gained by the winning monster
	 */
	public faint(monster: Monster, infoSection: string, xp: number, trainer?: NPC) {
		// fade monster away position & handle faint queue onComplete
		gsap.to(monster.sprite.position, {
			y: monster.sprite.position.y + 20,
			onComplete: () => {
				flags.setIsAnimating(false)
				// hide attack btns to prevent attack trigger after monster fainted
				attacksSection.replaceChildren()
				// If fainted monster in the opponent, handle XP queue
				if (monster.isEnemy) {
					// generate xp queue
					const monsParticipated = battle.getMonsterParticipatedAlive()
					const xpQueue: (() => void)[] = []
					for (let i = monsParticipated.length - 1; i >= 0; i--) {
						const monQueue = battle.generateXPQueue(monsParticipated[i], monster, xp, trainer, i === monsParticipated.length - 1, i === 0)
						xpQueue.push(...monQueue)
					}
					// handle trainer next monster
					const trainerStillHasMon = trainer?.monsters?.find(mon => mon.stats.IS.hp > 0)
					if (trainer && trainerStillHasMon) {
						battle.renderedSprites.shift()
						battle.resetMonsParticipatedAlive()
						const nextMonster = trainerStillHasMon
						nextMonster.sprite.setPos(nextMonster.getMonsterPos())
						reaction.queue.push(
							...xpQueue,
							() => {
								monsterStatsEl.removeAttribute('style')
								initTrainerHTML(trainer)
								enemyTrainerInfoEl.style.display = 'block'
								dialog.show(`${trainer.name} is about to send out ${nextMonster.name} into the battle...`)
							},
							() => {
								dialog.show(`Will ${player.name} switch the MONSTER?`)
								const t = setTimeout(() => {
									initYesNoAfterTrainerMonFaints(trainer, nextMonster)
									clearTimeout(t)
								}, 1000)
								// next reactions are in the function "initYesNoAfterTrainerMonFaints()"
							}
						)
						return
					}
					// enemy trainer is beaten and has no more monster, handle sequence
					if (trainer && !trainerStillHasMon) {
						// remove trainer last mon from rendered sprites
						battle.renderedSprites.shift()
						reaction.queue.push(
							...xpQueue,
							() => {
								// change trainer map sprite to idle
								trainer.sprite.animate = true
								trainer.sprite.frames = { ...trainer.sprite.frames, val: trainer.getFrameValByImgSrc(trainer.sprite.image.src), hold: 210 }
								trainer.sprite.image.src = `./imgs/npcs/trainers/${trainer.isRival ? 'Rival' : formatName(trainer.name)}/idle.png`
								monsterStatsEl.removeAttribute('style')
								flags.setIsAnimating(true)
								trainer.setIsBeaten()
								// stop battle music
								trainer.audio?.battle.stop()
								// play victory against trainer audio
								trainer.playVictoryAudio()
								dialog.show(`${player.name} defeated ${trainer.name}!`)
								// fade trainer back into viewport after beeing beaten
								trainer.fadeBattleSpriteIntoViewport()
							},
							() => dialog.show(`${trainer.name}: ${trainer.looseDialog || ''}`),
							() => {
								if (!trainer.effort || !trainer.monsters) { return }
								const reward = formula.calcTrainerPayout(trainer.effort, trainer.monsters[trainer.monsters.length - 1].level)
								player.addSats(reward)
								// show reward
								dialog.show(`${player.name} received some FIAT and converts it into ${reward} SATS!`)
							}
						)
					}
					// push evolution sequences after the XP loop right before the last queue-function
					for (let i = monsParticipated.length - 1; i >= 0; i--) {
						const mon = monsParticipated[i]
						// Check for monster evolution only after a level up
						if (mon.transformLvl && mon.level >= mon.transformLvl) {
							reaction.queue.push(
								() => {
									if (!mon.transTo) {
										console.warn('this monster is not able to evolve because it has no evolution monster idx!')
										return
									}
									mapAudio.wild_victory.pause()
									trainer?.audio?.victory.pause()
									// start sound
									playAudio('evolution')
									// set is animating flag
									flags.setIsAnimating(true)
									dialog.show(`WHAT?! ${mon.name} is about to EVOLVE!`)
									// get the evolved monster object
									const evoMonObj = getMonsterByIdx(mon.transTo)
									if (!evoMonObj) {
										console.warn('evolution monster object could not be found!')
										return
									}
									const evoMon = new Monster({
										...evoMonObj,
										uid: mon.uid,
										level: mon.level,
										audio: monsterAudio[formatName(getMonsterByIdx(mon.transTo)?.name)],
										stats: mon.stats,
										attacks: mon.attacks,
										status: mon.status
									})
									// recalculate the stats of the evolved monster
									evoMon.stats.IS = formula.calcStatsIS(evoMon.stats, evoMon.level)
									// generate evolution html section and start handling evolution
									battle.evolutionSequence(mon, evoMon, trainer)
								}
							)
						}
					}
					// last func in queue
					reaction.queue.push(
						() => {
							trainer?.audio?.victory.stop()
							endBattleAnimation(trainer)
						}
					)
					// xpQueue.forEach(func => {
					// 	console.log('func: ', func)
					// })
					if (!trainer) {
						reaction.queue.splice(
							0,
							0,
							...xpQueue
						)
					}
					return
				}
				// Else: Player Monster fainted, Check for next Player Monster
				reaction.queue.push(
					() => {
						const monIdx = player.getMonsterAliveIdx()
						// No more monsters available
						if (monIdx < 0) {
							if (trainer) {
								// remove enemy monster and info-bar from viewport
								enemyMonInfoEl.style.display = 'none'
								battle.renderedSprites.shift()
								flags.setIsAnimating(true)
								const t = setTimeout(() => {
									// fade trainer back into viewport if trainer available
									trainer.fadeBattleSpriteIntoViewport(true)
									clearTimeout(t)
								}, 500)
							} else {
								dialog.show(`All MONSTER belonging to ${player.name} have been defeated!`)
							}
							const correctQueue = trainer ?
								[
									() => dialog.show(`All MONSTER belonging to ${player.name} have been defeated!`),
									() => {
										if (!trainer.effort || !trainer.monsters) { return }
										const amount = formula.calcTrainerPayout(trainer.effort, trainer.monsters[trainer.monsters.length - 1].level)
										player.decreaseSats(amount / 2)
										dialog.show(`${player.name} lost ${amount} SATS!`)
									},
									() => dialog.show(`${player.name} faints!`),
									() => {
										mapAudio.wild_battle.stop()
										trainer?.audio?.battle.stop()
										playAudio('leaveBattle')
										// Spawn player in front of Hospital with regenerated monsters
										player.hospitalSpawn()
										// replace trainer sprite and boundary to its initial position on the map
										trainer?.placeTrainerBackToInitialPos()
										// regenerate monster of trainer
										trainer.monsters?.forEach(monster => monster.regenerate())
										// console.log('trainer initial pos after reset to initial in hospital spawn: ', trainer?.initialPos)
										endBattleAnimation()
									}
								]
								:
								[
									() => dialog.show(`${player.name} faints!`),
									() => {
										mapAudio.wild_battle.stop()
										playAudio('leaveBattle')
										// Spawn player in front of Hospital with regenerated monsters
										player.hospitalSpawn()
										endBattleAnimation()
									}
								]
							reaction.queue.push(...correctQueue)
							return
						}
						// Else: display player monster list
						dialog.hide()
						// show monster list
						displayMonList({ switchAfterFaint: true, trainer })
						// remove fainted monster attack btns
						attacksSection.replaceChildren()
						dialog.show(`What will ${player.name} do?`)
					}
				)
			}
		})
		// fade monster away opacity
		gsap.to(monster.sprite, {
			opacity: 0,
			onComplete: () => {
				// reset position to initial
				gsap.to(monster.sprite.position, {
					y: monster.sprite.position.y - 20
				})
			}
		})
		// fade monster info section away
		gsap.to(infoSection, {
			y: 75,
			opacity: 0,
			onComplete: () => {
				gsap.to(infoSection, {
					y: 0
				})
			}
		})
	}

	/**
	 * This method handles the progress bar animation
	 * @param recipient - The monster object from whom the progress bar belongs to
	 * @param healthBar - A string that represents the progress bar ID of the target monster 
	 * @param isEnemy - A boolean that determines if the monster is an enemy to handle the HTML of that specific monster
	 */
	public hpProgress(recipient: Monster, healthBar: string) {
		const progressBar = getElByQuery(healthBar)
		const progressVal = progressBar.getAttribute('value')
		const hpTxtElement = getElByQuery('#playerHpTxt')
		gsap.to(healthBar, {
			value: recipient.stats.IS.hp < 0 ? 0 : recipient.stats.IS.hp,
			// determine duration based on the amount of HP damage
			duration: progressVal ? this.#getProgressbarDuration(+progressVal - recipient.stats.IS.hp) : 1,
			onComplete: () => {
				flags.setIsAnimating(false)
				if (!recipient.isEnemy) {
					hpTxtElement.innerText = `HP: ${recipient.stats.IS.hp < 0 ? 0 : recipient.stats.IS.hp}/${recipient.maxHP}`
				}
				progressBar.className = ''
				progressBar.classList.add('HPBar')
				if (recipient.stats.IS.hp <= recipient.maxHP * .15) {
					progressBar.classList.add('HPBarRed')
					return
				}
				if (recipient.stats.IS.hp > recipient.maxHP * .15 && recipient.stats.IS.hp <= recipient.maxHP * .5) {
					progressBar.classList.add('HPBarYellow')
				}
			}
		})
	}

	/**
	 * @param name - A string that determines the attack name to get the right attack-sequence
	 * @returns the attack animation sequence
	 */
	public getSequence(name: string) {
		return this.#sequences[name]
	}

	/*************************************/
	/********** Private Methods **********/
	/*************************************/

	/**
	 * This method generates a number which represents the speed of the HP progressbar animation
	 * @param damage - a number representing the damage 
	 */
	#getProgressbarDuration(damage: number) {
		if (damage < 10) { return .6 }
		if (damage >= 10 && damage < 25) { return .9 }
		if (damage >= 25 && damage < 50) { return 1.2 }
		if (damage >= 50 && damage < 75) { return 1.5 }
		if (damage >= 75 && damage < 120) { return 1.7 }
		if (damage >= 120 && damage < 175) { return 2 }
		if (damage >= 175 && damage < 220) { return 2.5 }
		return 2.5
	}

	/**
	 * This method animates the damage sequence and generated the corresponding damage reaction queue
	 * @param recipient - The target monster object that gets damaged
	 * @param result - The damage object
	 */
	#damage(recipient: Monster, result: IDamageResult) {
		// display type effectivness
		const typeEffectivness = battle.generateTypeEffectiveTxt(result.type1, result.type2)
		if (typeEffectivness) { reaction.queue.splice(0, 0, () => dialog.show(typeEffectivness)) }
		// animate enemy vibration
		gsap.to(recipient.sprite.position, {
			x: recipient.sprite.position.x + 10,
			yoyo: true,
			repeat: 5,
			duration: .08
		})
		// animate enemy blink
		gsap.to(recipient.sprite, {
			opacity: 0,
			repeat: 5,
			yoyo: true,
			duration: .08,
			onComplete: () => {
				if (result.isCritical) {
					reaction.queue.splice(
						0,
						0,
						() => dialog.show('Critical hit!')
					)
				}
			}
		})
	}

	/**
	 * This method animates a slow shake from side to side of the battle background sprite and the monsters in it
	 * @param monster - The monster obj that needs to be animated
	 * @param recipient - The enemy monster obj that needs to be animated
	 */
	#shakeScreen(monster: Monster, recipient: Monster) {
		// Please help doing this with less code...
		const timeline = gsap.timeline()
		// shake background
		timeline.to(battle.background.position, {
			x: battle.background.position.x + 20,
			yoyo: true,
			duration: .2
		}).to(battle.background.position, {
			x: battle.background.position.x - 20,
			yoyo: true,
			duration: .2
		}).to(battle.background.position, {
			x: battle.background.position.x + 20,
			yoyo: true,
			duration: .2
		}).to(battle.background.position, {
			x: battle.background.position.x,
			yoyo: true,
			duration: .2
		})
			// shake enemy
			.to(recipient.sprite.position, {
				x: recipient.sprite.position.x + 20,
				yoyo: true,
				duration: .2
			}, 0).to(recipient.sprite.position, { // the zero tells this tween to start at time 0
				x: recipient.sprite.position.x - 20,
				yoyo: true,
				duration: .2
			}, .2).to(recipient.sprite.position, {
				x: recipient.sprite.position.x + 20,
				yoyo: true,
				duration: .2
			}, .4).to(recipient.sprite.position, {
				x: recipient.sprite.position.x,
				yoyo: true,
				duration: .2
			}, .6)
			// shake monster
			.to(monster.sprite.position, {
				x: monster.sprite.position.x + 20,
				yoyo: true,
				duration: .2
			}, 0).to(monster.sprite.position, { // the zero tells this tween to start at time 0
				x: monster.sprite.position.x - 20,
				yoyo: true,
				duration: .2
			}, .2).to(monster.sprite.position, {
				x: monster.sprite.position.x + 20,
				yoyo: true,
				duration: .2
			}, .4).to(monster.sprite.position, {
				x: monster.sprite.position.x,
				yoyo: true,
				duration: .2
			}, .6)
		// timeline.to(battle.background.position, {
		// 	x: battle.background.position.x + 50,
		// 	yoyo: true,
		// 	ease: Elastic.easeInOut,
		// 	duration: 1,
		// 	onComplete: () => {
		// 		gsap.to(battle.background.position, {
		// 			x: battle.background.position.x - 50,
		// 			yoyo: true,
		// 			ease: Elastic.easeInOut,
		// 			duration: 1,
		// 		})
		// 	}
		// })
	}

	#getCenteredAttackPos(recipient: Monster, attack: Sprite) {
		const recipientWidth = recipient.isEnemy ? recipient.sprite.enemyWidth : recipient.sprite.image.width * viewport.getScaleFactor(true)
		const recipientHeight = recipient.isEnemy ? recipient.sprite.enemyHeight : recipient.sprite.image.height * viewport.getScaleFactor(true)
		const attackWidth = attack.enemyWidth
		const attackHeight = attack.enemyHeight
		const widthDiff = recipientWidth - attackWidth
		const heightDiff = recipientHeight - attackHeight
		return {
			x: recipient.sprite.position.x + (widthDiff / 2),
			y: recipient.sprite.position.y + (heightDiff / 2)
		}
	}

	#getThrowAttackSpawnPos(monster: Monster) {
		return {
			x: monster.isEnemy ? monster.sprite.position.x - 20 : monster.sprite.position.x + monster.sprite.image.width / 2,
			y: monster.sprite.position.y + monster.sprite.enemyHeight / 4
		}
	}

	#getThrowAttackPosX(recipient: Monster) {
		return recipient.isEnemy ? recipient.sprite.position.x - (recipient.sprite.enemyWidth / 2) : recipient.sprite.position.x + (recipient.sprite.image.width * viewport.getScaleFactor(true)) / 2
	}

	#getThrowAttackPosY(recipient: Monster) {
		return recipient.isEnemy ? recipient.sprite.position.y + (recipient.sprite.enemyHeight / 3) : recipient.sprite.position.y + (recipient.sprite.image.height * viewport.getScaleFactor(true)) / 2
	}

	#sequences: TAttackSequences = {
		'Tackle': this.tackle,
		'Fireball': this.fireball,
		'Waterball': this.waterball,
		'Snowball': this.snowball,
		'Leaf Bullet': this.leafBullet,
		'Dirtball': this.dirtball,
		'Growl': this.growl,
		'Scratch': this.scratch,
		'Bite': this.bite,
		'Cut': this.cut,
		'Wing Attack': this.wingAttack,
		'FUD': this.fud,
		'Chain Split': this.chainSplit,
		'51% Attack': this.attack51,
		'Hashrain': this.hashrain,
		'Laser Eyes': this.lasereyes,
		'Difficulty Adjustment': this.diffAdjust,
	}

}

export const attackSequences = Sequences.newSequences()