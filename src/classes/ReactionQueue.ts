import type { TReactionQueue } from '../models/classes'

class ReactionQueue {

	static #instance: ReactionQueue
	#queue: TReactionQueue

	/** @returns the queue array containing functions */
	get queue() { return this.#queue }

	public static newReactionQueue() {
		if (!this.#instance) { return new ReactionQueue() }
		return this.#instance
	}

	/** This class handles the reaction queue */
	private constructor() {
		this.#queue = []
	}

	/**
	 * This method executes the first function in the reaction queue
	 */
	public execute() {
		if (!this.#queue.length) {
			console.warn('There is no queue to execute!')
			return
		}
		this.#queue[0]()
		// remove the executed item
		this.#queue.shift()
	}

	/** This method clears the reaction queue */
	public clearQueue() {
		this.#queue = []
	}

}

export const reaction = ReactionQueue.newReactionQueue()