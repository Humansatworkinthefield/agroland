import { player } from '..'
import { getStoreInventory } from '../data/item/storeInventory'
import { getSavedPlayer } from '../util/localStorage'
import { Item } from './Items/Item'

interface IStoreArgs { mapID: number, storeID: number }

class Store {

	#items

	get items() { return this.#items }

	constructor({ mapID, storeID }: IStoreArgs) {
		this.#items = getStoreInventory({ mapID, storeID })
	}

	public buy(item: Item, count: number) {
		if (player.satoshis < item.price * count) {
			console.log('player has not enough sats')
			return
		}
		const storeItem = this.#getItemByName(item.name)
		if (!storeItem) {
			console.log('item not available')
			return
		}
		if (storeItem.count < count) {
			console.log('not enough items in store inventory')
			return
		}
		// TODO add max count
		player.decreaseSats(item.price * count)
		storeItem.decreaseCount(count)
		const playerItem = player.items.find(i => i.name === item.name)
		if (playerItem) {
			playerItem.increaseCount(count)
			return
		}
		player.addItem(item)
	}

	public sell(item: Item, count: number) {
		const playerItem = player.items.find(i => i.name === item.name)
		if (playerItem) {
			if (playerItem.count < count) {
				console.log('can not sell more than player has')
				return
			}
			playerItem.decreaseCount(count)
			player.addSats(item.price / 2 * count)
		}
	}

	#getItemByName(name: string) {
		return this.#items.find(item => item.name === name)
	}

}

let store = new Store({ mapID: getSavedPlayer().currentMapId, storeID: getSavedPlayer().currentHouseId })

export function getStore() { return store }

export function setStore({ mapID, storeID }: IStoreArgs) { store = new Store({ mapID, storeID }) }