import { IHouseMapData, IMapData, IPosition } from '../models/classes'
import { getSavedData } from '../util/localStorage'
import { canvas } from './Canvas'
import { Monster } from './Monster'
import { monsterAudio } from '../data/audio'
import { getMonster, getMonsterNameByIdx } from '../data/monster/allMonsters'
import { viewport } from './Viewport'
import { mapData } from '../data/map/mapData'
import { NPC } from './NPC'
import { player } from '..'

class Data {

	#mapData

	get all() { return this.#mapData }

	constructor() {
		this.#mapData = getSavedData()
		// this.addFirstMonsterForRival()
	}

	public getDataByMapID(mapID: number, houseID?: number) {
		const house = this.#mapData[mapID].houses.find(house => house.id === houseID)
		return house || this.#mapData[mapID]
	}

	public getNPCByID({ npcID, mapID, houseID }: { npcID: number, mapID: number, houseID?: number }) {
		if (houseID && houseID > 0) {
			const house = this.#mapData[mapID].houses.find(house => house.id === houseID)
			return house?.npcs.find(npc => npc.id === npcID)
		}
		return this.#mapData[mapID].npcs.find(npc => npc.id === npcID)
	}

	public updateRivalName(name: string) {
		for (const key in this.#mapData) {
			if (Object.prototype.hasOwnProperty.call(this.#mapData, key)) {
				this.#mapData[key].npcs.forEach(npc => {
					if (npc.id === 106) {
						npc.name = name
					}
				})
				this.#mapData[key].houses.forEach(house => {
					house.npcs.forEach(npc => {
						if (npc.id === 106) {
							npc.name = name
						}
					})
				})
			}
		}
	}

	public updateAllBoundariesPos(mapID: number, houseID: number, oldWidth?: number, oldHeight?: number, updateNPCStaticPos = false) {
		for (const key in this.#mapData) {
			if (Object.prototype.hasOwnProperty.call(this.#mapData, key)) {
				const map = this.#mapData[key]
				const offsetDiff = this.calcOffsetDiff(map, oldWidth, oldHeight)
				if ((houseID === 0 && +key !== mapID) || houseID > 0) {
					for (let i = 0; i < map.boundaries.length; i++) {
						const boundary = map.boundaries[i]
						boundary.position = {
							x: boundary.position.x + offsetDiff.x,
							y: boundary.position.y + offsetDiff.y
						}
					}
					for (let i = 0; i < map.npcs.length; i++) {
						const npc = map.npcs[i]
						this.updateNPCPos(npc, offsetDiff, updateNPCStaticPos)
					}
				}
				for (let i = 0; i < map.houses.length; i++) {
					const house = map.houses[i]
					const houseOffsetDiff = this.calcOffsetDiff(house, oldWidth, oldHeight)
					if (houseID === 0 || (houseID > 0 && house.id !== houseID)) {
						for (let i = 0; i < house.boundaries.length; i++) {
							const boundary = house.boundaries[i]
							boundary.position = {
								x: boundary.position.x + houseOffsetDiff.x,
								y: boundary.position.y + houseOffsetDiff.y
							}
						}
						for (let i = 0; i < house.npcs.length; i++) {
							const npc = house.npcs[i]
							this.updateNPCPos(npc, houseOffsetDiff, updateNPCStaticPos)
						}
					}
				}
			}
		}
	}

	public updateAllOffsets(mapID: number, houseID: number, oldWidth?: number, oldHeight?: number) {
		// update last visited hospital offset if available
		if (player.lastHospitalData) {
			const offsetDiff = this.calcOffsetDiff(this.#mapData[player.lastHospitalData.mapID], oldWidth, oldHeight)
			player.lastHospitalData.offset = {
				x: player.lastHospitalData.offset.x + offsetDiff.x,
				y: player.lastHospitalData.offset.y + offsetDiff.y,
			}
		}
		// update offset of all maps
		for (const key in this.#mapData) {
			if (Object.prototype.hasOwnProperty.call(this.#mapData, key)) {
				const map = this.#mapData[key]
				const offset = viewport.isSmallDevice() ? map.offset.smartphone : map.offset.desktop
				const newMapOffset = this.calcNewOffset(offset, oldWidth, oldHeight)
				if ((houseID === 0 && +key !== mapID) || houseID > 0) {
					if (viewport.isSmallDevice()) {
						map.offset.smartphone = newMapOffset
					} else {
						map.offset.desktop = newMapOffset
					}
				}
				for (let i = 0; i < map.houses.length; i++) {
					const house = map.houses[i]
					const offset = viewport.isSmallDevice() ? house.offset.smartphone : house.offset.desktop
					const newHouseOffset = this.calcNewOffset(offset, oldWidth, oldHeight)
					if (houseID === 0 || (houseID > 0 && house.id !== houseID)) {
						if (viewport.isSmallDevice()) {
							house.offset.smartphone = newHouseOffset
						} else {
							house.offset.desktop = newHouseOffset

						}
					}
				}
			}
		}
	}

	public calcOffsetDiff(map: IMapData | IHouseMapData, oldWidth?: number, oldHeight?: number) {
		const offset = viewport.isSmallDevice() ? map.offset.smartphone : map.offset.desktop
		const newMapOffset = this.calcNewOffset(offset, oldWidth, oldHeight)
		return { x: newMapOffset.x - offset.x, y: newMapOffset.y - offset.y }
	}

	/**
	 * This method calculates the map-offset after a window resize
	 * @param oldOffset - The old offset obj
	 * @param oldWidth - The old canvas width before resize
	 * @param oldHeight - The old canvas height before resize 
	 * @returns a new mapOffset object
	 */
	public calcNewOffset(oldOffset: IPosition, oldWidth?: number, oldHeight?: number) {
		if (!oldWidth || !oldHeight) { return { x: 0, y: 0 } }
		const widthDiff = canvas.element.width - oldWidth
		const heightDiff = canvas.element.height - oldHeight
		return { x: oldOffset.x + (widthDiff / 2), y: oldOffset.y + (heightDiff / 2) }
	}

	public addFirstMonsterForRival(firstMonIdx: number) {
		if (typeof firstMonIdx === 'undefined' || firstMonIdx < 0) { return }
		for (const key in this.#mapData) {
			if (Object.prototype.hasOwnProperty.call(this.#mapData, key)) {
				const map = this.#mapData[key]
				// add rival monster depending on map ID
				if (key === '22') {
					const rival = map.npcs.find(npc => npc.id === 106)
					if (!rival) { return }
					rival.monsters?.push(new Monster({
						...getMonster(getMonsterNameByIdx(firstMonIdx)),
						uid: 0,
						audio: monsterAudio[getMonsterNameByIdx(firstMonIdx)],
						level: 8,
						isEnemy: true
					}))
					return
				}
				// if (key === '24') {}
				// for (let i = 0; i < map.houses.length; i++) {
				// 	const house = map.houses[i]
				// }
			}
		}
	}

	public getInitialOffsetByID(mapID: number, houseID?: number) {
		if (houseID) {
			const house = mapData[mapID].houses.find(house => house.id === houseID)
			if (!house) {
				console.warn('house not found for offset calculation, returning {x: 0, y: 0}')
				return { x: 0, y: 0 }
			}
			return viewport.isSmallDevice() ? house.offset.smartphone : house.offset.desktop
		}
		return viewport.isSmallDevice() ? mapData[mapID].offset.smartphone : mapData[mapID].offset.desktop
	}

	public updateNPCPos(npc: NPC, offsetDiff: IPosition, updateNPCStaticPos = false) {
		npc.sprite.position = {
			x: npc.sprite.position.x + offsetDiff.x,
			y: npc.sprite.position.y + offsetDiff.y // - (16 * viewport.getScaleFactor(false))
		}
		npc.boundary.position = {
			x: npc.boundary.position.x + offsetDiff.x,
			y: npc.boundary.position.y + offsetDiff.y
		}
		if (npc.spriteHead) {
			npc.spriteHead.position = npc.sprite.position
		}
		npc.initialPos = {
			x: npc.initialPos.x + offsetDiff.x,
			y: npc.initialPos.y + offsetDiff.y
		}
		if (updateNPCStaticPos) {
			npc.initialStaticPos = {
				x: npc.initialStaticPos.x + offsetDiff.x,
				y: npc.initialStaticPos.y + offsetDiff.y
			}
		}
	}

	public toJSON() {
		return {
			mapData: this.#mapData
		}
	}

}

export const data = new Data()
