import type { IBoundayArgs, IPosition } from '../models/classes'
import { canvas } from './Canvas'

export class Boundary {
	#id
	#position
	#width
	#height

	/** @returns The ID of the boundary object */
	get id() { return this.#id }
	/** @returns The x and y coordinates of the boundary object */
	get position() { return this.#position }
	/** @returns The width of the boundary object */
	get width() { return this.#width }
	/** @returns The height of the boundary object */
	get height() { return this.#height }

	// eslint-disable-next-line @typescript-eslint/adjacent-overload-signatures
	set position(pos: IPosition) { this.#position = pos }

	/**
	 * This class draws the rectangle boundaries at a given width and height in pixels
	 */
	public constructor({ id, position, width, height }: IBoundayArgs) {
		this.#id = id
		this.#position = position
		this.#width = width
		this.#height = height
	}

	/**
	 * This method draws the collision boundaries into the canvas
	 */
	public draw() {
		if (!canvas.ctx) { return }
		canvas.ctx.fillStyle = this.#id > 244 ? 'rgba(0,0,255,.4)' : 'rgba(255,0,0,.4)'
		canvas.ctx.fillRect(this.#position.x, this.#position.y, this.#width, this.#height)
	}

	public toJSON() {
		return {
			id: this.#id,
			position: this.#position,
			width: this.#width,
			height: this.#height
		}
	}
}