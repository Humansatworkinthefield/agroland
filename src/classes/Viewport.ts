class Viewport {

	static #instance: Viewport

	public static newViewport() {
		if (!this.#instance) { return new Viewport() }
		return this.#instance
	}

	/** This class contains methods to check the current screensizes applied to the canvas */
	// eslint-disable-next-line @typescript-eslint/no-empty-function
	private constructor() {}

	/**
	 * @returns true if window.innerWidth <= 450
	 */
	public isSmartphone() {
		return window.innerWidth <= 450
	}

	/**
	 * @returns true if window.innerWidth <= 950 && window.innerHeight <= 550
	 */
	public isTurnedSmartphone() {
		return window.innerWidth <= 950 && window.innerHeight <= 550
	}

	public isSmallDevice() {
		return this.isSmartphone() || this.isTurnedSmartphone()
	}

	/**
	 * @returns true if cwindow.innerWidth > 750 && window.innerWidth < 900 &&
	 * window.innerHeight > 950 && window.innerHeight < 1180
	 */
	public isTablet() {
		return (
			window.innerWidth > 750 &&
			window.innerWidth < 900 &&
			window.innerHeight > 950 &&
			window.innerHeight < 1180
		)
	}

	/**
	 * @returns true if window.innerWidth >= 900 && window.innerWidth <= 1024 &&
	 * window.innerHeight > 720 && window.innerHeight < 820
	 */
	public isSmallDesktop() {
		return (
			window.innerWidth >= 900 &&
			window.innerWidth <= 1024 &&
			window.innerHeight > 720 &&
			window.innerHeight < 820
		)
	}

	/**
	 * @param forMon A boolean that indicates if the return value is the scale factor for monster sprites or for other sprites
	 * @returns a number representig the current scale factor for drawing into the canvas
	 */
	public getScaleFactor(forMon: boolean) {
		if (this.isSmartphone() || this.isTurnedSmartphone()) { return forMon ? .6 : .5 }
		return 1
	}

	/**
	 * @returns a number representig the current scale factor for drawing the trainer battle-sprites into the canvas
	 */
	public getTrainerScaleFactor() {
		return this.isSmartphone() || this.isTurnedSmartphone() ? .6 : 1
	}
}

export const viewport = Viewport.newViewport()