import gsap from 'gsap'
import { playAudio } from '../data/audio'
import { IStatusSequenceParams, TStatusSequence, IAttackPreventingSequence } from '../models/functions'
import { attackSequences } from './Attack'
import { dialog } from './Dialog'
import { flags } from './Flags'
import { Monster } from './Monster'
import { NPC } from './NPC'
import { reaction } from './ReactionQueue'

class Status {

	#damagingStatus = ['burning', 'poisoned', 'bleeding',]
	#nonDamagingStatus = ['frozen', 'paralyzed',]
	#attackPreventingStatus = ['frozen', 'paralyzed', 'confused']

	/**
	 * This class contains the status related methods
	 * source: https://bulbapedia.bulbagarden.net/wiki/Status_condition
	 */
	// eslint-disable-next-line @typescript-eslint/no-empty-function
	constructor() {}

	public get(name: string) {
		return this.#statusSequences[name]
	}

	public hasNonDamagingStatus(monster: Monster) {
		for (let i = 0; i < Object.entries(monster.status).length; i++) {
			const status = Object.entries(monster.status)[i]
			if (this.#nonDamagingStatus.includes(status[0]) && status[1]) {
				return true
			}
		}
		return false
	}

	public hasAttackPreventingStatus(monster: Monster) {
		for (let i = 0; i < Object.entries(monster.status).length; i++) {
			const status = Object.entries(monster.status)[i]
			if (this.#attackPreventingStatus.includes(status[0]) && status[1]) {
				return { statusName: status[0], isAffected: true }
			}
		}
		return { statusName: '', isAffected: false }
	}

	/* Non-volatile status (which remain after the battle) */

	/**
	 * Burning status (BRN) sequence
	 * source: https://bulbapedia.bulbagarden.net/wiki/Burn_(status_condition)
	 */
	public burning = ({
		affectedMon,
		attacker,
		withDamage,
		trainer
	}: IStatusSequenceParams) => {
		flags.setIsAnimating(true)
		const healthBar = affectedMon.isEnemy ? '#enemyHealthBar' : '#playerHealthBar'
		if (withDamage) {
			dialog.show(`${affectedMon.name} is suffering burns`)
			playAudio('tackleHit')
			const damage = Math.floor(affectedMon.maxHP / 12) < 1 ? 1 : Math.floor(affectedMon.maxHP / 12)
			affectedMon.stats.IS.hp -= damage
			gsap.to(affectedMon.sprite, {
				opacity: 0,
				repeat: 5,
				yoyo: true,
				duration: .08,
				onComplete: () => {
					// animate healthbar
					attackSequences.hpProgress(affectedMon, healthBar)
					if (affectedMon.stats.IS.hp <= 0) {
						// CAUTION NOTE to myself: this faint sequence can involve a fainted winner due to status damage 
						reaction.queue.push(() => affectedMon.faint(attacker, trainer))
					}
				}
			})
			return
		}
		// else: animate burns status
		dialog.show(`${affectedMon.name} is BURNING!`)
		playAudio('fireballHit')
		this.#monVibration(affectedMon)
	}

	/**
	 * source: https://bulbapedia.bulbagarden.net/wiki/Poison_(status_condition)
	 */
	public poisoned = ({
		affectedMon,
		attacker,
		withDamage,
		trainer
	}: IStatusSequenceParams) => {
		flags.setIsAnimating(true)
		const healthBar = affectedMon.isEnemy ? '#enemyHealthBar' : '#playerHealthBar'
		if (withDamage) {
			dialog.show(`The poison harms ${affectedMon.name}`)
			playAudio('tackleHit')
			const damage = Math.floor(affectedMon.maxHP / 12) < 1 ? 1 : Math.floor(affectedMon.maxHP / 12)
			affectedMon.stats.IS.hp -= damage
			gsap.to(affectedMon.sprite, {
				opacity: 0,
				repeat: 5,
				yoyo: true,
				duration: .08,
				onComplete: () => {
					// animate healthbar
					attackSequences.hpProgress(affectedMon, healthBar)
					if (affectedMon.stats.IS.hp <= 0) {
						// CAUTION NOTE to myself: this faint sequence can involve a fainted winner due to status damage 
						reaction.queue.push(() => affectedMon.faint(attacker, trainer))
					}
				}
			})
			return
		}
		// else: animate poisoned suffering
		dialog.show(`${affectedMon.name} is POISONED!`)
		// TODO add poisoned audio
		this.#monVibration(affectedMon)
	}

	// TODO add badly poisoned:
	/*
		The bad poison condition inflicts damage every turn, with the amount of damage increasing each turn.
		It initially inflicts damage equal to 1/16 of the Pokémon's maximum HP, with the damage inflicted increasing by 1/16 each turn
		(2/16 on the second turn, 3/16 on the third turn, etc.). In Generation V, Pokémon glow purple while afflicted with bad poison. 
	*/

	/**
	 * source: https://bulbapedia.bulbagarden.net/wiki/Freeze_(status_condition)
	 */
	public frozen = ({ affectedMon, showDialog }: IAttackPreventingSequence) => {
		flags.setIsAnimating(true)
		// else: animate frozen suffering
		if (showDialog) { dialog.show(`${affectedMon.name} is FROZEN!`) }
		affectedMon.sprite.brightness = 250
		// TODO add frozen audio
		this.#monVibration(affectedMon)
	}

	/**
	 * source: https://bulbapedia.bulbagarden.net/wiki/Paralysis_(status_condition)
	 */
	public paralyzed = ({ affectedMon, showDialog }: IAttackPreventingSequence) => {
		flags.setIsAnimating(true)
		// else: animate frozen suffering
		if (showDialog) { dialog.show(`${affectedMon.name} is PARALYZED!`) }
		// TODO add paralyzed audio
		this.#monVibration(affectedMon)
	}

	/**
	 * Bleeding sequence
	 */
	public bleeding = ({
		affectedMon,
		attacker,
		withDamage,
		trainer
	}: IStatusSequenceParams) => {
		flags.setIsAnimating(true)
		const healthBar = affectedMon.isEnemy ? '#enemyHealthBar' : '#playerHealthBar'
		if (withDamage) {
			dialog.show(`The wound is dealing damage to ${affectedMon.name}!`)
			playAudio('tackleHit')
			const damage = Math.floor(affectedMon.maxHP / 12) < 1 ? 1 : Math.floor(affectedMon.maxHP / 12)
			affectedMon.stats.IS.hp -= damage
			gsap.to(affectedMon.sprite, {
				opacity: 0,
				repeat: 5,
				yoyo: true,
				duration: .08,
				onComplete: () => {
					// animate healthbar
					attackSequences.hpProgress(affectedMon, healthBar)
					if (affectedMon.stats.IS.hp <= 0) {
						// CAUTION NOTE to myself: this faint sequence can involve a fainted winner due to status damage 
						reaction.queue.push(() => affectedMon.faint(attacker, trainer))
					}
				}
			})
			return
		}
		// else: animate bleeding suffering
		dialog.show(`${affectedMon.name} is BLEEDING!`)
		// TODO add bleeding audio
		this.#monVibration(affectedMon)
	}

	/* Volatile status (which do not remain after the battle) */

	public confused = ({ affectedMon, showDialog }: IAttackPreventingSequence) => {
		flags.setIsAnimating(true)
		if (showDialog) { dialog.show(`${affectedMon.name} is CONFUSED!`) }
		// TODO add confused audio
		this.#monVibration(affectedMon)
	}

	/** Undo Status with a % chance */

	/**
	 * This method checks if the frozen monster can thaw with the use of Math.random() and
	 * resets his status to initial
	 * @param monster - The monster affected by the frozen status
	 * @returns a boolean which indicates if the monster is thawed or not
	 */
	public isThawed = (monster: Monster) => {
		// 20% chance of thawing
		if (monster.status.frozen && Math.random() < .2) {
			monster.sprite.brightness = 100
			monster.status.frozen = 0
			return true
		}
		return false
	}

	// TODO add sleeping: https://bulbapedia.bulbagarden.net/wiki/Sleep_(status_condition)

	// TODO add bound: https://bulbapedia.bulbagarden.net/wiki/Bound

	// TODO add can't escape: https://bulbapedia.bulbagarden.net/wiki/Escape_prevention

	// see more: https://bulbapedia.bulbagarden.net/wiki/Status_condition

	/* Handle damaging status queue */
	public generateDamagingStatusQueue(monster: Monster, recipient: Monster, trainer?: NPC, withDamage = false) {
		// create empty queue
		const statusQueue: (() => void)[] = []
		// dont generate queue if the opponent will faint
		if (recipient.stats.IS.hp <= 0) { return statusQueue }
		// generate status sequence queue 
		for (let i = 0; i < Object.entries(monster.status).length; i++) {
			const status = Object.entries(monster.status)[i]
			const statusName = status[0]
			if (this.#damagingStatus.includes(statusName) && status[1]) {
				statusQueue.push(() => {
					this.get(statusName)?.({
						affectedMon: monster,
						attacker: recipient,
						withDamage,
						trainer
					})
				})
			}
		}
		return statusQueue
	}

	/* Handle non-damaging status queue */
	public generateNonDamagingStatusQueue(monster: Monster) {
		// create queue
		const statusQueue: (() => void)[] = []
		for (let i = 0; i < Object.entries(monster.status).length; i++) {
			const status = Object.entries(monster.status)[i]
			const statusName = status[0]
			if ((this.#nonDamagingStatus.includes(statusName) || this.#attackPreventingStatus.includes(statusName)) && status[1]) {
				statusQueue.push(() => {
					this.get(statusName)?.({
						affectedMon: monster,
						showDialog: true
					})
				})
			}
		}
		return statusQueue
	}

	/* Handle non-damaging status */

	#statusSequences: TStatusSequence = {
		'burning': this.burning,
		'poisoned': this.poisoned,
		'frozen': this.frozen,
		'paralyzed': this.paralyzed,
		'bleeding': this.bleeding,
		'confused': this.confused
	}

	/* Animation */

	#monVibration(monster: Monster) {
		gsap.to(monster.sprite.position, {
			x: monster.sprite.position.x + 5,
			yoyo: true,
			repeat: 20,
			duration: .06,
			onComplete: () => {
				// reset sprite pos
				monster.sprite.position.x -= 5
				flags.setIsAnimating(false)
			}
		})
	}
}

export const statusSequence = new Status()