describe('Test Item class', () => {

	test('defines method increaseCount()', async () => {
		const x = await import(__dirname + "/../../src/classes/Items/Item")
		const item = new x.Item({ name: 'Ball', count: 1 })
		expect(typeof item.increaseCount).toBe('function')
	})

	test('defines method decreaseCount()', async () => {
		const x = await import(__dirname + "/../../src/classes/Items/Item")
		const item = new x.Item({ name: 'Ball', count: 1 })
		expect(typeof item.decreaseCount).toBe('function')
	})

	test('defines method toJSON()', async () => {
		const x = await import(__dirname + "/../../src/classes/Items/Item")
		const item = new x.Item({ name: 'Ball', count: 1 })
		expect(typeof item.toJSON).toBe('function')
	})

	test('should increase the count of the item', async () => {
		const x = await import(__dirname + "/../../src/classes/Items/Item")
		const item = new x.Item({ name: 'Ball', count: 1 })
		item.increaseCount(1)
		expect(item.count).toStrictEqual(2)
	})

	test('should decrease the count of the item', async () => {
		const x = await import(__dirname + "/../../src/classes/Items/Item")
		const item = new x.Item({ name: 'Ball', count: 1 })
		item.decreaseCount(1)
		expect(item.count).toStrictEqual(0)
	})

	test('should serialize some Item object properties for saving progress purposes', async () => {
		const x = await import(__dirname + "/../../src/classes/Items/Item")
		const item = new x.Item({ name: 'Ball', count: 1 })
		const result = item.toJSON()
		expect(result).toStrictEqual({ "count": 1, "name": "BALL" })
	})

})