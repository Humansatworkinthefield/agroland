import { readFileSync } from "fs"

describe('test Canvas class', () => {

	beforeAll(() => {
		// console.log('env', process.env.NODE_ENV)
		document.body.innerHTML = readFileSync(__dirname + "/../../public/index.html").toString()
	})

	test('defines method setSizes()', async () => {
		const x = await import(__dirname + "/../../src/classes/Canvas")
		expect(typeof x.canvas.setSizes).toBe('function')
	})

	test('defines method centerGameWrap()', async () => {
		const x = await import(__dirname + "/../../src/classes/Canvas")
		expect(typeof x.canvas.centerGameWrap).toBe('function')
	})

	test('should have a canvas width of 1024', async () => {
		const x = await import(__dirname + "/../../src/classes/Canvas")
		expect(x.canvas.element.width).toStrictEqual(1024)
	})

	test('should have a canvas height of 576', async () => {
		const x = await import(__dirname + "/../../src/classes/Canvas")
		expect(x.canvas.element.height).toStrictEqual(576)
	})

	test('should have the canvas context method drawImage()', async () => {
		const x = await import(__dirname + "/../../src/classes/Canvas")
		const img = new Image()
		img.src = '../../public/imgs/ball/ballSmoke.png'
		img.onload = () => {
			x.canvas.ctx.drawImage(img, 0, 0)
			expect(x.canvas.ctx.drawImage).toHaveBeenCalledTimes(1)
		}
	})

})