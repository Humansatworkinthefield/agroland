import { readFileSync } from "fs"

global.structuredClone = jest.fn(val => JSON.parse(JSON.stringify(val)))

describe('test Sprite class', () => {

	beforeAll(() => {
		// console.log('env', process.env.NODE_ENV)
		document.body.innerHTML = readFileSync(__dirname + "/../../public/index.html").toString()
	})

	const position = { x: 0, y: 0 }

	test('defines method draw()', async () => {
		const x = await import(__dirname + "/../../src/classes/Sprite")
		const sprite = new x.Sprite({ position })
		expect(typeof sprite.draw).toBe('function')
	})

	test('defines method resetPos()', async () => {
		const x = await import(__dirname + "/../../src/classes/Sprite")
		const sprite = new x.Sprite({ position })
		expect(typeof sprite.resetPos).toBe('function')
	})

	test('defines method setPos()', async () => {
		const x = await import(__dirname + "/../../src/classes/Sprite")
		const sprite = new x.Sprite({ position })
		expect(typeof sprite.setPos).toBe('function')
	})

	test('defines method toJSON()', async () => {
		const x = await import(__dirname + "/../../src/classes/Sprite")
		const sprite = new x.Sprite({ position })
		expect(typeof sprite.toJSON).toBe('function')
	})

	test('should reset the sprite position to initial', async () => {
		const x = await import(__dirname + "/../../src/classes/Sprite")
		const sprite = new x.Sprite({ position })
		sprite.resetPos()
		expect(sprite.position).toStrictEqual({ x: 512, y: 288 })
	})

	test('should set the sprite position to a given position', async () => {
		const x = await import(__dirname + "/../../src/classes/Sprite")
		const sprite = new x.Sprite({ position })
		sprite.setPos({ x: 1000, y: 800 })
		expect(sprite.position).toStrictEqual({ x: 1000, y: 800 })
	})

	test('should serialize some Sprite object properties for saving progress purposes', async () => {
		const x = await import(__dirname + "/../../src/classes/Sprite")
		const sprite = new x.Sprite({ position })
		const result = sprite.toJSON()
		expect(result).toStrictEqual({ "position": { "x": 512, "y": 288 } })
	})

	/* test('method draw() should call the canvas context API "drawImage" ', async () => {
		const x = await import(__dirname + "/../../src/classes/Sprite")
		const canvas = await import(__dirname + "/../../src/classes/canvas")
		// const ctx = canvas.getContext('2d')
		const sprite = new x.Sprite({ position })
		sprite.draw()
		// const calls = ctx?.__getDrawcalls()
		expect(canvas.ctx.drawImage).toHaveBeenCalledTimes(1)
	}) */

})