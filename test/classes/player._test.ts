// import { Player } from "../../src/classes/Player"

import { readFileSync } from "fs"

jest.mock(__dirname + '/../../public/js/release', () => ({
	doSomething: jest.fn(),
	isColliding: jest.fn(),
	wasmColliding: jest.fn(),
	calculateOverlappingArea: jest.fn(),
	wasmCalcOverlapping: jest.fn(),
}))
global.structuredClone = jest.fn(val => {
	return JSON.parse(JSON.stringify(val))
})
describe('Player class', () => {
	beforeAll(() => {
		console.log('envp', process.env.NODE_ENV, __dirname + "/../../public/test.html")
		document.body.id = 'appBody'
		document.body.innerHTML = readFileSync(__dirname + "/../../public/test.html").toString()
		// const div = document.createElement('div')
		// div.id = 'gameWrap'
		// document.body.append(div)
	})
	test('defines method hasMoved()', async () => {
		const x = await import(__dirname + "/../../src/classes/Player")
		const player = new x.Player({
			name: 'PLAYER',
			monsters: [],
			items: []
		})
		player.monsters[0].stats.IS.hp = 0
		const result = player.getMonsterAliveIdx()
		expect(result).toBe(1)
	})

})