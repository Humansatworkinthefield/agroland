import { readFileSync } from "fs"

describe('test Dialog class', () => {

	beforeAll(() => {
		// console.log('env', process.env.NODE_ENV)
		document.body.id = 'appBody'
		document.body.innerHTML = readFileSync(__dirname + "/../../public/index.html").toString()
	})

	test('defines method show()', async () => {
		const x = await import(__dirname + "/../../src/classes/Dialog")
		expect(typeof x.dialog.show).toBe('function')
	})

})