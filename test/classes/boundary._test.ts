import { readFileSync } from "fs"

describe('Test Boundary class', () => {

	beforeAll(() => {
		// console.log('env', process.env.NODE_ENV)
		document.body.innerHTML = readFileSync(__dirname + "/../../public/index.html").toString()
	})

	test('defines method draw()', async () => {
		const x = await import(__dirname + "/../../src/classes/Boundary")
		const boundary = new x.Boundary({
			position: {
				x: 0,
				y: 0
			}
		})
		expect(typeof boundary.draw).toBe('function')
	})

	test('should draw a boundary', async () => {
		const x = await import(__dirname + "/../../src/classes/Boundary")
		const boundary = new x.Boundary({
			position: {
				x: 0,
				y: 0
			}
		})
		boundary.draw()
		expect(boundary.position).toStrictEqual({ x: 0, y: 0 })
		// expect(boundary.draw).toHaveBeenCalledTimes(1)
	})

})