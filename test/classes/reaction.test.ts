import { reaction } from '../../src/classes/ReactionQueue'

describe('Test Reaction class', () => {
	test('defines method execute', () => {
		expect(typeof reaction.execute).toBe("function")
	})
	test('defines method clearQueue', () => {
		expect(typeof reaction.clearQueue).toBe("function")
	})
	test('should execute first queue', () => {
		let result = 0
		reaction.queue.push(...[() => result += 10, () => result -= 1])
		reaction.execute()
		expect(result).toBe(10)
	})
	test('should clear the queue', () => {
		reaction.queue.push(...[() => console.log('Hello'), () => console.log('World')])
		reaction.clearQueue()
		expect(reaction.queue).toEqual([])
	})
	test('should not execute empty queue', () => {
		const result = reaction.queue
		reaction.execute()
		expect(result).toEqual([])
	})
})