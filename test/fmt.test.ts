import { formatMonIdx, formatName } from "../src/util/fmt"

describe('test formatMonIdx', () => {
	test('formatMonIdx < 10 forEntry', () => {
		const idx = 1
		const forEntry = true
		const result = formatMonIdx(idx, forEntry)
		expect(result).toBe(`- 00${idx + 1} -`)
	})
	test('formatMonIdx < 10 ', () => {
		const idx = 1
		const forEntry = false
		const result = formatMonIdx(idx, forEntry)
		expect(result).toBe(`00${idx + 1}`)
	})
	test('formatMonIdx < 100 forEntry', () => {
		const idx = 10
		const forEntry = true
		const result = formatMonIdx(idx, forEntry)
		expect(result).toBe(`- 0${idx + 1} -`)
	})
	test('formatMonIdx < 100 ', () => {
		const idx = 10
		const forEntry = false
		const result = formatMonIdx(idx, forEntry)
		expect(result).toBe(`0${idx + 1}`)
	})
	test('formatMonIdx < 1000 forEntry', () => {
		const idx = 100
		const forEntry = true
		const result = formatMonIdx(idx, forEntry)
		expect(result).toBe(`- ${idx + 1} -`)
	})
	test('formatMonIdx < 1000 ', () => {
		const idx = 100
		const forEntry = false
		const result = formatMonIdx(idx, forEntry)
		expect(result).toBe(`${idx + 1}`)
	})
})

describe('test formatName', () => {
	test('formatName Uppercase', () => {
		const name = 'DRAGGLE'
		const result = formatName(name)
		expect(result).toBe('Draggle')
	})
	test('formatName lowercase', () => {
		const name = 'draggle'
		const result = formatName(name)
		expect(result).toBe('Draggle')
	})
	test('formatName with number in string', () => {
		const name = '6595563'
		const result = formatName(name)
		expect(result).toBe('6595563')
	})
	test('formatName empty string', () => {
		const name = ''
		expect(() => formatName(name))
			.toThrowError(new Error(`Invalid name: NAME MISSING`))
	})
})