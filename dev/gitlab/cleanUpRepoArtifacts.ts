

export interface IGitlabGetJobsResp {
	id: number;
	status: string;
	stage: string;
	name: string;
	ref: string;
	tag: boolean;
	coverage: number | null;
	allow_failure: boolean;
	created_at: string;
	started_at: string;
	finished_at: string;
	duration: number;
	queued_duration: number;
	user: IUser;
	commit: ICommit;
	pipeline: IPipeline;
	web_url: string;
	artifacts: IArtifact[];
	artifacts_file?: IArtifact[];
	runner: IRunner;
	artifacts_expire_at: null | string;
	tag_list: any[];
}

export interface IArtifact {
	file_type: string;
	size: number;
	filename: string;
	file_format: null;
}

export interface ICommit {
	id: string;
	short_id: string;
	created_at: string;
	parent_ids: string[];
	title: string;
	message: string;
	author_name: string;
	author_email: string;
	authored_date: string;
	committer_name: string;
	committer_email: string;
	committed_date: string;
	web_url: string;
}

export interface IPipeline {
	id: number;
	iid: number;
	project_id: number;
	sha: string;
	ref: string;
	status: string;
	source: string;
	created_at: string;
	updated_at: string;
	web_url: string;
}

export interface IRunner {
	id: number;
	description: string;
	ip_address: string;
	active: boolean;
	paused: boolean;
	is_shared: boolean;
	runner_type: string;
	name: string;
	online: boolean;
	status: string;
}

export interface IUser {
	id: number;
	username: string;
	name: string;
	state: string;
	avatar_url: string;
	web_url: string;
	created_at: string;
	bio: string;
	location: string;
	public_email: string;
	skype: string;
	linkedin: string;
	twitter: string;
	website_url: string;
	organization: string;
	job_title: string;
	pronouns: null;
	bot: boolean;
	work_information: null;
	followers: number;
	following: number;
	local_time: null;
}

const token = process.env.NPM_TOKEN || 'GITLAB-PRIVATE-TOKEN'
const server = 'https://gitlab.com'
const projectId = 38050637
const apiBase = `${server}/api/v4/projects/${projectId}`

// /api/v4/projects/%v/jobs?per_page=%d&page=%d&artifact_expired=false
const req = (path: string, method: 'GET' | 'POST' | 'DELETE') => fetch(
	`${apiBase}/${path}`, {
		headers: { 'PRIVATE-TOKEN': token },
		method,
	})

const getJobs = async (page = 1, lenPerPage = 100) =>
	await (await req(
		`jobs?per_page=${lenPerPage}&page=${page}`, 'GET'
	)).json() as IGitlabGetJobsResp[]
const eraseJob = async (jobId: number) => {
	// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
	const eraseResp = await (await req(`jobs/${jobId}/erase`, 'POST')).json()
	// eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
	if (eraseResp?.message === '403 Forbidden - Job is not erasable!') {
		// console.log(`${idx}/${ids.length} job erase error : ${id} - '403 Forbidden - Job is not erasable!'`)
	}
	return eraseResp as { message?: string }
}
const deleteArtifacts = async (jobId: number) => {
	const delResp = await req(`jobs/${jobId}/artifacts`, 'DELETE')
	return delResp.status === 204
}
const deleteAllArtifacts = async () => {
	const delResp = await req('artifacts', 'DELETE')
	return delResp.status === 202
}
const getAllJobIds = async () => {
	let oldLen = -1
	let page = 1
	// header X-Total-Pages
	// header X-Next-Page
	// header X-Total
	const resp = await getJobs(page)
	while (resp.length !== oldLen) {
		oldLen = resp.length
		page++
		resp.push(...await getJobs(page))
		console.log(page, resp.length, oldLen)
	}
	console.log(resp.length)
	return resp.map(x => x.id)
}
// eslint-disable-next-line @typescript-eslint/no-unused-vars
const hasArtifacts = (x: IGitlabGetJobsResp) => x?.artifacts_file || x.artifacts?.length

const cleanUp = async () => {
	const ids = await getAllJobIds()
	for (const id of ids) {
		const idx = ids.indexOf(id) + 1
		console.log(`${idx}/${ids.length} job to process: ${id}`)
		if (await deleteArtifacts(id)) {
			console.log(`${idx}/${ids.length} job artifacts deleted: ${id}`)
		} else {
			console.log(`${idx}/${ids.length} job artifacts delete error : ${id}`)
		}
		const eraseResp = await eraseJob(id)
		if (eraseResp?.message === '403 Forbidden - Job is not erasable!') {
			console.log(`${idx}/${ids.length} job erase error : ${id} - '403 Forbidden - Job is not erasable!'`)
		} else if (eraseResp) {
			console.log(`${idx}/${ids.length} job erased: ${id}`, eraseResp)
		} else {
			console.log(`${idx}/${ids.length} job erase error : ${id}`, eraseResp)
		}
		// break
	}
	if (await deleteAllArtifacts()) {
		console.log('all artifacts deleted')
	} else { console.log('all artifacts delete error') }
}
void cleanUp()